const ADMIN_EMAIL = 'phakeem1@gmail.com';
const SENDER_EMAIL = "hello@talentsandmentors.com";
var Comment = require('./models/Comment');
var Like = require('./models/Like');
var _Message = require('./models/Message');
var Connection = require('./models/Connection');
var UserGroup = require('./models/UserGroups');
var Group = require('./models/Group');
var UserMentor = require('./models/UserMentors');
var Mentor = require('./models/Mentor');

var express = require('express');
var cor = require('cors');
var cluster = require("cluster");
// var sticky = require('sticky-session');
var app = express();
var http = require('http').Server(app);
var numCPUs = require("os").cpus().length;
var io = require('socket.io').listen(http);
var redis = require('socket.io-redis');

Object.assign = require('object-assign');

// var express = require('express');
// var app = express();
// var os = require('os');
// var http = require('http');
// var server = http.createServer(app);
// var io = require('socket.io').listen(server);
// var redis = require('socket.io-redis');
//
// if (cluster.isMaster) {
//     console.log('master');
//     // we create a HTTP server, but we do not use listen
//     // that way, we have a socket.io server that doesn't accept connections
//
//     io.adapter(redis({ host: 'localhost', port: 6379 }));
//
//     setInterval(function() {
//         // all workers will receive this in Redis, and emit
//         io.emit('data', 'payload');
//     }, 1000);
//
//     for (var i = 0; i < os.cpus().length; i++) {
//         cluster.fork();
//     }
//
//     cluster.on('exit', function(worker, code, signal) {
//         console.log('worker ' + worker.process.pid + ' died');
//     });
// }
//
// if (cluster.isWorker) {
//     console.log('worker');
//
//     server = http.createServer(app);
//
//     io.adapter(redis({ host: 'localhost', port: 6379 }));
//     io.on('connection', function(socket) {
//         socket.emit('data', 'connected to worker: ' + cluster.worker.id);
//     });
//
//     app.listen(3600);
// }

var request = require('request-promise');
var bodyParser = require('body-parser');
var Linkedin = require('node-linkedin')('774bs20nb5i6qo', 'wUFMm93xt3Rzzy4v');
var cookieParser = require('cookie-parser');
var sg = require('sendgrid')('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');
var es = require('elasticsearch');
var path = require('path');
var compression = require('compression');

// var mongo = require('mongodb').MongoClient;
// var io = require('socket.io')(http);
// var socket_redis = require('socket.io-redis');
// io.adapter(socket_redis({ host: 'localhost', port: 6379 }));

io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});

var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/commentSystem');

// s3 integration
var s3Url = 's3://AKIAJ7YO3EAGMS5WBDWA:p9lMyKj9eZeES+wY7QGQv7e+K5Q+5ybwvt9dPH1Q@talents.s3.amazonaws.com';
var s3 = require('node-s3')(s3Url);

var client = new es.Client({
    host: '178.62.199.243:9200',
    log: 'trace'
});

client.ping({
    requestTimeout: Infinity,
    hello: "elasticsearch!"
}, function (error) {
    if (error) {
        console.trace('elasticsearch cluster is down!');
    } else {
        console.log('All is well');
    }
});

var Redis = require('ioredis'),
    redis_client = new Redis('6379', '178.62.199.243');

redis_client.on('connect', function () {
    console.log('Redis connected');
});

function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}


function MHGETALL(keys, cb) {

    redis_client.multi({pipeline: false});

    keys.forEach(function (key, index) {
        redis_client.hgetall(key);
    });

    redis_client.exec(function (err, result) {
        cb(err, result);
    });
}

function toObject(arr) {
    var one = arr.filter(function (res) {
        return arr.indexOf(res) % 2 == 0
    });
    var two = arr.filter(function (res) {
        return one.indexOf(res) == -1
    });

    var rv = {};
    for (var i = 0; i < two.length; ++i)
        rv[one[i]] = two[i];
    return rv;
}

Linkedin.auth.setCallback('http://localhost/linkedin');

var scope = ['r_basicprofile', 'r_emailaddress', 'r_contactinfo'];

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.use(cookieParser());
app.use(compression());

app.engine('html', require('ejs').renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

function saveAuth(data) {
    var auth = {
        _id: "session",
        session: data.session_token,
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email,
        name: data.name,
        is_mentor: data.is_mentor
    };

    // db.put(auth, function callback(err, result) {
    //     if (!err) {
    //         return true;
    //     }
    // });

    return false;
}

function isAuthenticated(req, res, next) {

    // CHECK IF A USER IS LOGGED IN
    var session = req.cookies['session'];

    if (session) {
        return next();
    }
    else {
        res.redirect('/');
    }
}

function isMentor(req, res, next) {
    var profile = req.cookies['user_profile'];

    if (profile) {
        var dat = JSON.parse(profile);
        return dat.is_mentor ? next() : res.redirect('/');
    }
    else {
        res.redirect('/');
    }
}

function notAuthenticated(req, res, next) {

    var session = req.cookies['session'];

    if (session) {
        res.redirect('/home');
    }
    else {
        return next();
    }
}

function sendMail(recipient, from, subject, body, url, content_type, from_name, recipient_name) {

    url = url || '';
    content_type = content_type || 'text/html';
    from_name = from_name || null;
    recipient_name = recipient_name || null;

    var request = sg.emptyRequest();
    request.body = {
        "content": [
            {
                "type": content_type,
                "value": body + '<br><br><br><table class="module" role="module" data-type="button" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-attributes="%7B%22dropped%22%3Atrue%2C%22borderradius%22%3A6%2C%22buttonpadding%22%3A%2212%2C18%2C12%2C18%22%2C%22text%22%3A%22Accept%22%2C%22alignment%22%3A%22center%22%2C%22fontsize%22%3A16%2C%22url%22%3A%22%2525url%2525%22%2C%22height%22%3A%22%22%2C%22width%22%3A%22%22%2C%22containerbackground%22%3A%22%23ffffff%22%2C%22padding%22%3A%220%2C0%2C0%2C0%22%2C%22buttoncolor%22%3A%22%231188e6%22%2C%22textcolor%22%3A%22%23ffffff%22%2C%22bordercolor%22%3A%22%231288e5%22%7D"><tr><td style="padding: 0px 0px 0px 0px;" align="center" bgcolor="#ffffff"><table border="0" cellpadding="0" cellspacing="0" class="wrapper-mobile"><tr><td align="center" style="-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; font-size: 16px;" bgcolor="#1188e6"><a href="' + url + '" ' + 'class="bulletproof-button" target="_blank" style="height: px; width: px; font-size: 16px; line-height: px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 18px 12px 18px; text-decoration: none; color: #ffffff; text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; border: 1px solid #1288e5; display: inline-block;">Accept</a></td></tr></table></td></tr></table>'
            }
        ],
        "from": {
            "email": from,
            "name": from_name
        },
        // "mail_settings": {
        //   "footer": {
        //     "enable": true,
        //     "html": "<p>Thanks</br>Talents & Mentors</p>",
        //     "text": "Thanks,/n Talents & Mentors"
        //   }
        // },
        "personalizations": [
            {
                "headers": {
                    "X-Accept-Language": "en"
                    // "X-Mailer": "MyApp"
                },
                "subject": subject,
                // "substitutions": {
                //   "id": "substitutions",
                //   "type": "object"
                // },
                "to": [
                    {
                        "email": recipient,
                        "name": recipient_name
                    }
                ]
            }
        ],
        "reply_to": {
            "email": "hello@talentsandmentors.com",
            "name": "Hello"
        },
        "subject": subject,
        "template_id": "f68ff518-999f-4ae4-89b8-359470a81eca",
        "url": "http://talentsandmentors.com"
    };
    request.method = 'POST';
    request.path = '/v3/mail/send';
    sg.API(request, function (error, response) {
    })
}

app.use(cor());
app.use('/static', express.static('static'));
app.use('/images', express.static('static/images'));

app.get('/', notAuthenticated, function (req, res) {
    res.clearCookie('email');
    res.clearCookie('session');
    res.clearCookie('user_profile');
    res.render('index');
});

app.post('/mail/talents', function (req, res) {

    var text = req.body.email + " is requesting for your mentoring";

    if (req.body.text) {
        text = req.body.text;
    }

    sendMail(req.body.recipient, req.body.email, req.body.email + " is requesting for mentoring", text);
});

app.post('/mail/mentors', function (req, res) {

    var text = req.body.email + " is requesting to be a mentor";

    if (req.body.text) {
        text = req.body.text;
    }

    var _id = new Date().getTime();
    var uniq_id = new Buffer(req.body.email + '_notifications_' + _id).toString('base64');

    // var resp = sendMail(ADMIN_EMAIL, SENDER_EMAIL, "Request to be a mentor - " + req.body.email, text, 'http://talentsandmentors.com/notifications/' + uniq_id);

    var url = 'http://talentsandmentors.com:8900';
    var request = sg.emptyRequest();
    request.body = {
        "content": [
            {
                "type": 'text/html',
                "value": text + '<br><br><br><table class="module" role="module" data-type="button" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-attributes="%7B%22dropped%22%3Atrue%2C%22borderradius%22%3A6%2C%22buttonpadding%22%3A%2212%2C18%2C12%2C18%22%2C%22text%22%3A%22Accept%22%2C%22alignment%22%3A%22center%22%2C%22fontsize%22%3A16%2C%22url%22%3A%22%2525url%2525%22%2C%22height%22%3A%22%22%2C%22width%22%3A%22%22%2C%22containerbackground%22%3A%22%23ffffff%22%2C%22padding%22%3A%220%2C0%2C0%2C0%22%2C%22buttoncolor%22%3A%22%231188e6%22%2C%22textcolor%22%3A%22%23ffffff%22%2C%22bordercolor%22%3A%22%231288e5%22%7D"><tr><td style="padding: 0px 0px 0px 0px;" align="center" bgcolor="#ffffff"><table border="0" cellpadding="0" cellspacing="0" class="wrapper-mobile"><tr><td align="center" style="-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; font-size: 16px;" bgcolor="#1188e6"><a href="' + url + '" ' + 'class="bulletproof-button" target="_blank" style="height: px; width: px; font-size: 16px; line-height: px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 18px 12px 18px; text-decoration: none; color: #ffffff; text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; border: 1px solid #1288e5; display: inline-block;">Accept</a></td></tr></table></td></tr></table>'
            }
        ],
        "from": {
            "email": SENDER_EMAIL,
            "name": "Mentors"
        },
        "personalizations": [
            {
                "headers": {
                    "X-Accept-Language": "en"
                },
                "subject": "Request to be a mentor - " + req.body.email,
                "to": [
                    {
                        "email": ADMIN_EMAIL,
                        "name": "Admin"
                    }
                ]
            }
        ],
        "reply_to": {
            "email": "hello@talentsandmentors.com",
            "name": "Hello"
        },
        "subject": "Request to be a mentor - " + req.body.email,
        "template_id": "f68ff518-999f-4ae4-89b8-359470a81eca",
        "url": "http://talentsandmentors.com:8900"
    };
    request.method = 'POST';
    request.path = '/v3/mail/send';
    sg.API(request, function (error, response) {
    });

    try {
        // redis_client.hmset(uniq_id, {
        //     "id": uniq_id,
        //     'actor': req.body.name,
        //     'recipient_mail': "admin@talentsandmnetors.com",
        //     'action': '',
        //     'verb': 'admin_request',
        //     'subject': "Mentor Request",
        //     'message': req.body.name + " is requesting to be a mentor.",
        //     'image_url': req.body.image_url,
        //     "name": req.body.name
        // });
        // redis_client.sadd("admin_notifications_" + req.body.email, uniq_id);
        var identifier = new Buffer('mentor_request_' + req.body.email).toString('base64');
        redis_client.hmset(identifier, {
            'name': req.body.name,
            "email": req.body.email,
            "text": text,
            "username": req.body.username,
            "image_url": req.body.image_url,
            "type": "request"
        });
        redis_client.sadd("admin_mentor_requests", identifier);
        redis_client.set("requested_" + req.body.email, 'requested', function (data, status) {
        });
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.post('/mentors/verify', function (req, res) {

    var text = req.body.email + " is requesting to be verified";

    if (req.body.text) {
        text = req.body.text;
    }

    var _id = new Date().getTime();
    var uniq_id = new Buffer(req.body.email + '_notifications_' + _id).toString('base64');

    var url = 'http://talentsandmentors.com:8900';
    var request = sg.emptyRequest();
    request.body = {
        "content": [
            {
                "type": 'text/html',
                "value": text + '<br><br><br><table class="module" role="module" data-type="button" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-attributes="%7B%22dropped%22%3Atrue%2C%22borderradius%22%3A6%2C%22buttonpadding%22%3A%2212%2C18%2C12%2C18%22%2C%22text%22%3A%22Accept%22%2C%22alignment%22%3A%22center%22%2C%22fontsize%22%3A16%2C%22url%22%3A%22%2525url%2525%22%2C%22height%22%3A%22%22%2C%22width%22%3A%22%22%2C%22containerbackground%22%3A%22%23ffffff%22%2C%22padding%22%3A%220%2C0%2C0%2C0%22%2C%22buttoncolor%22%3A%22%231188e6%22%2C%22textcolor%22%3A%22%23ffffff%22%2C%22bordercolor%22%3A%22%231288e5%22%7D"><tr><td style="padding: 0px 0px 0px 0px;" align="center" bgcolor="#ffffff"><table border="0" cellpadding="0" cellspacing="0" class="wrapper-mobile"><tr><td align="center" style="-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; font-size: 16px;" bgcolor="#1188e6"><a href="' + url + '" ' + 'class="bulletproof-button" target="_blank" style="height: px; width: px; font-size: 16px; line-height: px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 18px 12px 18px; text-decoration: none; color: #ffffff; text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; border: 1px solid #1288e5; display: inline-block;">Accept</a></td></tr></table></td></tr></table>'
            }
        ],
        "from": {
            "email": SENDER_EMAIL,
            "name": "Mentors"
        },
        "personalizations": [
            {
                "headers": {
                    "X-Accept-Language": "en"
                },
                "subject": "Request to be a verified mentor - " + req.body.email,
                "to": [
                    {
                        "email": ADMIN_EMAIL,
                        "name": "Admin"
                    }
                ]
            }
        ],
        "reply_to": {
            "email": "hello@talentsandmentors.com",
            "name": "Hello"
        },
        "subject": "Request to be a verified mentor - " + req.body.email,
        "template_id": "f68ff518-999f-4ae4-89b8-359470a81eca",
        "url": "http://talentsandmentors.com:8900"
    };
    request.method = 'POST';
    request.path = '/v3/mail/send';
    sg.API(request, function (error, response) {
    });

    try {
        var identifier = new Buffer('verification_request_' + req.body.email).toString('base64');
        redis_client.hmset(identifier, {
            'name': req.body.name,
            "email": req.body.email,
            "text": text,
            "username": req.body.username,
            "image_url": req.body.image_url,
            "type": "verification"
        });
        redis_client.sadd("admin_mentor_requests", identifier);
        redis_client.set("verification_requested_" + req.body.email, 'requested', function (data, status) {
        });
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.post('/send/mail', function (req, res) {

    var text = "Hi " + req.body.recipient + ", kindly connect with me on Talents & Mentors. Thanks.";

    if (req.body.text) {
        text = req.body.text;
    }

    var _id = new Date().getTime();
    var uniq_id = new Buffer(req.body.requester + '_notifications_' + _id).toString('base64');

    var resp = sendMail(req.body.requestee, req.body.requester, "Connection Request from " + req.body.sender, text, 'http://talentsandmentors.com/notifications/' + uniq_id);

    // try {
    redis_client.hmset(uniq_id, {
        "id": uniq_id,
        'actor': req.body.sender,
        'recipient_mail': req.body.requester,
        'action': '',
        'verb': 'request',
        'subject': "Connection request",
        'message': req.body.sender + " is requesting to connect with you.",
        'image_url': req.body.image_url,
        "username": req.body.username,
        "name": req.body.recipient,
        "is_mentor": req.body.is_mentor
    });
    redis_client.sadd("notifications_" + req.body.requestee, uniq_id);

    // emit notification update message
    var msg = String(req.body.requestee);
    io.emit('notification', {
        email: msg
    });

    var identifier = new Buffer(req.body.requester + '_request_' + req.body.requestee).toString('base64');
    redis_client.hmset(identifier, {
        'user': req.body.recipient,
        "email": req.body.requestee
    });
    redis_client.sadd(req.body.requester + "_request", identifier, function (d, s) {
    });
    res.json({"response": true});
    // } catch (err) {
    //     res.json({"response": false});
    // }
});

app.post('/group/request', function (req, res) {
    var text = req.body.sender + " wants to your group - " + req.body.recipient;
    if (req.body.text) {
        text = req.body.text;
    }

    var _id = new Date().getTime();
    var uniq_id = new Buffer(req.body.requester + '_notifications_' + _id).toString('base64');

    var resp = sendMail(req.body.requestee, req.body.requester, "Group Request from " + req.body.sender, text, 'http://talentsandmentors.com/notifications/' + uniq_id);

    try {
        redis_client.hmset(uniq_id, {
            "id": uniq_id,
            'actor': req.body.sender,
            'recipient_mail': req.body.requester,
            'action': '',
            'verb': 'group_request',
            'subject': "Group Request",
            'message': req.body.sender + " is requesting to join " + req.body.recipient,
            'image_url': req.body.image_url,
            "username": req.body.username,
            "name": req.body.recipient,
            "is_mentor": req.body.is_mentor
        });
        redis_client.sadd("notifications_" + req.body.requestee, uniq_id);

        var msg = String(req.body.requestee);
        io.emit('notification', {
            email: msg
        });

        var identifier = new Buffer(req.body.requester + '__group_request__' + req.body.requestee).toString('base64');
        redis_client.hmset(identifier, {
            'user': req.body.recipient,
            "email": req.body.requestee
        });
        redis_client.sadd(req.body.requester + "_group_request_", identifier);
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.post('/mentor/request', function (req, res) {
    var text = req.body.sender + " wants your mentoring - " + req.body.recipient;

    if (req.body.text) {
        text = req.body.text;
    }

    var _id = new Date().getTime();
    var uniq_id = new Buffer(req.body.requester + '_notifications_' + _id).toString('base64');

    var resp = sendMail(req.body.requestee, req.body.requester, "Mentor Request from " + req.body.sender, text, 'http://talentsandmentors.com/notifications/' + uniq_id);

    try {
        redis_client.hmset(uniq_id, {
            "id": uniq_id,
            'actor': req.body.sender,
            'recipient_mail': req.body.requester,
            'action': '',
            'verb': 'mentor_request',
            'subject': "Mentor Request",
            'message': req.body.sender + " is requesting for your mentoring " + req.body.recipient,
            'image_url': req.body.image_url,
            "username": req.body.username,
            "name": req.body.recipient,
            "is_mentor": req.body.is_mentor
        });
        redis_client.sadd("notifications_" + req.body.requestee, uniq_id);

        var msg = String(req.body.requestee);
        io.emit('notification', {
            email: msg
        });

        var identifier = new Buffer(req.body.requester + '__mentor_request__' + req.body.requestee).toString('base64');
        redis_client.hmset(identifier, {
            'user': req.body.recipient,
            "email": req.body.requestee
        });
        redis_client.sadd(req.body.requester + "_mentor_request_", identifier);

        var ident = new Buffer(req.body.requestee + '_mentor_requests_' + req.body.requester).toString('base64');
        redis_client.hmset(ident, {
            'name': req.body.sender,
            'email': req.body.requester,
            'image_url': req.body.image_url,
            "username": req.body.username,
            "is_mentor": req.body.is_mentor,
            "note_id": uniq_id
        });
        redis_client.sadd("mentor_requests_" + req.body.requestee, ident);

        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.get('/logout', isAuthenticated, function (req, res) {
    var session = req.cookies['session'];
    request({
        url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
        method: 'DELETE',
        headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": session.session
        }
    }, function (error, response, body) {
        res.clearCookie('session');
        res.clearCookie('user_profile');
        res.redirect('/');
    });
});

app.get('/forgot', notAuthenticated, function (req, res) {
    res.render('templates/forgot');
});

app.get('/login', notAuthenticated, function (req, res) {
    res.render('templates/login');
});

app.post('/login', function (req, res) {
    request({
        url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
        method: 'POST',
        json: {
            email: req.body.username,
            password: req.body.password,
            duration: 0,
            remember_me: true
        }
    }, function (error, response, body) {
        if (body.session_token) {
            var auth = {
                session: body.session_token,
                first_name: body.first_name,
                last_name: body.last_name,
                email: body.email,
                name: body.name
            };
            request({
                method: 'GET',
                url: "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?filter=email%3D%22" + auth.email + "%22",
                headers: {
                    "cache-control": "no-cache",
                    "x-dreamfactory-session-token": auth.session,
                    "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
                }
            }, function (err, resp, __data) {
                var bdy = JSON.parse(__data);
                if (bdy.resource && bdy.resource.length > 0) {
                    var resg = {
                        session: auth.session_token,
                        first_name: auth.first_name,
                        last_name: auth.last_name,
                        name: auth.name,
                        birthdate: bdy.resource[0].birthdate,
                        signup_date: bdy.resource[0].datecreated,
                        marital: bdy.resource[0].marital,
                        email: bdy.resource[0].email,
                        lastlogin: bdy.resource[0].lastlogin,
                        phone: bdy.resource[0].phone,
                        occupation: bdy.resource[0].industry,
                        banner_url: bdy.resource[0].banner_url,
                        image_url: bdy.resource[0].image_url,
                        profile_url_param: bdy.resource[0].profile_url_param,
                        summary: bdy.resource[0].summary,
                        is_mentor: bdy.resource[0].is_mentor
                    };
                    res.cookie('user_profile', resg);
                    res.redirect('/home');
                }
                else {
                    res.redirect('/regsiter')
                }
            })
        }
        else {
            res.redirect('/regsiter')
        }
    });
});

app.get('/about', function (req, res) {
    res.render('about');
});

app.get('/policy', function (req, res) {
    res.render('policy');
});

app.get('/agreement', function (req, res) {
    res.render('agreement');
});

app.get('/register', notAuthenticated, function (req, res) {
    res.render('templates/signup');
    // res.sendFile(__dirname + '/views/templates/signup.html')
});

app.post('/register', notAuthenticated, function (req, res, next) {
    request({
        url: 'http://178.62.199.243:8180/api/v2/user/register', //URL to hit
        method: 'POST',
        json: {
            email: req.body.email,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            display_name: req.body.first_name,
            new_password: req.body.password
        }
    }, function (error, response, body) {
        if (response.statusCode == 400) {
            res.render(__dirname + '/views/templates/signup.html', {
                error: body.error.context
            })
        } else {
            res.redirect('/login');
        }
    });
});

app.get('/home', isAuthenticated, function (req, res) {
    res.render('templates/index');
    // res.sendFile(__dirname + '/views/templates/index.html')
});

app.post('/posts/create', isAuthenticated, function (req, res) {
    try {
        redis_client.hmset(req.body.user + '_' + req.body.id, {
            "id": req.body.user + '_' + req.body.id,
            'body': req.body.text,
            'user': req.body.user,
            'tag': req.body.tag,
            'poster_url': req.body.poster_url,
            'poster': req.body.poster,
            'image_url': req.body.image_url,
            "comments_count": 0,
            "likes_count": 0
        });
        redis_client.sadd("posts_" + req.body.user, req.body.user + '_' + req.body.id);
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

// app.post('/posts/:post_id/like', isAuthenticated, function (req, res) {
//     try {
//         var uniq_id = req.params.post_id + '_' + req.body.user_id;
//         redis_client.hexists("likes", uniq_id, function (data, status) {
//             if (!status) {
//                 redis_client.hset("likes", uniq_id, 1, function (data, status) {
//                     if (status) {
//                         redis_client.hincrby(req.params.post_id, "likes_count", 1)
//                     }
//                 });
//             }
//             res.json({"response": true});
//         });
//     } catch (err) {
//         console.log(err);
//         res.json({"response": false});
//     }
// });

app.post('/posts/:post_id/comment', isAuthenticated, function (req, res) {
    try {
        var uniq_id = req.params.post_id + '_' + req.body.user_id;
        redis_client.hexists("comments_count", uniq_id, function (data, status) {
            if (!status) {
                redis_client.hset("comments_count", uniq_id, 1, function (data, status) {
                    if (status) {
                        redis_client.hincrby(req.params.post_id, "comments_count", 1)
                    }
                });
            }
            res.json({"response": true});
        });
    } catch (err) {
        console.log(err);
        res.json({"response": false});
    }
});

app.get('/posts/:email/get', isAuthenticated, function (req, res) {
    redis_client.smembers("posts_" + req.params.email, function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var posts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": posts, 'error': err});
        });
    })
});

app.get('/posts', function (req, res) {
    res.render('templates/posts');
});

app.get('/choices', notAuthenticated, function (req, res) {
    var email = req.cookies['email'];
    if (!email || email == undefined) {
        res.redirect('/')
    }
    res.sendFile(__dirname + '/views/templates/choices.html')
});

app.get('/details', notAuthenticated, function (req, res) {
    var choice = req.cookies['choice'];
    var email = req.cookies['email'];
    if (choice == undefined && email == undefined) {
        res.redirect('/home')
    }
    res.sendFile(__dirname + '/views/templates/details.html')
});

app.get('/preferences', notAuthenticated, function (req, res) {
    var choice = req.cookies['choice'];
    var email = req.cookies['email'];
    var details = req.cookies['detail'];
    if (!choice && !email && !details) {
        res.redirect('/home')
    }
    res.sendFile(__dirname + '/views/templates/preferences.html')
});

app.get('/mentors', function (req, res) {
    res.render('templates/mentors');
});

app.get('/talents', function (req, res) {
    res.render('templates/talent');
});

app.get('/community', function (req, res) {
    res.render('templates/guideline');
});

app.get('/notifications', isAuthenticated, function (req, res) {
    res.render('templates/notification');
});

app.get('/connections', isAuthenticated, function (req, res) {
    res.render('templates/connections');
});

app.get('/messages', isAuthenticated, function (req, res) {
    res.render('templates/messages');
});

app.get('/private/mentors', isAuthenticated, function (req, res) {
    res.render('templates/private_mentors');
});

app.get('/search', function (req, res) {
    res.render('templates/search-result');
});

app.post('/search', function (req, res) {
    client.search({
        index: 'talents',
        fields: ["title", "description", "name", "phone", "occupation", "summary", "email", "location", "marital", "link"],
        q: req.body.search_q
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._id;
        });
        client.mget({
            index: 'talents',
            body: {
                ids: result
            }
        }, function (error, response) {
            if (response.error) {
                res.json({"response": [], "error": null, "total": 0});
            }
            else {
                res.json({"response": response.docs, "error": null, "total": response.docs.length});
            }
        });
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/dashboard', isMentor, function (req, res) {
    res.render('admin/dashboard');
});

app.get('/conversations', isMentor, function (req, res) {
    res.render('admin/conversations');
});

app.get('/library', isMentor, function (req, res) {
    res.render('admin/library');
});

app.get('/mentors/talents', isMentor, function (req, res) {
    res.render('talents');
});

app.get('/mentors/notifications', isMentor, function (req, res) {
    res.render('notifications');
});

app.get('/jobs', function (req, res) {
    res.render('templates/jobs');
});

app.get('/posts/:id/likes', function (req, res) {
    res.render('templates/likes');
});


app.get('/guide', function (req, res) {
    res.render('guide');
});

app.get('/profile', isAuthenticated, function (req, res) {
    res.render('templates/profile');
});

app.get('/Editprofile', isAuthenticated, function (req, res) {
    res.render('templates/Editprofile');
});

app.get('/userProfile', isAuthenticated, function (req, res) {
    res.render('templates/userProfile');
});

app.get('/posts/:id/likes', function (req, res) {
    res.render('templates/likes');
});

app.get('/guide', function (req, res) {
    res.render('guide');
});

app.get('/profile/edit', isAuthenticated, function (req, res) {
    res.render('templates/edit');
});

app.get('/groups/new', isAuthenticated, function (req, res) {
    res.render('templates/create_group');
});

app.get('/groups', function (req, res) {
    res.render('templates/groups');
});

app.get('/socket', function (req, res) {
    res.render('socket');
});

app.get('/group', isAuthenticated, function (req, res) {
    res.render('templates/group');
});

app.get('/posts/:id', function (req, res) {
    res.render('templates/article');
});

app.get('/posts/:id/comments', function (req, res) {
    Comment.find({postID: req.params.id}, function (err, comments) {
        res.json(comments);
    });
});

app.post('/post/:id/comment', function (req, res) {

    var comment = new Comment();
    comment.body = req.body.body;
    comment.name = req.body.name;
    comment.author = req.body.author;
    comment.postID = req.params.id;
    comment.username = req.body.username;
    comment.image_url = req.body.image_url;
    comment.save(function (err) {
        res.json({message: "Comment saved successfully"});
    });
});
//
// app.get('/likes/:id', function(req,res){
//     Like.find({postID: req.params.id}, function(err,likes){
//         res.json(likes);
//     });
// });

app.post('/user/:id/messages', function (req, res) {

    var msg = new _Message();
    msg.uniqueID = req.params.id;
    msg.sender_url = req.body.sender_url;
    msg.sender_name = req.body.sender_name;
    msg.sender_email = req.body.sender_email;
    msg.recipient_url = req.body.recipient_url;
    msg.sender_username = req.body.sender_username;
    msg.recipient_username = req.body.recipient_username;
    msg.text = req.body.text;
    msg.save(function (err) {

        io.emit(req.body.recipient_mail, {
            email: req.body.sender_email
        });

        io.emit(req.body.sender_email, {
            email: req.body.recipient_mail
        });

        res.json({message: "Message saved successfully"});
    });
});

app.get('/user/:id/conversations', function (req, res) {
    _Message.find({uniqueID: req.params.id}, function (err, messages) {
        res.json(messages);
    });
});

app.get('/invite', isAuthenticated, function (req, res) {
    res.render('templates/invite');
});

app.post('/invite', isAuthenticated, function (req, res) {

    var emails = req.body.emails.split(',');

    for (var i = 0; i < emails.length; i++) {
        var x = emails[i];
        var text = req.body.sender + " wants you to join him on Talents&Mentors. Talents&Mentors is the world's largest professional mentoring network.";
        var request = sg.emptyRequest();
        request.body = {
            "content": [
                {
                    "type": 'text/html',
                    "value": text +
                    '<br><br><br><table class="module" role="module" data-type="button" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-attributes="%7B%22dropped%22%3Atrue%2C%22borderradius%22%3A6%2C%22buttonpadding%22%3A%2212%2C18%2C12%2C18%22%2C%22text%22%3A%22Accept%22%2C%22alignment%22%3A%22center%22%2C%22fontsize%22%3A16%2C%22url%22%3A%22%2525url%2525%22%2C%22height%22%3A%22%22%2C%22width%22%3A%22%22%2C%22containerbackground%22%3A%22%23ffffff%22%2C%22padding%22%3A%220%2C0%2C0%2C0%22%2C%22buttoncolor%22%3A%22%231188e6%22%2C%22textcolor%22%3A%22%23ffffff%22%2C%22bordercolor%22%3A%22%231288e5%22%7D"><tr><td style="padding: 0px 0px 0px 0px;" align="center" bgcolor="#ffffff"><table border="0" cellpadding="0" cellspacing="0" class="wrapper-mobile"><tr><td align="center" style="-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; font-size: 16px;" bgcolor="#1188e6"><a href="http://talentsandmentors.com/register" ' + 'class="bulletproof-button" target="_blank" style="height: px; width: px; font-size: 16px; line-height: px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 18px 12px 18px; text-decoration: none; color: #ffffff; text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; border: 1px solid #1288e5; display: inline-block;">Join</a></td></tr></table></td></tr></table>'
                }
            ],
            "from": {
                "email": "hello@talentsandmentors.com",
                "name": "Hello"
            },
            "personalizations": [
                {
                    "headers": {
                        "X-Accept-Language": "en"
                    },
                    "subject": req.body.sender + " wants you to join him on Talents&Mentors.",
                    "to": [
                        {
                            "email": x
                        }
                    ]
                }
            ],
            "reply_to": {
                "email": "hello@talentsandmentors.com",
                "name": "Hello"
            },
            "subject": req.body.sender + " wants you to join him on Talents&Mentors.",
            "template_id": "f68ff518-999f-4ae4-89b8-359470a81eca",
            "url": "http://talentsandmentors.com/register"
        };
        request.method = 'POST';
        request.path = '/v3/mail/send';
        sg.API(request, function (error, response) {
            console.log(error);
        });
    }
    res.json({"response": true});
});

app.get('/likes/:id', isAuthenticated, function (req, res) {
    redis_client.smembers("likes_" + req.params.id, function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.post('/post/:id/like', function (req, res) {
    try {
        redis_client.hmset(req.body.email + '_' + req.params.id, {
            "post_id": req.params.id
        });
        redis_client.sadd(req.body.email + "_likes", req.body.email + '_' + req.params.id);
        redis_client.hmset(req.body.email + '_likes_' + req.params.id, {
            "post_id": req.params.id,
            "name": req.body.name,
            "author": req.body.email,
            "postID": req.params.id,
            "username": req.body.username,
            "image_url": req.body.image_url,
            "uniqueid": req.body.author + '_' + req.params.id
        });
        redis_client.sadd("likes_" + req.params.id, req.body.email + '_likes_' + req.params.id);
        res.json({"response": true});
        // save likes
        // var like = new Like();
        // like.name = req.body.name;
        // like.author = req.body.email;
        // like.postID = req.params.id;
        // like.username = req.body.username;
        // like.image_url = req.body.image_url;
        // like.uniqueID = req.body.author + '_' + req.params.id;
        // like.save(function(err){
        //     res.json({message:"Like saved successfully"});
        // });
    } catch (err) {
        res.json({"response": false});
    }
});


app.post('/post/:id/unlike', function (req, res) {
    try {
        redis_client.del(req.body.email + '_' + req.params.id);
        redis_client.srem(req.body.email + "_likes", req.body.email + '_' + req.params.id);

        redis_client.del(req.body.email + '_likes_' + req.params.id);
        redis_client.srem("likes_" + req.params.id, req.body.email + '_likes_' + req.params.id);
        res.json({"response": true});
        // Like.findOneAndRemove({uniqueID: req.body.uniqueid}, function (data, status) {
        //     console.log(data);
        //     console.log(status);
        //     res.json({"response": true});
        // });
    } catch (err) {
        res.json({"response": false});
    }
});

app.get('/likes/:id', isAuthenticated, function (req, res) {
    redis_client.smembers("likes_" + req.params.id, function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/users/:email/likes', isAuthenticated, function (req, res) {
    redis_client.smembers(req.params.email + "_likes", function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/group/:name', isAuthenticated, function (req, res) {
    res.render('templates/individual');
});

app.get('/public/:account', function (req, res) {
    if (!req.params.account) {
        res.redirect('/home');
    }
    res.render('templates/userProfile');
});

app.get('/notifications/:id', isAuthenticated, function (req, res) {
    res.sendFile(__dirname + '/views/templates/single_notification.html')
});

app.get('/oauth/linkedin', function (req, res) {
    var url = 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=774bs20nb5i6qo&redirect_uri=http://localhost/linkedin&state=YWdyZWVtZW50YW5kZGlzYWdmZ2s&scope=r_basicprofile%20r_emailaddress%20w_share';
    res.redirect(url);
});

app.get('/linkedin', function (req, res) {
    var code = req.query.code;
    var error = req.query.error;
    if (!code && error) {
        res.redirect('/login');
    }

    request({
        url: 'https://www.linkedin.com/oauth/v2/accessToken', //URL to hit
        method: 'POST',
        form: {
            grant_type: 'authorization_code',
            code: code,
            redirect_uri: 'http://localhost/linkedin',
            client_id: '774bs20nb5i6qo',
            client_secret: 'wUFMm93xt3Rzzy4v'
        }
    }, function (error, response, body) {
        var resp = JSON.parse(body);

        if (resp.error) {
            res.redirect('/login');
        }

        request({
            url: 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,location,public-profile-url,email-address)?format=json',
            method: 'GET', //Specify the method
            headers: { //We can define headers too
                'Authorization': 'Bearer ' + resp.access_token
            }
        }, function (error, response, body) {
            if (error) {
                console.log(error);
            } else {
                var data = JSON.parse(body);
                request({
                    url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
                    method: 'POST',
                    json: {
                        email: data.emailAddress,
                        password: data.id,
                        duration: 0
                    }
                }, function (error, response, body) {
                    var json_data = {
                        email: data.emailAddress,
                        first_name: data.firstName,
                        last_name: data.lastName,
                        display_name: data.firstName,
                        new_password: data.id
                    };
                    if (response.statusCode == 400 || response.statusCode == 401) {
                        request({
                            url: 'http://178.62.199.243:8180/api/v2/user/register', //URL to hit
                            method: 'POST',
                            json: json_data
                        }, function (error, response, body) {
                            if (response.statusCode == 400 || response.statusCode == 401) {
                                res.redirect('/login');
                            } else {
                                request({
                                    url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
                                    method: 'POST',
                                    json: {
                                        email: data.emailAddress,
                                        password: data.id,
                                        duration: 0
                                    }
                                }, function (error, response, body) {
                                    if (response.statusCode == 400 || response.statusCode == 401) {
                                        res.redirect('/login');
                                    } else {
                                        saveAuth(body);
                                        res.redirect('/home');
                                    }
                                });
                            }
                        });
                    } else {
                        saveAuth(body);
                        res.redirect('/home');
                    }
                })
            }
        });
    });
});

app.post('/elastic/:_type/index', function (req, res) {
    client.index({
        index: "talents",
        type: req.params._type,
        id: req.body.id,
        body: req.body
    }, function (err, resp, status) {
        if (status == 201) {
            res.sendStatus(status);
        }
        else {
            res.json({"error": err});
        }
    });
});

app.post('/elastic/:_type/update', function (req, res) {
    client.update({
        index: 'talents',
        type: req.params._type,
        id: req.body.id,
        body: {
            doc: {
                name: req.body.name,
                image_url: req.body.image_url,
                banner_url: req.body.banner_url,
                summary: req.body.summary,
                username: req.body.username,
                location: req.body.location,
                industry: req.body.industry,
                marital: req.body.marital,
                birthdate: req.body.birthdate,
                phone: req.body.phone,
                is_mentor: req.body.is_mentor,
                id: req.body.email
            }
        }
    }, function (error, response) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({response: response, error: error}));
    });
});

app.get('/elastic/:email/recommendations', function (req, res) {

    client.search({
        index: 'talents',
        type: "people",
        body: {
            "query": {
                "function_score": {
                    "functions": [
                        {
                            "random_score": {
                                "seed": new Date().getTime()
                            }
                        }
                    ],
                    "score_mode": "sum",
                    "filter": {
                        "bool": {
                            "must_not": [
                                {"term": {"id": req.params.email}},
                                {"term": {"is_mentor": true}}
                            ]
                        }
                    }
                }
            },
            "from": 0,
            "size": 3
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/elastic/recommendations', function (req, res) {

    client.search({
        index: 'talents',
        type: "people",
        body: {
            "query": {
                "function_score": {
                    "functions": [
                        {
                            "random_score": {
                                "seed": new Date().getTime()
                            }
                        }
                    ],
                    "score_mode": "sum"
                }
            },
            "from": 0,
            "size": 8
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/elastic/profiles', function (req, res) {

    client.search({
        index: 'talents',
        type: "people",
        body: {
            "query": {
                "function_score": {
                    "functions": [
                        {
                            "random_score": {
                                "seed": new Date().getTime()
                            }
                        }
                    ],
                    "score_mode": "sum"
                }
            },
            "from": 0,
            "size": 4
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/job', function (req, res) {

    client.search({
        index: 'talents',
        type: 'job',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "match_all": {}
                    }
                }
            },
            "sort": [
                {
                    "dateadded": {
                        "order": "desc"
                    }
                }
            ],
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/:_type/all', function (req, res) {

    client.search({
        index: 'talents',
        type: req.params._type,
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "match_all": {}
                    }
                }
            },
            "sort": [
                {
                    "id": {
                        "order": "desc"
                    }
                }
            ],
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/talents', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": false}}
                            ],
                            "must_not": {
                                "term": {"email": req.body.email}
                            }
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/mentors/query', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": true}}
                            ]
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/talents/query', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": false}}
                            ]
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/mentors', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": true}}
                            ],
                            "must_not": {
                                "term": {"email": req.body.email}
                            }
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/elastic/people', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": true}}
                            ]
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/elastic/groups', function (req, res) {

    client.search({
        index: 'talents',
        type: 'groups',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "match_all": {}
                    }
                }
            },
            "sort": [
                {
                    "id": {
                        "order": "desc"
                    }
                }
            ],
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/:_type/search', function (req, res) {

    var _q = {};
    _q[req.body.field] = req.body.value;

    client.search({
        index: 'talents',
        type: req.params._type,
        body: {
            "query": {
                "match": _q
            }
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/job/query', function (req, res) {

    client.search({
        index: 'talents',
        type: 'job',
        fields: ["title", "description"],
        q: req.body.search_q
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._id;
        });
        client.mget({
            index: 'talents',
            type: 'job',
            body: {
                ids: result
            }
        }, function (error, response) {
            if (response.error) {
                res.json({"response": [], "error": null, "total": 0});
            }
            else {
                var output = response.docs.map(function (item) {
                    return item._source;
                });
                res.json({"response": output, "error": null, "total": response.docs.length});
            }
        });
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/connections/:email/add', isAuthenticated, function () {
    try {
        redis_client.hmset(req.params.email + '_' + req.body.email, req.body);
        redis_client.sadd("connections_" + req.params.email, req.params.email + '_' + req.body.email);
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.post('/connections/:email/get', isAuthenticated, function (req, res) {
    Connection.paginate({creator: req.params.email}, {page: req.body.page, limit: 20}).then(function (result) {
        res.json({"response": result});
    });
});

app.post('/notifications/record', isAuthenticated, function (req, res) {
    try {
        var _id = new Date().getTime();
        redis_client.hmset(req.body.user + '_' + _id, {
            "id": req.body.user + '_' + _id,
            'actor': req.body.actor,
            'action': req.body.action,
            'verb': req.body.verb,
            'subject': req.body.subject,
            'message': req.body.message,
            'image_url': req.body.image_url
        });
        redis_client.sadd("notifications_" + req.body.user, req.body.user + '_' + _id);
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.get('/notifications/:email/get', function (req, res) {
    redis_client.smembers("notifications_" + req.params.email, function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/notification/:id/get', function (req, res) {
    redis_client.hgetall(req.params.id, function (status, data) {
        res.json({"response": data, 'error': status});
    });
});

app.get('/group/:id/get', function (req, res) {
    Group.find({group_name: req.params.email}, function (err, members) {
        res.json({"response": members, 'error': err});
    });
    // redis_client.smembers("groups_" + req.params.id, function (err, keys) {
    //     MHGETALL(keys, function (err, arr) {
    //         var acts = arr.map(function (res) {
    //             if (res[0] == null) {
    //                 return toObject(res[1]);
    //             }
    //         });
    //         res.json({"response": acts, 'error': err});
    //     });
    // })
});

app.get('/connections/:email/get', function (req, res) {
    Connection.find({creator: req.params.email}).then(function (result) {
        res.json({"response": result});
    });
    // redis_client.smembers("connections_" + req.params.email, function (err, keys) {
    //     MHGETALL(keys, function (err, arr) {
    //         var acts = arr.map(function (res) {
    //             if (res[0] == null) {
    //                 return toObject(res[1]);
    //             }
    //         });
    //         res.json({"response": acts, 'error': err});
    //     });
    // })
});

app.get('/account/:email/requested', function (req, res) {
    redis_client.smembers(req.params.email + "_request", function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/groups/:email/get', function (req, res) {
    UserGroup.find({creator: req.params.email}, function (err, groups) {
        res.json({"response": groups, 'error': err});
    });
    // var _id = "groups_" + req.params.email;
    // redis_client.smembers(_id, function (err, keys) {
    //     MHGETALL(keys, function (err, arr) {
    //         var acts = arr.map(function (res) {
    //             if (res[0] == null) {
    //                 return toObject(res[1]);
    //             }
    //         });
    //         res.json({"response": acts, 'error': err});
    //     });
    // })
});

app.get('/mentors/:email/get', function (req, res) {
    UserMentor.find({creator: req.params.email}, function (err, mentors) {
        res.json({"response": mentors, 'error': err});
    });
    // var _id = "mentors_" + req.params.email;
    // redis_client.smembers(_id, function (err, keys) {
    //     MHGETALL(keys, function (err, arr) {
    //         var acts = arr.map(function (res) {
    //             if (res[0] == null) {
    //                 return toObject(res[1]);
    //             }
    //         });
    //         res.json({"response": acts, 'error': err});
    //     });
    // })
});

app.get('/groups/:email/requested', function (req, res) {
    var _id = req.params.email + "_group_request_";
    redis_client.smembers(_id, function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/mentor/:email/requested', function (req, res) {
    redis_client.smembers(req.params.email + "_mentor_request_", function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/mentor/:email/requests', function (req, res) {
    redis_client.smembers("mentor_requests_" + req.params.email, function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/mentor/:email/talents', function (req, res) {
    Mentor.find({creator: req.params.email}, function (err, talents) {
        res.json({"response": talents, 'error': err});
    });
    // redis_client.smembers("mentees_" + req.params.email, function (err, keys) {
    //     MHGETALL(keys, function (err, arr) {
    //         var acts = arr.map(function (res) {
    //             if (res[0] == null) {
    //                 return toObject(res[1]);
    //             }
    //         });
    //         res.json({"response": acts, 'error': err});
    //     });
    // })
});

app.get('/requested/:email/check', isAuthenticated, function (req, res) {
    redis_client.get("requested_" + req.params.email, function (data, status) {
        res.json({"response": status});
    })
});

app.get('/requested/:email/verification', isAuthenticated, function (req, res) {
    redis_client.get("verification_requested_" + req.params.email, function (data, status) {
        res.json({"response": status});
    })
});

app.post('/connections/accept', isAuthenticated, function (req, res) {

    // var datar = {
    //     "params": [
    //         {
    //             "name": "pri_name",
    //             "param_type": "string",
    //             "value": req.body.recipient
    //         },
    //         {
    //             "name": "pri_username",
    //             "param_type": "string",
    //             "value": req.body.username
    //         },
    //         {
    //             "name": "pri_email",
    //             "param_type": "string",
    //             "value": req.body.recipient_mail
    //         },
    //
    //         {
    //             "name": "pri_image",
    //             "param_type": "string",
    //             "value": req.body.image_url
    //         },
    //         {
    //             "name": "pri_mentor",
    //             "param_type": "string",
    //             "value": req.body.is_mentor == true ? 1: 0
    //         },
    //         {
    //             "name": "pri_admin",
    //             "param_type": "string",
    //             "value": req.body.email
    //         },
    //
    //         {
    //             "name": "sec_name",
    //             "param_type": "string",
    //             "value": req.body.name
    //         },
    //         {
    //             "name": "sec_username",
    //             "param_type": "string",
    //             "value": req.body.mentor_username
    //         },
    //         {
    //             "name": "sec_email",
    //             "param_type": "string",
    //             "value": req.body.email
    //         },
    //         {
    //             "name": "sec_image",
    //             "param_type": "string",
    //             "value": req.body.mentor_image_url
    //         },
    //         {
    //             "name": "sec_mentor",
    //             "param_type": "string",
    //             "value": req.body.mentor_check== true ? 1: 0
    //         },
    //         {
    //             "name": "sec_admin",
    //             "param_type": "string",
    //             "value": req.body.recipient_mail
    //         }
    //     ],
    //     "schema": {
    //         "_field_name_": "string"
    //     },
    //     "wrapper": "string",
    //     "returns": "string"
    // };
    // request({
    //     async: true,
    //     crossDomain: true,
    //     url: 'http://178.62.199.243:8180/api/v2/prime/_proc/AddConnection', //URL to hit
    //     method: 'POST',
    //     headers: {
    //         "cache-control": "no-cache",
    //         "x-dreamfactory-session-token": req.body.session,
    //         "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
    //     },
    //     json: datar
    // }, function (error, response, body) {
    //     redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
    //         if (status == 1) {
    //             redis_client.del(req.body.note_id);
    //             var ident = new Buffer(req.body.recipient_mail + '_request_' + req.body.email).toString('base64');
    //             redis_client.srem(req.body.recipient_mail + "_request", ident);
    //             redis_client.del(ident, function (da, sta) {
    //                 var msg = String(req.body.email);
    //                 io.emit('notification', {
    //                     email: msg
    //                 });
    //                 res.json({"response": true, 'error': null});
    //             });
    //         }
    //     })
    // });

    var conn = new Connection();
    conn.creator = req.body.email;
    conn.name = req.body.recipient;
    conn.is_mentor = req.body.is_mentor;
    conn.email = req.body.recipient_mail;
    conn.username = req.body.username;
    conn.image_url = req.body.image_url;
    conn.save();

    var _conn = new Connection();
    _conn.creator = req.body.recipient_mail;
    _conn.name = req.body.name;
    _conn.is_mentor = req.body.mentor_check;
    _conn.email = req.body.email;
    _conn.username = req.body.mentor_username;
    _conn.image_url = req.body.mentor_image_url;
    _conn.save();

    redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
        if (status == 1) {
            redis_client.del(req.body.note_id);
            var ident = new Buffer(req.body.recipient_mail + '_request_' + req.body.email).toString('base64');
            redis_client.srem(req.body.recipient_mail + "_request", ident);
            redis_client.del(ident, function (da, sta) {
                var msg = String(req.body.email);
                io.emit('notification', {
                    email: msg
                });
                res.json({"response": true, 'error': null});
            });
        }
    });

    // var identifier = new Buffer(req.body.email + '_connections_' + req.body.recipient_mail).toString('base64');
    // var recipient_identifier = new Buffer(req.body.recipient_mail + '_connections_' + req.body.email).toString('base64');
    // add request to requestee connection
    // redis_client.hmset(identifier, {
    //     "name": req.body.recipient,
    //     "username": req.body.username,
    //     "email": req.body.recipient_mail,
    //     "image_url": req.body.image_url,
    //     "is_mentor": req.body.is_mentor
    // });
    // redis_client.sadd("connections_" + req.body.email, identifier);

    // add requestee to requester connection
    // redis_client.hmset(recipient_identifier, {
    //     "name": req.body.name,
    //     "username": req.body.mentor_username,
    //     "email": req.body.email,
    //     "image_url": req.body.mentor_image_url,
    //     "is_mentor": req.body.mentor_check
    // });
    // redis_client.sadd("connections_" + req.body.recipient_mail, recipient_identifier);
});

app.post('/groups/accept', isAuthenticated, function (req, res) {

    // add group to user's groups
    // var _id = new Buffer(req.body.recipient_mail + '_group_' + req.body.name).toString('base64');
    // redis_client.hmset(_id, {
    //     "name": req.body.name,
    //     "summary": req.body.summary,
    //     "email": req.body.email,
    //     "image_url": req.body.image_url
    // });
    // redis_client.sadd("groups_" + req.body.recipient_mail, _id);
    //
    // // add user to group's members
    // var identifier = new Buffer(req.body.email + '_group_member_' + req.body.name).toString('base64');
    // redis_client.hmset(identifier, {
    //     "name": req.body.recipient,
    //     "username": req.body.username,
    //     "email": req.body.recipient_mail,
    //     "image_url": req.body.image_url,
    //     "is_mentor": req.body.is_mentor
    // });
    // redis_client.sadd("groups_" + req.body.name, identifier, function (d, s) {
    //     redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
    //         if (status == 1) {
    //             redis_client.del(req.body.note_id, function (dat, stat) {
    //                 var identifier = new Buffer(req.body.recipient_mail + '__group_request__' + req.body.email).toString('base64');
    //                 redis_client.srem(req.body.recipient_mail + "_group_request_", identifier, function (d, s) {
    //                     redis_client.del(identifier, function (da, sta) {
    //                         var msg = String(req.body.email);
    //                         io.emit('notification', {
    //                             email: msg
    //                         });
    //                         res.json({"response": true, 'error': null});
    //                     });
    //                 })
    //             });
    //         }
    //     })
    // });
    var user_grp = new UserGroup();
    user_grp.creator = req.body.recipient_mail;
    user_grp.name = req.body.name;
    user_grp.summary = req.body.summary;
    user_grp.email = req.body.email;
    user_grp.image_url = req.body.image_url;
    user_grp.save();

    var grp = new Group();
    grp.group_name = req.body.name;
    grp.name = req.body.recipient;
    grp.username = req.body.username;
    grp.is_mentor = req.body.is_mentor;
    grp.email = req.body.recipient_mail;
    grp.image_url = req.body.image_url;
    grp.save();

    redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
        if (status == 1) {
            redis_client.del(req.body.note_id, function (dat, stat) {
                var identifier = new Buffer(req.body.recipient_mail + '__group_request__' + req.body.email).toString('base64');
                redis_client.srem(req.body.recipient_mail + "_group_request_", identifier, function (d, s) {
                    redis_client.del(identifier, function (da, sta) {
                        var msg = String(req.body.email);
                        io.emit('notification', {
                            email: msg
                        });
                        res.json({"response": true, 'error': null});
                    });
                })
            });
        }
    })

});

app.post('/mentors/accept', isAuthenticated, function (req, res) {

    // add mentor to user's mentors
    // var _id = new Buffer(req.body.recipient_mail + '_mentor_' + req.body.email).toString('base64');
    // redis_client.hmset(_id, {
    //     "name": req.body.name,
    //     "email": req.body.email,
    //     "image_url": req.body.mentor_image_url,
    //     "username": req.body.mentor_username,
    //     "is_mentor": true
    // });
    // redis_client.sadd("mentors_" + req.body.recipient_mail, _id);
    //
    // // add user to group's members
    // var identifier = new Buffer(req.body.email + '_mentees_' + req.body.recipient_mail).toString('base64');
    // redis_client.hmset(identifier, {
    //     "name": req.body.recipient,
    //     "username": req.body.username,
    //     "email": req.body.recipient_mail,
    //     "image_url": req.body.image_url,
    //     "is_mentor": req.body.is_mentor
    // });
    //
    // redis_client.sadd("mentees_" + req.body.email, identifier);

    var user_ment = new UserMentor();
    user_ment.creator = req.body.recipient_mail;
    user_ment.name = req.body.name;
    user_ment.username = req.body.mentor_username;
    user_ment.is_mentor = true;
    user_ment.email = req.body.email;
    user_ment.image_url = req.body.mentor_image_url;
    user_ment.save();

    var mentor = new Mentor();
    mentor.creator = req.body.email;
    mentor.name = req.body.recipient;
    mentor.username = req.body.username;
    mentor.is_mentor = req.body.is_mentor;
    mentor.email = req.body.recipient_mail;
    mentor.image_url = req.body.image_url;
    mentor.save();

    redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
        if (status == 1) {
            redis_client.del(req.body.note_id);

            var ident = new Buffer(req.body.recipient_mail + '__mentor_request__' + req.body.email).toString('base64');
            redis_client.srem(req.body.recipient_mail + "_mentor_request_", ident);
            redis_client.del(ident);

            var ide = new Buffer(req.body.email + '_mentor_requests_' + req.body.recipient_mail).toString('base64');
            redis_client.srem("mentor_requests_" + req.body.email, ide);
            redis_client.del(ide);

            var msg = String(req.body.email);
            io.emit('notification', {
                email: msg
            });

            res.json({"response": true, 'error': null});
        }
    });

});

app.post('/connections/reject', isAuthenticated, function (req, res) {
    redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
        if (status == 1) {
            redis_client.del(req.body.note_id, function (dat, stat) {
                var identifier = new Buffer(req.body.recipient_mail + '_request_' + req.body.email).toString('base64');
                redis_client.srem(req.body.recipient_mail + "_request", identifier, function (d, s) {
                    redis_client.del(identifier, function (da, sta) {

                        var msg = String(req.body.email);
                        io.emit('notification', {
                            email: msg
                        });
                        res.json({"response": true, 'error': null});
                    });
                })
            });
        }
    })
});

app.post('/groups/reject', isAuthenticated, function (req, res) {
    redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
        if (status == 1) {
            redis_client.del(req.body.note_id, function (dat, stat) {
                var identifier = new Buffer(req.body.recipient_mail + '__group_request__' + req.body.email).toString('base64');
                redis_client.srem(req.body.recipient_mail + "_group_request_", identifier, function (d, s) {
                    redis_client.del(identifier, function (da, sta) {

                        var msg = String(req.body.email);
                        io.emit('notification', {
                            email: msg
                        });

                        res.json({"response": true, 'error': null});
                    });
                })
            });
        }
    })
});

app.post('/mentors/reject', isAuthenticated, function (req, res) {
    redis_client.srem("notifications_" + req.body.email, req.body.note_id, function (data, status) {
        if (status == 1) {

            // delete notification
            redis_client.del(req.body.note_id, function (dat, stat) {
            });

            // delete request
            var identifier = new Buffer(req.body.recipient_mail + '__mentor_request__' + req.body.email).toString('base64');
            redis_client.srem(req.body.recipient_mail + "_mentor_request_", identifier, function (d, s) {
            });
            redis_client.del(identifier, function (da, sta) {
            });

            var ident = new Buffer(req.body.email + '_mentor_requests_' + req.body.recipient_mail).toString('base64');
            redis_client.srem("mentor_requests_" + req.body.email, ident);
            redis_client.del(ident, function (da, sta) {
            });

            var msg = String(req.body.email);
            io.emit('notification', {
                email: msg
            });

            res.json({"response": true, 'error': null});
        }
    })
});

app.post('/connections/test', function (req, res) {

    var user_ment = new UserMentor();
    user_ment.creator = req.body.recipient_mail;
    user_ment.name = req.body.name;
    user_ment.username = req.body.mentor_username;
    user_ment.is_mentor = true;
    user_ment.email = req.body.email;
    user_ment.image_url = req.body.mentor_image_url;
    user_ment.save();

    var mentor = new Mentor();
    mentor.creator = req.body.email;
    mentor.name = req.body.recipient;
    mentor.username = req.body.username;
    mentor.is_mentor = req.body.is_mentor;
    mentor.email = req.body.recipient_mail;
    mentor.image_url = req.body.image_url;
    mentor.save();

    res.json({"response": true, 'error': null});
});

if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on("exit", function (worker, code, signal) {
        cluster.fork();
    });
} else {
    http.listen(3600, function () {
        console.log('Application listening on port 8081!')
    });
}