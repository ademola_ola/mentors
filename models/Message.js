const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const MessageSchema = mongoose.Schema({
        uniqueID: String,
        sender_url: String,
        recipient_url: String,
        sender_username: String,
        sender_name: String,
        sender_email: String,
        recipient_username: String,
        text: String
    },
    {
        timestamps: true
    });

module.exports = mongoose.model('Message', MessageSchema);