/**
 * Created by stikks-workstation on 1/14/17.
 */
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var groupSchema = mongoose.Schema({
        group_name: String,
        email: String,
        is_mentor: Boolean,
        username: String,
        image_url: String,
        name: String
    },
    {
        timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

groupSchema.plugin(mongoosePaginate);

var Group = mongoose.model('Group', groupSchema);

module.exports = Group;