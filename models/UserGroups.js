/**
 * Created by stikks-workstation on 2/4/17.
 */

var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var user_groupSchema = mongoose.Schema({
        creator: String,
        email: String,
        name: String,
        summary: String,
        image_url: String
    },
    {
        timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

user_groupSchema.plugin(mongoosePaginate);

var UserGroup = mongoose.model('UserGroup', user_groupSchema);

module.exports = UserGroup;