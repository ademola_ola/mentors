﻿(function () {
    'use strict';

    var module = angular.module('app', ['ngRoute', 'angularMoment', 'dynamicNumber', 'ngIdle']);

   //module.constant('S_URL', 'https://services.newprudential.com');
   module.constant('S_URL', 'http://178.62.199.243:8180');

    module.config(function (IdleProvider, KeepaliveProvider) {
        IdleProvider.idle(600); // 10 minutes idle
        IdleProvider.timeout(30); // after 30 seconds idle, time the user out
        KeepaliveProvider.interval(300); // 5 minute keep-alive ping
        //     KeepaliveProvider.http('http://192.168.10.44/api/v2/user/session')
    });

    module.run(['Idle', function (Idle) {
        Idle.watch();
        //console.log('hshh')

        //$scope.$on('IdleStart', function () {
        //    console.log('started')
        //});

        //$scope.$on('IdleTimeout', function () {
        //    console.log('timedout')
        //});

        //$scope.$on('IdleEnd', function () {
        //    console.log('stopped')
        //});
    }]);


    module.controller('AccountController', function ($scope, $http, $rootScope, Idle, Keepalive, S_URL, $q) {

        $scope.$on('IdleStart', function () {
            console.log('started')
            $('#modaltimeout').openModal();
        });

        $scope.$on('IdleTimeout', function () {
            console.log('timedout')
            $('#modaltimeout').closeModal();
            $scope.logout();
        });

        $scope.$on('IdleEnd', function () {
            console.log('stopped')
            $('#modaltimeout').closeModal();
            $scope.renew();
        });

        //User management
        $scope.renew = function () {
            var url = S_URL + "/api/v2/user/session";

            $http({
                method: 'GET',
                url: url,
                headers: {
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                }
            }).success(function (data) {
                console.log('PING!!')
            }).
            error(function (error) {
                //console.log(error);
                //   Materialize.toast("Error Processing Request. Please try again.", 5000)
                $scope.logout();
            });
        }

        $scope.logout = function () {
            $scope.starter = false
            var url = S_URL + "/api/v2/user/session"

            $http({
                method: 'DELETE',
                url: url,
                headers: {
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                }
            }).success(function (data) {
                $scope.Auth = []
                $scope.ace = []
                window.location.href = 'login.html'
                //console.log(data)
            }).
            error(function (error) {
                console.log(error);
                Materialize.toast("Error Processing Request. Please try again.", 5000)
            });
        }

        $scope.resetpword = function () {
            if ($scope.res.newpass != $scope.res.cpass) {
                swal("Oops!", "New Passwords do not match!", "error");
            }
            else {
                $scope.updateAuthpass();
            }

        }

        $scope.updateAuthpass = function () {
            $scope.chload = true;
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_Resetpassself";

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "cache-control": "no-cache",
                    "x-dreamfactory-api-key": "68181bd08ebbc58257151383f0a9142f85dfdcbd935a466a464ae4f7d0c7a57a",
                    "x-dreamfactory-session-token": $scope.ace.session
                },
                data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    },\r\n    {\r\n      \"name\": \"oldPassword\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.res.oldpass + "\"\r\n    },\r\n    {\r\n      \"name\": \"newpassword\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.res.newpass + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            }).success(function (data) {
                $scope.updatepass();
            }).
            error(function (error) {
                console.log(error);
                Materialize.toast('Error Processing Request. Please try again later.', 5000);
            });
        }

        $scope.updatepass = function () {
            var url = S_URL + "/api/v2/user/password";
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"old_password\": \"" + $scope.res.oldpass + "\",\r\n  \"new_password\": \"" + $scope.res.newpass + "\",\r\n  \"email\": \"" + $scope.ace.email + "\",\r\n  \"code\": \"\"\r\n}"
            }).success(function (data) {
                $scope.chload = false
                $scope.clearset();
                swal({ title: "Done!", text: "Password reset successfully!", type: "success", showCancelButton: false, confirmButtonColor: "#DD6B55", confirmButtonText: "OK", closeOnConfirm: false }, function () { $scope.logout(); });

            }).
            error(function (error) {
                //console.log(error);
                Materialize.toast('Error Processing Request. Please try again later.', 5000);
            });
        }

        $scope.clearset = function () {
            $scope.chload = false;
            $('#modalpass').closeModal();
            $('#modalpin').closeModal();
            $scope.res = []
        }

        $scope.resetpin = function () {
            if ($scope.res.newpin != $scope.res.cpin) {
                swal("Oops!", "New PINs do not match!", "error");
            }
            else {
                $scope.chload = true;
                var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_updtpinold"

                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"oldPIN\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.res.oldpin + "\"\r\n    },\r\n    {\r\n      \"name\": \"newPIN\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.res.newpin + "\"\r\n    },\r\n{\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
                }).success(function (data) {
                    //     console.log(data)
                    $scope.chload = false;
                    if (data[0].RESPONSE == 0) {
                        swal("Done!", "PIN was changed Successfully!", "success")
                        $scope.clearset();
                    }
                    else {
                        swal("Oops!", "It seems your Old PIN is invalid", "error")
                    }

                }).
        error(function (error) {
            //  console.log(error);
            Materialize.toast('Error Processing Request. Please try again later.', 5000);
        });
            }
        }

        $scope.sendpin = function () {
            $scope.chload = true;
            var url = S_URL + "/api/v2/npmbbusiness/resetPIN";

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n \"userid\":\"" + $scope.ace.id + "\"\r\n}"
            }).success(function (data) {
                $scope.chload = false;
                console.log(data)
                if (data.resetPINResult == 0) {
                    swal("Done!", "New PIN has been sent to your email address", "success")
                    $scope.clearset();
                }
                else {
                    swal("Oops!", "Problem Resetting PIN. Please try again later.", "error")
                    $scope.clearset();
                }

            }).
            error(function (error) {
                Materialize.toast("Error Processing Request", 5000);
            });
        }

        $scope.init = function () {
            $scope.getparams()
            $scope.isAuthenticated();
            //    $scope.getip();
            //   $scope.initialize_db()
            $('.fixed-action-btn').openFAB();

        }

        $scope.initappr = function () {
            $scope.getparams()
            $scope.isAuthenticatedAppr();
            //    $scope.getip();
            //   $scope.initialize_db()
            $('.fixed-action-btn').openFAB();
        }

        $scope.getparams = function () {
            var query = window.location.search.substring(1)
            //console.log(query)
            $scope.ace = []
            $scope.ace.session = query.split('&')[0];
            $scope.ace.name = query.split('&')[2];
            $scope.ace.id = query.split('&')[1];
            $scope.ace.bizid = query.split('&')[3]
            console.log($scope.ace)
        }

        $scope.inisetup = function () {
            $scope.lclvl1 = true;
            $scope.lclvl2 = false;
            $scope.scotland = false;
            $scope.chload = false;
            $scope.clearintra();
            //   console.log($sessionStorage.Auth)
        }

        $scope.isAuthenticated = function () {
            var url = S_URL + "/api/v2/user/session"

            $http({
                method: 'GET',
                url: url,
                headers: {
                    "cache-control": "no-cache",
                    "x-dreamfactory-session-token": $scope.ace.session
                }
            }).success(function (data) {

                $scope.ace.email = data.email
                $scope.ace.name = data.last_name + ',' + data.first_name
                // console.log($scope.ace)
                $scope.inisetup();
                $scope.getacccount();
                $scope.getbanks();
                $scope.getbenNIP();
                $scope.getbenNPMB();
                $scope.getapprovaloffc();
                //   $scope.getaccountoffc();
                //    $scope.getbalance();
                //   $scope.getcard();
                $scope.starter = true;
            }).
           error(function (error) {
               console.log(error);
               window.location.href = 'login.html'
           });
        }

        $scope.isAuthenticatedAppr = function () {
            var url = S_URL + "/api/v2/user/session"

            $http({
                method: 'GET',
                url: url,
                headers: {
                    "cache-control": "no-cache",
                    "x-dreamfactory-session-token": $scope.ace.session
                }
            }).success(function (data) {

                $scope.ace.email = data.email
                $scope.ace.name = data.last_name + ',' + data.first_name
                // console.log($scope.ace)
                $scope.inisetup();
                $scope.getacccount();
                $scope.getapprcount();
             //   $scope.getbanks();
              //  $scope.getbenNIP();
               // $scope.getbenNPMB();
              //  $scope.getapprovaloffc();
                //   $scope.getaccountoffc();
                //    $scope.getbalance();
                //   $scope.getcard();
                $scope.starter = true;
            }).
           error(function (error) {
               console.log(error);
               window.location.href = 'login.html'
           });
        }

        //Accounts
        $scope.getbalance = function () {
            var url = S_URL + "/api/v2/v1/_proc/ibankGetBalanceTable";

            $scope.france = true;
            $scope.france2 = false;
            $('#modalbal').openModal();

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            }).success(function (data) {
                $scope.france = false;
                $scope.france2 = true;
                $scope.bals = data
            }).
            error(function (error) {
                //  console.log(error);
                $scope.france = false;
                $scope.france2 = true;
            });

        }

        $scope.getacccount = function () {
            var url = S_URL + "/api/v2/v1/_proc/ibankGetAcct";
            $scope.trfaccount = null;
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            //console.log(datar)

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                $scope.accounts = data
                // console.log(data)
                $('select').material_select();
            }).
            error(function (error) {
                console.log(error);
            });

        }

        $scope.getaccountoffc = function () {
            var url = S_URL + "/api/v2/v1/_proc/ibankGetAccOffc";

            $scope.france = true;
            $scope.france2 = false;
            $('#modalAccofc').openModal();

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            }).success(function (data) {
                $scope.accos = data
                $scope.france = false;
                $scope.france2 = true;
            }).
            error(function (error) {
                console.log(error);
                $scope.france = false;
                $scope.france2 = true;
            });

        }

        $scope.getapprovaloffc = function () {
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_GetApprovOffc";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"BizID\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"userid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"string\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            //console.log(datar)
            $scope.tnd = true;
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {

                $scope.appr = data
                $scope.apprv1 = ''
                $scope.apprv2 = ''
                if (data[0].count == 1) {
                    $scope.tnd = false;
                }
                else {
                    $scope.tnd = true;
                }
                // console.log(data)
              //  $('select').material_select();
            }).
            error(function (error) {
                $scope.tnd = true;
                console.log(error);
            });

        }

        //Approvals
        $scope.getapprcount = function () {
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_GetApprCount";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"BizID\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"userid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"string\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
             //   console.log(data[0][0].single)
                $scope.alg = data[0][0].single;
            }).
            error(function (error) {
                console.log(error);
            });

        }

        $scope.opensingappr = function () {
            $('#modalsingappr').openModal();
            $scope.getawtapprovals();
        }

        $scope.getawtapprovals = function () {
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_GetAwaitingAppr";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"BizID\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"userid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"string\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"

            $scope.seg = true;
            $scope.seg2 = false;

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                $scope.sapprs = data
                $scope.seg = false;
                $scope.seg2 = true;
            }).
            error(function (error) {
                $scope.seg = false;
                $scope.seg2 = true;
                console.log(error);
            });

        }

        $scope.singleapprdeets = function (trans) {
            $scope.transdt = trans
            //console.log($scope.transdt)
            $('#modalsingapprdeet').openModal();
        }

        $scope.genapprtoken = function (amt) {
           swal({
                title: "Generate OTP",
                text: "Please enter transaction PIN",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                animation: "slide-from-top"
            },
                function (inputValue) {
                    if (inputValue === false) return false;

                    if (inputValue === "") {
                        swal.showInputError("No PIN Entered!");
                        return false
                    }
                    $scope.tokenpin = inputValue
                    var pin = inputValue;
                    var url = S_URL + "/api/v2/npmbbusiness/generateOTP";
                    var datar = "{\n \"PIN\":\"" + pin + "\",\n \"amount\":\"" + amt + "\",\n \"userid\":\"" + $scope.ace.id + "\"\n}"
                   console.log(datar)
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {
                            "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                            "x-dreamfactory-session-token": $scope.ace.session,
                            "cache-control": "no-cache"
                        },
                        data: datar
                    }).success(function (data) {
                     //   console.log(data.generateOTPResult)
                        if (data.generateOTPResult == 0) {
                            swal("Done!", "OTP has been sent to your registered phone/email", "success");
                        }
                        else if (data.generateOTPResult == 81) {
                            swal({
                                title: "Oops!", text: "Incorrect PIN", type: "error", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Send me a new PIN", cancelButtonText: "No, Cancel", closeOnConfirm: false, closeOnCancel: false, showLoaderOnConfirm: true
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    $scope.sendpin();
                                }
                                else {
                                    $scope.clearintra();
                                    swal("Cancelled", "Transaction has been cancelled!", "error");
                                }
                            });
                            //    sweetAlert("Oops!", 'Incorrect PIN', "error");
                        }
                        else {
                            $scope.loadingf = false
                            Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                        }

                    }).
                    error(function (error) {
                        console.log(error);
                        $scope.loadingf = false
                    });


                });
        }

        $scope.approvetrans = function () {
            $scope.loadingf = true;
            var url = S_URL + "/api/v2/npmbbusiness/TransAuth";
            var datar = "{\n \"cusid\":\"" + $scope.ace.bizid + "\",\n \"userid\":\"" + $scope.ace.id + "\",\n \"transid\":\"" + $scope.transdt.seq + "\",\n \"amt\":\"" + $scope.transdt.amount + "\",\n \"PIN\":\"" + $scope.tokenpin + "\",\n \"otp\":\"" + $scope.transdt.otp + "\"\n}"
            // console.log(datar)
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                console.log(data)
                $scope.loadingf = false;
                if (data.TransAuthResult == "00") {
                    swal("Done", "Trasanction Approved", "success");
                    $('#modalsingapprdeet').closeModal();
                    $scope.getapprcount();
                    $scope.getawtapprovals();
                }
                else {
                     Materialize.toast('Oops! Error processing request. Please try again later', 5000);
                }

            });
        }

        $scope.declinetrans = function () {
            swal({
                title: "Decline Transaction",
                text: "Reason for rejection:",
                type: "input",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                closeOnConfirm: false,
                animation: "slide-from-top"
            },
            function (inputValue) {
                if (inputValue === false) return false;

                if (inputValue === "") {
                    swal.showInputError("You need state a reason!");
                    return false
                }

                $scope.reason = inputValue;
                var url = S_URL + "/api/v2/npmbbusiness/TransReject";
                var datar = "{\n \"cusid\":\"" + $scope.ace.bizid + "\",\n \"userid\":\"" + $scope.ace.id + "\",\n \"transid\":\"" + $scope.transdt.seq + "\",\n \"reason\":\"" + $scope.reason + "\"\n}"
                // console.log(datar)
                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: datar
                }).success(function (data) {
                    //console.log(data.TransRejectResult)
                    if (data.TransRejectResult == "00") {
                        swal("Trasanction Declined", "Reason: " + inputValue, "success");
                        $('#modalsingapprdeet').closeModal();
                        $scope.getapprcount();
                        $scope.getawtapprovals();
                    }
                    else {
                        Materialize.toast('Oops! Error processing request. Please try again later', 5000);
                    }

                });
            });
        }

        //Bulk Trans
        $scope.parseExcel = function(file){
            var f = document.getElementById('xlf').files[0];
            var reader = new FileReader();

            reader.onload = function(e){
               // console.log(e.target.result)
                var data = e.target.result;
             //   console.log(data);
                var workbook = XLSX.read(data, {type : 'binary'});

                workbook.SheetNames.forEach(function(sheetName){
                    // Here is your object
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    console.log(XL_row_object)
                    //var json_object = JSON.stringify(XL_row_object);
                    //console.log(json_object);
                    $scope.casa = XL_row_object
                    $scope.$digest()
                })

            };

            reader.onerror = function(ex){
                console.log(ex);
            };

            reader.readAsBinaryString(f);
        }

        $scope.validatebatch = function () {
            var updateScope = function () {
                console.log(ire)
            };

            var i

            //for (i = 0; i < 10; i++) {
            //    console.log(i)
            //}

            for (ire = 0; ire < $scope.casa.length; ire++) {
                var item = $scope.casa[i]
               // console.log (item.Account + ' :' +i)
                if (item.BankCode == '561') {
                     var url = S_URL + "/api/v2/v1/_proc/ibankGetAccountName";
                     var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"accountno\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.casa[i].Account + "\"\r\n    },\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
                     console.log(ire)
                    //  console.log('got here: ' + datar)
                        $http({
                            //async: true,
                            //crossDomain: true,
                            method: 'POST',
                            url: url,
                            headers: {
                                "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                                "x-dreamfactory-session-token": $scope.ace.session,
                                "cache-control": "no-cache"
                            },
                            data: datar
                        }).success(function (data) {
                            // console.log(data)
                            if (data.length != 0) {
                                item.Name = data[0].accountdesc
                                updateScope();
                                console.log(ire)
                            //    console.log(item)
                            }
                            else {
                                item = "Account Number is not correct";
                                // return result;
                            }
                        })
                        .error(function () {
                            item = 'Problem getting Account name'
                            Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                        });


                }
                else {

                }

            //    console.log(item.Name)
            //    $scope.$digest()
            }
        }

        $scope.validate = function () {
            var i = 0;
            var count = $scope.casa.length
            $scope.batchtot = 0
            $scope.batchfee = 0
            $scope.dovalidate(i)
        }

        $scope.dovalidate = function (co) {
            var i = co;
            var count = $scope.casa.length - 1
            var item = $scope.casa[i]

            var defer = $q.defer()

            if (item.BankCode == '561') {
                var url = S_URL + "/api/v2/v1/_proc/ibankGetAccountName";
                var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"accountno\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + item.Account + "\"\r\n    },\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
                //console.log(i)
                //console.log(datar)
                var getname = function () {
                    $http({
                        async: true,
                        crossDomain: true,
                        method: 'POST',
                        url: url,
                        headers: {
                            "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                            "x-dreamfactory-session-token": $scope.ace.session,
                            "cache-control": "no-cache"
                        },
                        data: datar
                    }).success(function (data) {
                      //  console.log(data)
                        if (data.length != 0) {
                            item.Name = data[0].accountdesc
                            item.Fee = 0
                            item.BankName = 'New Prudential'
                            item.Status = 'Valid'
                            $scope.batchtot = parseFloat($scope.batchtot) + parseFloat(item.Amount)
                            $scope.batchfee = parseFloat($scope.batchfee) + parseFloat(item.Fee)
                            $scope.batchcount = i + 1
                           // console.log($scope.newbatchtot)
                            if (i < count) {
                                i++
                                $scope.dovalidate(i)
                            }
                        }
                        else {
                            item.Name = "Account Number is not correct";
                            item.Status = 'Invalid'
                            if (i < count) {
                                i++
                                $scope.dovalidate(i)
                            }
                        }
                    })
                      .error(function () {
                          item.Name = 'Problem getting Account name'
                          Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                      });
                }

                defer.promise
                 .then(function () {
                     getname()
                 })
                  .then(function () {
                      // console.log(item)
                  })
                  .catch(function () {
                      Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 3000);
                  });

                defer.resolve();
            }

        }

        $scope.savebatch = function () {
            if (($scope.batchOacct != undefined) && ($scope.batchRemark != undefined)) {
                var i = 0
                var url = S_URL + "/api/v2/npmbbusiness/Batchtrf";
                var datar = {
                    "cusid": $scope.ace.bizid,
                    "userid": $scope.ace.id,
                    "frmAcct": $scope.batchOacct,
                    "count": $scope.batchcount.toString(),
                    "amt": $scope.batchtot.toString(),
                    "fee": $scope.batchfee.toString(),
                    "remark": $scope.batchRemark,
                    "ip": ""
                }

                datar = JSON.stringify(datar)
                //  console.log(datar)
                $scope.riri2 = true
                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: datar
                }).success(function (data) {
                    // console.log(data.BatchtrfResult)
                    $scope.dosavebatch(data.BatchtrfResult, i)
                }).
                error(function (error) {
                    $scope.riri2 = false
                    console.log(error);
                });
            }
            else {
                Materialize.toast('Please select an Originating Account and add Batch Remarks to complete the process.', 5000);
            }

        }

        $scope.dosavebatch = function (id,co) {
            var bid = id
            var i = co
            var count = $scope.casa.length - 1
            var url = S_URL + "/api/v2/npmbbusiness/Batchtrfdetails";
            var item = $scope.casa[i]

            var datar = {
                "batchid": bid,
                "payee": item.Name,
                "account": item.Account,
                "amount": item.Amount.toString(),
                "fee": item.Fee.toString(),
                "remark": item.Remark,
                "bank": item.BankCode,
                "cusid": $scope.ace.bizid
            }

            datar = JSON.stringify(datar)
            //console.log(datar)

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
               // console.log(data)
                if (data.BatchtrfdetailsResult == "00") {
                    item.Status = 'Saved'
                    if (i < count) {
                        i++
                        $scope.dosavebatch(bid,i)
                    }
                    else {
                        $scope.riri2 = false
                        swal({
                            title: "Success",
                            text: "Batch Saved",
                            type: "success",
                            showCancelButton: false
                        }, function () {
                            $scope.getbatches()
                            $('#modalbatchnew').closeModal();
                            })
                    }
                }
                else {
                    item.Status = 'Failed'
                    if (i < count) {
                        i++
                        $scope.dosavebatch(bid, i)
                    }
                    else {
                        $scope.riri2 = false
                        swal({
                            title: "Failed",
                            text: "Unable to complete Batch processing.",
                            type: "error",
                            showCancelButton: false
                        }, function () {
                            //delete batch
                            $scope.getbatches()
                            $('#modalbatchnew').closeModal();
                        })
                    }
                }

            }).
            error(function (error) {
                item.Status = 'Failed'
                if (i < count) {
                    i++
                    $scope.dosavebatch(bid, i)
                }
                else {
                    $scope.riri2 = false
                    swal({
                        title: "Failed",
                        text: "Unable to complete Batch processing.",
                        type: "error",
                        showCancelButton: false
                    }, function () {
                        //delete batch
                        $scope.getbatches()
                        $('#modalbatchnew').closeModal();
                    })
                }
            });
        }

        $scope.getnamelocal = function (acct) {
            var result = ''
            var account = acct
            var url = S_URL + "/api/v2/v1/_proc/ibankGetAccountName";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"accountno\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + account + "\"\r\n    },\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"

        //  console.log('got here: ' + datar)
            $http({
                async: true,
                crossDomain: true,
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
              // console.log(data)
                if (data.length != 0) {
                    result = data[0].accountdesc
                    return result;
                }
                else {
                    result = "Account Number is not correct";
                    return result;
                }
            })
            .error(function () {
                result = 'Problem getting Account name'
                Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
            });

            //console.log(result)
            //return result;
        }

        $scope.batchmenu = function () {
            $('#modalbatch').openmodal();
        }

        $scope.batchdeets = function (anc) {
            $scope.amt = anc.TotalAmt
            $scope.fee = anc.TotalFee
            $scope.co = anc.ItemCount
           console.log(anc)
            var id = anc.Seq

            var url = S_URL + "/api/v2/portalsrvc/_proc/GetBatchDetails";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"batchid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + id + "\"\r\n    },\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            console.log(datar)

            $http({
                async: true,
                crossDomain: true,
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                console.log(data)
                $scope.micasa = data
                $('#modalbatchdetails').openModal()

            })
            .error(function () {
                 Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
            });



        }

        $scope.getbatches = function () {
            $scope.riri = true
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_GetBatchPosted";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"userid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"

            //  console.log('got here: ' + datar)
            $http({
                async: true,
                crossDomain: true,
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                $scope.riri = false
                $scope.batches = data
            })
            .error(function () {
                $scope.riri = false
                Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
            });
        }

        $scope.initdelete = function (baid) {
            swal({
                title: "Delete Batch", text: "Are you sure you want to delete this batch?", type: "info",
                showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "DELETE",
                cancelButtonText: "No, Cancel", closeOnConfirm: false, closeOnCancel: true, showLoaderOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $scope.deletebatch(baid);
                }
            });
        }

        $scope.deletebatch = function (baid) {
            var id = baid
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_DeleteBatch";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"userid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    },\r\n    {\r\n      \"name\": \"batchid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + id + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"

            console.log('got here: ' + datar)
            $http({
                async: true,
                crossDomain: true,
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                if (data[0].Response == '00') {
                    swal("Done!", "Batch has been deleted.", "success")
                    $scope.getbatches()
                }
                else {
                    swal("Sorry!", "You can't delete a batch that has been sent for approval", "info")
                    $scope.getbatches()
                }

            })
            .error(function () {
                Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
            });
        }

        $scope.batchsubmit = function (batch) {
            $scope.sub = batch
            $('#batchsubmit').openModal()
        }

        $scope.dobatchsubmit = function (batch) {
            $scope.sub = batch
            console.log($scope.sub)

            var url = S_URL + "/api/v2/npmbbusiness/Batchtrfappr";

            if (angular.isUndefined($scope.remarks)) {
                $scope.Remark = ""
            }

            if (angular.isUndefined($scope.approval2)) {
                $scope.appr2 = ""
            }

            var datar1 = "{\r\n \"batchid\":\"" + $scope.sub.Seq + "\",\r\n \"cusid\":\"" + $scope.ace.bizid + "\",\r\n \"amt\":\"" + $scope.sub.TotalAmt + "\",\r\n \"pin\":\"" + $scope.tokenpin + "\",\r\n \"otp\":\"" + $scope.batchotp + "\",\r\n \"appr1\":\"" + $scope.appr1 + "\",\r\n \"appr2\":\"" + $scope.appr2 + "\"\r\n}"
            console.log(datar1)
            $scope.bobo = true

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar1
            }).success(function (data) {
                console.log(data)
                if ((data.BatchtrfapprResult.split('|')[0]) == '00') {
                    sweetAlert("Done!", 'Transaction sent for Approval', "success");
                    $scope.bobo = false
                    $scope.sub = []
                    $('#batchsubmit').closeModal()
                    $scope.getbatches()
                }
                else if ((data.BatchtrfapprResult.split('|')[0]) == '21') {
                    sweetAlert("Done!", 'Sent. But for failed to notify Approving officers.', "success");
                    $scope.bobo = false
                }
                else if (data.BatchtrfapprResult.split('|')[0] == '80') {
                    $scope.bobo = false
                    swal({
                        title: "Oops!", text: "OTP supplied is Invalid.", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Send new OTP", cancelButtonText: "Try Again", closeOnConfirm: false, closeOnCancel: true, showLoaderOnConfirm: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            var url = S_URL + "/api/v2/npmbbusiness/generateOTP";
                            $http({
                                method: 'POST',
                                url: url,
                                headers: {
                                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                                    "x-dreamfactory-session-token": $scope.ace.session,
                                    "cache-control": "no-cache"
                                },
                                data: "{\n \"PIN\":\"" + $scope.tokenpin + "\",\n \"amount\":\"" + $scope.sub.TotalAmt + "\",\n \"userid\":\"" + $scope.ace.id + "\"\n}"
                            }).success(function (data) {
                                console.log(data.generateOTPResult)
                                if (data.generateOTPResult == 0) {
                                    swal("Sent!", "A new OTP has been sent.", "success");
                                }
                                else if (data.generateOTPResult == 8) {
                                    $scope.loadingf = false
                                    sweetAlert("Oops!", 'Incorrect PIN', "error");
                                }
                                else {
                                    $scope.loadingf = false
                                    Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                                }

                            }).
                            error(function (error) {
                                console.log(error);
                            });

                        } else {
                            $scope.clearintra();
                            swal("Cancelled", "Your transaction has been cancelled.", "error");
                            $('#modaltrfnpmb').closeModal();
                            $scope.bobo = false
                        }
                    });
                }
                else {
                    $scope.loadingfc = false
                    sweetAlert("Oops!", data.BatchtrfapprResult.split('|')[1], "error");
                    $scope.bobo = false

                }
            }).
                error(function (error) {
                    console.log(error);
                    $scope.bobo = false
                    sweetAlert("Oops!", "Error Processing your Request", "error");
                });
        }



        //Beneficiaries
        $scope.getbenNPMB = function () {
            var url = S_URL + "/api/v2/portalsrvc/_proc/GetBeneficiary";

            $scope.trfbene = null;
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"type\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"NPMB\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            //console.log(datar)

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                $scope.locbeneflist = data
                //console.log(data)
            }).
            error(function (error) {
                //  console.log(error);
            });

        }

        $scope.getbenNIP = function () {
            var url = S_URL + "/api/v2/portalsrvc/_proc/GetBeneficiary";
            $scope.trfbene = null;
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"type\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"NIP\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            //console.log(datar)

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                $scope.beneflist = data
                //   console.log($scope.beneflist)
                //   console.log('got here')
            }).
            error(function (error) {
                console.log(error);
            });

        }

        $scope.AddBeneficiary = function () {
            var url = S_URL + "/api/v2/portalsrvc/_proc/AddBeneficiary";

            //   $scope.trfaccount = null;
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n    {\r\n      \"name\": \"type\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ben.type + "\"\r\n    },\r\n    {\r\n      \"name\": \"name\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ben.name + "\"\r\n    },\r\n    {\r\n      \"name\": \"account\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ben.account + "\"\r\n    },\r\n    {\r\n      \"name\": \"bank\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ben.bank + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            // console.log(datar)

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                $scope.bene = data
                // console.log(data)
            }).
            error(function (error) {
                //console.log(error);
            });

        }

        //Transactions
        $scope.getbanks = function () {
            var url = S_URL + "/api/v2/portalsrvc/_table/tblNIPBanks?fields=InstitutionCode%2CInstitutionName&filter=category<>0";
            $scope.trfaccount = null;
            $http({
                method: 'GET',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                }
            }).success(function (data) {
                //  console.log(data.resource)
                $scope.banks = data.resource
            }).
            error(function (error) {
                // console.log(error);
            });

        }

        $scope.trfmovenext = function () {
            if (!$scope.scotland) {
                Materialize.toast('Recipient Account number is not yet valid.', 5000);
            }
            else {
                var url = S_URL + "/api/v2/npmbbusiness/generateOTP";
                $scope.loadingf = true
                var datar = "{\n \"PIN\":\"" + $scope.pin + "\",\n \"amount\":\"" + $scope.amount + "\",\n \"userid\":\"" + $scope.ace.id + "\"\n}"
                console.log(datar)
                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: datar
                }).success(function (data) {
                    console.log(data.generateOTPResult)
                    if (data.generateOTPResult == 0) {
                        $scope.loadingf = false;
                        $scope.lclvl1 = false;
                        $scope.lclvl2 = true;
                    }
                    else if (data.generateOTPResult == 81) {
                        $scope.loadingf = false
                        swal({
                            title: "Oops!", text: "Incorrect PIN", type: "error", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Send me a new PIN", cancelButtonText: "No, cancel transaction", closeOnConfirm: false, closeOnCancel: false, showLoaderOnConfirm: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $scope.sendpin();
                            }
                            else {
                                $scope.clearintra();
                                swal("Cancelled", "Transaction has been cancelled!", "error");
                            }
                        });
                        //    sweetAlert("Oops!", 'Incorrect PIN', "error");
                    }
                    else {
                        $scope.loadingf = false
                        Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                    }

                }).
                error(function (error) {
                    console.log(error);
                    $scope.loadingf = false
                });
            }

        }

        $scope.instamovenext = function () {
            if (!$scope.scotland) {
                Materialize.toast('Recipient Account number is not yet valid.', 5000);
            }
            else {
                var url = S_URL + "/api/v2/npmbbusiness/generateOTP";

                $scope.loadingf = true
                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: "{\n \"PIN\":\"" + $scope.to.pin + "\",\n \"amount\":\"" + $scope.to.amount + "\",\n \"cusid\":\"" + $scope.ace.id + "\"\n}"
                }).success(function (data) {
                    console.log(data.generateOTPResult)
                    if (data.generateOTPResult == 0) {
                        $scope.loadingf = false;
                        $scope.lclvl1 = false;
                        $scope.lclvl2 = true;
                    }
                    else if (data.generateOTPResult == 81) {
                        $scope.loadingf = false
                        swal({
                            title: "Oops!", text: "Incorrect PIN", type: "error", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Send me a new PIN.", cancelButtonText: "No, cancel transaction", closeOnConfirm: false, closeOnCancel: false, showLoaderOnConfirm: true
                        }, function (isConfirm) {
                            if (isConfirm) {
                                $scope.sendpin();
                            }
                            else {
                                $scope.clearintra();
                                swal("Cancelled", "Transaction has been cancelled!", "error");
                            }
                        });
                    }
                    else {
                        $scope.loadingf = false
                        Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                    }

                }).
                error(function (error) {
                    console.log(error);
                    $scope.loadingf = false
                });
            }

        }

        $scope.intratrf = function () {
            var url = S_URL + "/api/v2/npmbbusiness/intraTrf";

            //Save beneficiary
            if ($scope.savben) {
                $scope.ben = []
                $scope.ben.type = 'NPMB';
                $scope.ben.name = $scope.name
                $scope.ben.account = $scope.toaccount
                $scope.ben.bank = 'NPMB'
                $scope.AddBeneficiary();

            }

            if (angular.isUndefined($scope.remarks)) {
                $scope.remarks = ""
            }

            if (angular.isUndefined($scope.apprv2)) {
                $scope.apprv2 = ""
            }

            var datar1 = "{\r\n \"cusid\":\"" + $scope.ace.bizid + "\",\r\n \"userid\":\"" + $scope.ace.id + "\",\r\n \"frmAcct\":\"" + $scope.trfaccount + "\",\r\n \"toAcct\":\"" + $scope.toaccount + "\",\r\n \"amt\":\"" + $scope.amount + "\", \r\n \"remark\":\"" + $scope.remarks + "\",\r\n \"PIN\":\"" + $scope.pin + "\",\r\n \"otp\":\"" + $scope.otp + "\",\r\n \"appr1\":\"" + $scope.apprv1 + "\",\r\n \"appr2\":\"" + $scope.apprv2 + "\",\r\n \"ip\":\"\"\r\n}"
            console.log(datar1)
            $scope.loadingfc = true
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar1
            }).success(function (data) {
                console.log(data)
                if ((data.intraTrfResult.split('|')[0]) == '00') {
                    sweetAlert("Done!", data.intraTrfResult.split('|')[1], "success");
                    $scope.loadingfc = false
                    $scope.clearintra()
                    $('#modaltrfnpmb').closeModal();
                }
                else if (data.intraTrfResult.split('|')[0] == '80') {
                    $scope.loadingfc = false
                    //  sweetAlert("Oops!", 'Invalid OTP', "error");
                    swal({
                        title: "Oops!", text: "OTP supplied is Invalid.", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Send new OTP", cancelButtonText: "Try Again", closeOnConfirm: false, closeOnCancel: true, showLoaderOnConfirm: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            var url = S_URL + "/api/v2/npmbbusiness/generateOTP";
                            $http({
                                method: 'POST',
                                url: url,
                                headers: {
                                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                                    "x-dreamfactory-session-token": $scope.ace.session,
                                    "cache-control": "no-cache"
                                },
                                data: "{\n \"PIN\":\"" + $scope.pin + "\",\n \"amount\":\"" + $scope.amount + "\",\n \"userid\":\"" + $scope.ace.id + "\"\n}"
                            }).success(function (data) {
                                console.log(data.generateOTPResult)
                                if (data.generateOTPResult == 0) {
                                    swal("Sent!", "A new OTP has been sent.", "success");
                                }
                                else if (data.generateOTPResult == 8) {
                                    $scope.loadingf = false
                                    sweetAlert("Oops!", 'Incorrect PIN', "error");
                                }
                                else {
                                    $scope.loadingf = false
                                    Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                                }

                            }).
                            error(function (error) {
                                console.log(error);
                            });

                        } else {
                            //$scope.clearintra();
                            //swal("Cancelled", "Your transaction has been cancelled.", "error");
                            //$('#modaltrfnpmb').closeModal();
                        }
                    });
                }
                else {
                    $scope.loadingfc = false
                    sweetAlert("Oops!", data.intraTrfResult.split('|')[1], "error");

                }

            }).
                error(function (error) {
                    console.log(error);
                    $scope.loadingfc = false
                    sweetAlert("Oops!", "Error Processing your Request", "error");
                });

        }

        $scope.instatrf = function () {
            var url = S_URL + "/api/v2/npmbbusiness/interTrf";

            //Save Beneficiary
            if ($scope.savben) {
                $scope.ben = []
                $scope.ben.type = 'NIP';
                $scope.ben.name = $scope.to.name
                $scope.ben.account = $scope.to.account
                $scope.ben.bank = $scope.trfbank
                $scope.AddBeneficiary();
            }

            if (angular.isUndefined($scope.to.remarks)) {
                $scope.to.remarks = ""
            }

            var datar1 = "{\n \"session\":\"" + $scope.to.session + "\",\n \"bank\":\"" + $scope.trfbank + "\",\n \"oaccount\":\"" + $scope.trfaccount + "\",\n \"baccount\":\"" + $scope.to.account + "\",\n \"bname\":\"" + $scope.to.name + "\",\n \"bbvn\":\"" + $scope.to.bvn + "\",\n \"amt\":\"" + $scope.to.amount + "\",\n \"remark\":\"" + $scope.to.remarks + "\",\n \"PIN\":\"" + $scope.to.pin + "\",\n \"otp\":\"" + $scope.to.otp + "\",\n \"ip\":\"" + '' + "\",\n \"cusid\":\"" + $scope.ace.id + "\"\n }"
            console.log(datar1)
            $scope.loadingfc = true
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar1
            }).success(function (data) {
                console.log(data)
                if ((data.InterTrfResult.split('|')[0]) == '00') {
                    sweetAlert("Done!", data.InterTrfResult.split('|')[1], "success");
                    $scope.loadingfc = false
                    $scope.clearintra()
                    $('#modaltrfother').closeModal();
                }
                else if (data.InterTrfResult.split('|')[0] == '80') {
                    $scope.loadingfc = false
                    //  sweetAlert("Oops!", 'Invalid OTP', "error");
                    swal({
                        title: "Oops!", text: "OTP supplied is Invalid.", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Send new OTP", cancelButtonText: "Cancel transaction", closeOnConfirm: false, closeOnCancel: false, showLoaderOnConfirm: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            var url = S_URL + "/api/v2/npmbbusiness/generateOTP";
                            $http({
                                method: 'POST',
                                url: url,
                                headers: {
                                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                                    "x-dreamfactory-session-token": $scope.ace.session,
                                    "cache-control": "no-cache"
                                },
                                data: "{\n \"PIN\":\"" + $scope.to.pin + "\",\n \"amount\":\"" + $scope.to.amount + "\",\n \"cusid\":\"" + $scope.ace.id + "\"\n}"
                            }).success(function (data) {
                                console.log(data.generateOTPResult)
                                if (data.generateOTPResult == 0) {
                                    swal("Sent!", "A new OTP has been sent.", "success");
                                }
                                else if (data.generateOTPResult == 8) {
                                    $scope.loadingf = false
                                    sweetAlert("Oops!", 'Incorrect PIN', "error");
                                }
                                else {
                                    $scope.loadingf = false
                                    Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                                }

                            }).
                                error(function (error) {
                                    console.log(error);
                                });

                        } else {
                            $scope.clearintra();
                            swal("Cancelled", "Your transaction has been cancelled.", "error");
                            $('#modaltrfother').closeModal();
                        }
                    });
                }
                else if (data.InterTrfResult.split('|')[0] == 'PP') {
                    $scope.loadingfc = false
                    sweetAlert("Transaction Not Completed!", "Please confirm Transaction has failed before attempting again to avoid been debited twice.", "error");
                    $('#modaltrfother').closeModal();
                }
                else {
                    $scope.loadingfc = false
                    sweetAlert("Transaction Failed!", data.InterTrfResult.split('|')[1], "error");
                    $('#modaltrfother').closeModal();
                }

            }).
                error(function (error) {
                    console.log(error);
                    $scope.loadingfc = false
                    sweetAlert("Transaction Failed!", "Error completing your Request. Destination bank may not be available", "error");
                });

        }

        $scope.localtrf = function () {
            $scope.loadingf = true
            if ($scope.trfaccount == $scope.trfaccount2) {
                sweetAlert("Oops!", "You can't transfer to the same account!", "error");
                $scope.loadingf = false
            }
            else {
                var url = S_URL + "/api/v2/npmbbusiness/LocalTrf";

                if (angular.isUndefined($scope.remarks)) {
                    $scope.remarks = ""
                }

                var datar1 = "{\r\n \"sacct\":\"" + $scope.trfaccount + "\",\r\n \"racct\":\"" + $scope.trfaccount2 + "\",\r\n \"amt\":\"" + $scope.lamount + "\",\r\n \"ip\":\"\",\r\n \"remark\":\"" + $scope.remarks + "\",\r\n \"cusid\":\"" + $scope.ace.id + "\"\r\n}"
                //console.log(datar1)
                //$scope.loadingf = false
                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: datar1
                }).success(function (data) {
                    if ((data.LocalTrfResult.split('|')[0]) == '00') {
                        $scope.loadingf = false
                        sweetAlert("Done!", data.LocalTrfResult.split('|')[1], "success");
                        $('#modaltrfown').closeModal();
                        $scope.clearintra();
                    }
                    else {
                        sweetAlert("Oops!", data.LocalTrfResult.split('|')[1], "error");
                        $scope.loadingf = false
                    }

                }).
                error(function (error) {
                    console.log(error);
                    sweetAlert("Oops!", "Error Processing your Request", "error");
                    $scope.loadingf = false
                });
            }

        }

        $scope.getaccountname = function () {
           // console.log($scope.toaccount)
            if ($scope.toaccount != '') {
                $scope.loading = true;
                $scope.name = "";
                var acct = "'" + $scope.toaccount + "'";
                var url = S_URL + "/api/v2/v1/_proc/ibankGetAccountName";
                var dara = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"accountno\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.toaccount + "\"\r\n    },\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.id + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}";
                console.log(dara)

                $http({
                    async: true,
                    crossDomain: true,
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: dara
                }).success(function (data) {
                    // console.log(data[0].accountdesc)
                    if (data.length != 0) {
                        $scope.name = data[0].accountdesc
                        $scope.loading = false;
                        $scope.scotland = true;
                    }
                    else {
                        $scope.name = "Account Number is not correct";
                        $scope.loading = false;
                        $scope.scotland = false;
                    }
                })
                .error(function () {
                    $scope.loading = false;
                    $scope.scotland = false;
                    Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                });
            }
        }

        $scope.getnameinstant = function () {
            if ($scope.to.account != null) {
                $scope.loading = true;
                var url = S_URL + "/api/v2/npmbbusiness/getnameenq";

                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                        "x-dreamfactory-session-token": $scope.ace.session,
                        "cache-control": "no-cache"
                    },
                    data: "{\r\n \"acctno\":\"" + $scope.to.account + "\",\r\n \"bank\":\"" + $scope.trfbank + "\"\r\n}"
                }).success(function (data) {
                    console.log(data)
                    if ((data.getnameenqResult.split('|')[0]) == '00') {
                        $scope.loading = false;
                        $scope.scotland = true;
                        $scope.to.name = data.getnameenqResult.split('|')[2]
                        $scope.to.session = data.getnameenqResult.split('|')[1]
                        $scope.to.bvn = data.getnameenqResult.split('|')[3]
                    }
                    else {
                        $scope.loading = false;
                        $scope.scotland = false;
                        $scope.to.name = data.getnameenqResult.split('|')[1]
                    }
                })
                .error(function () {
                    $scope.loading = false;
                    $scope.scotland = false;
                    Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
                });
            }
        }

        $scope.getnameinstantBen = function () {
            $scope.loading = true;
            $scope.trfbank = $scope.toben.split('|')[1]
            $scope.to.account = $scope.toben.split('|')[0]
            var url = S_URL + "/api/v2/npmbbusiness/getnameenq";

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n \"acctno\":\"" + $scope.to.account + "\",\r\n \"bank\":\"" + $scope.trfbank + "\"\r\n}"
            }).success(function (data) {
                console.log(data)
                if ((data.getnameenqResult.split('|')[0]) == '00') {
                    $scope.loading = false;
                    $scope.scotland = true;
                    $scope.to.name = data.getnameenqResult.split('|')[2]
                    $scope.to.session = data.getnameenqResult.split('|')[1]
                    $scope.to.bvn = data.getnameenqResult.split('|')[3]
                }
                else {
                    $scope.loading = false;
                    $scope.scotland = false;
                    $scope.to.name = data.getnameenqResult.split('|')[1]
                }
            })
            .error(function () {
                $scope.loading = false;
                $scope.scotland = false;
                Materialize.toast('There seems to be a problem connecting at the moment. Please try again later', 5000);
            });
        }

        $scope.clearintra = function () {
            $scope.lclvl1 = true;
            $scope.lclvl2 = false;
            $scope.lamount = "";
            $scope.amount = ""; $scope.lamount = "";
            $scope.name = ""
            $scope.pin = ""
            $scope.toaccount = "";
            $scope.remarks = "";
            $scope.otp = "";
            $scope.to = []
            $scope.loadingf = false;
            $scope.loadingfc = false;
            $scope.scotland = false;
            $('#modaltrfnpmb').closeModal();
            $('#modaltrfother').closeModal();
            $('#modaltrfown').closeModal();
            $scope.getacccount()
            $scope.getbenNIP();
            // $scope.$apply();
        }

        //Statement Page
        $scope.stmt = function () {
            window.location.href = 'statement.html?' + $scope.ace.session + '&' + $scope.ace.id + '&' + $scope.ace.name + '&' + $scope.ace.bizid
        }

        $scope.getstmt = function () {
            var url = S_URL + "/api/v2/v1/_proc/ibankGetAccountstmt";
            $scope.loadstmt = true;
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"acctno\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.staccount + "\"\r\n    },\r\n    {\r\n      \"name\": \"sdate\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.st.sdate + "\"\r\n    },\r\n    {\r\n      \"name\": \"ndate\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.st.ndate + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            }).success(function (data) {
                console.log(data.length)
                $scope.stmtbals = data
                $scope.loadstmt = false
            }).
            error(function (error) {
                console.log(error);
                $scope.loadstmt = false
            });
        }

        $scope.returnhome = function () {
            window.location.href = 'index.html?' + $scope.ace.session + '&' + $scope.ace.id + '&' + $scope.ace.name + '&' + $scope.ace.bizid
        }

        //Reports
        $scope.report = function () {
            window.location.href = 'reports.html?' + $scope.ace.session + '&' + $scope.ace.id + '&' + $scope.ace.name
        }

        $scope.trftoday = function () {
            $scope.france = true;
            $scope.france2 = false;
            $('#modaltrftoday').openModal();
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_SngTrans";
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"bizid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            }).success(function (data) {
                $scope.todays = data
                $scope.france = false;
                $scope.france2 = true;
            }).
            error(function (error) {
                console.log(error);
                $scope.france = false;
                $scope.france2 = true;
            });
        }

        $scope.trfhistory = function () {
            $scope.france = true;
            $scope.france2 = false;

            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_SngTransHist";
            $scope.hi.sdatec = moment($scope.hi.sdate).format('YYYY-MM-DD')
            $scope.hi.ndatec = moment($scope.hi.ndate).format('YYYY-MM-DD')
            var datar = "{\r\n  \"params\": [\r\n   {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.ace.bizid + "\"\r\n    },\r\n     {\r\n      \"name\": \"sdate\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.hi.sdatec + "\"\r\n    },\r\n     {\r\n      \"name\": \"ndate\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.hi.ndatec + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
          //  console.log(datar)
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.ace.session,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                $scope.hists = data
              console.log($scope.hists)
                $scope.france = false;
                $scope.france2 = true;
            }).
            error(function (error) {
                console.log(error);
                $scope.france = false;
                $scope.france2 = true;
            });
        }

        $scope.setSelected = function (idSelected) {
            //  $scope.idSelectedVote = idSelectedVote;
            console.log(idSelected)
        }

        $scope.trfdeets = function (trans) {
            $scope.sdt = trans
            $('#modalsngdt').openModal()
        }

        //
        $scope.NA = function () {
            swal("Sorry!", "This feature is not yet Available!", "info")
        }

        //$scope.totaldebit = function () {
        //    var total = 0;
        //    for (var i = 0; i < $scope.stmtbals.length; i++) {
        //        var stmt = $scope.stmtbals[i];
        //        total += stmt.Debit;
        //    }
        //    return total;
        //}

        //$scope.totalcredit = function () {
        //    var total = 0;
        //    for (var i = 0; i < $scope.stmtbals.length; i++) {
        //        var stmt = $scope.stmtbals[i];
        //        total += stmt.Credit;
        //    }
        //    return total;
        //}
    })

    module.controller('LoginController', function ($scope, $http, $rootScope, $filter, S_URL) {

        $scope.init = function () {
            //   $scope.initialize_db()
            $scope.getip()
            $scope.start = false
            $scope.part1 = true
        }


        $scope.UserAuth = function () {
            $scope.loading = true
            var url = S_URL + "/api/v2/portalsrvc/_proc/BizUserAuth";
            var datar = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.user.id + "\"\r\n    },\r\n    {\r\n      \"name\": \"bizid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.user.code + "\"\r\n    },\r\n    {\r\n      \"name\": \"password\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.user.password + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"string\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}";
           // console.log(datar)

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "beb04e2cc524e366a582c7bd72f1f57ef8ece549da5b9b79e8a6fb4e41d76839",
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                if (data[0].Response == 1) {
                    $scope.user.email = data[0].Email
                    $scope.user.role = data[0].Role
                    $scope.loginUser()
                }
                else if (data[0].Response == 2) {
                    $scope.user.email = data[0].Email
                    $('#modalupdatecreds').openModal();
                    $scope.loading = false
                }
                else if (data[0].Response == 6) {
                    swal("Too many attempts", "Your account has been locked out. Please contact customer service", "error");
                    $scope.loading = false
                }
                else {
                    swal("Oops!", "Invalid UserID/Password!", "error");
                    $scope.loading = false
                }

               // console.log(data)
                $scope.loading = false
            }).
            error(function (error) {
                $scope.loading = false
                swal("Oops!", "Problems connecting at the moment. Please try again later!", "error");
                console.log(error);
            });
        }

        $scope.loginUser = function () {
            var url = S_URL + "/api/v2/user/session";

            $scope.Auth = []
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"email\": \"" + $scope.user.email + "\",\r\n  \"password\": \"" + $scope.user.password + "\",\r\n  \"duration\": 3600\r\n}"
            }).success(function (data) {
                //  console.log(data)
                $scope.loading = false
                $scope.Auth.session = data.session_token;
                $scope.Auth.name = data.last_name + ',' + data.first_name
                $scope.Auth.id = $scope.user.id
                $scope.Auth.bizid = $scope.user.code
                //var load = { id: $scope.user.id, first_name: data.first_name, token: data.session_token }
                //console.log(load)
                //  $scope.saveAuth(load)
                // $sessionStorage.Auth = $scope.Auth
                $rootScope.id = $scope.user.id

                if ($scope.user.role == 3) {
                    window.location.href = 'appr.html?' + $scope.Auth.session + '&' + $scope.Auth.id + '&' + $scope.Auth.name + '&' + $scope.Auth.bizid
                }
                else {
                    window.location.href = 'index.html?' + $scope.Auth.session + '&' + $scope.Auth.id + '&' + $scope.Auth.name + '&' + $scope.Auth.bizid
                }
            }).
            error(function (error) {
                console.log(error);
                $scope.loading = false
            });
        }

        $scope.updatecreds = function () {
            if ($scope.pass != $scope.cpass) {
                swal("Oops!", "Passwords do not match!", "error");
            }
            else {
                var url = S_URL + "/api/v2/user/session";
                $scope.cload = true
                $scope.Auth = []
                $http({
                    method: 'POST',
                    url: url,
                    headers: {
                        "cache-control": "no-cache"
                    },
                    data: "{\r\n  \"email\": \"" + $scope.user.email + "\",\r\n  \"password\": \"" + $scope.user.password + "\",\r\n  \"duration\": 0\r\n}"
                }).success(function (data) {
                    //  console.log(data)
                    $scope.Auth.session = data.session_token;
                    $scope.Auth.name = data.first_name
                    // console.log($scope.Auth)
                    //var load = {id:$scope.user.id, first_name: data.first_name, token: data.session_token}
                    //console.log(load)
                    // $scope.saveAuth(load)
                    $scope.updateAuthpass();
                }).
                error(function (error) {
                    console.log(error);
                    $scope.cload = false
                    Materialize.toast('Error Processing Request. Please try again later.', 5000);
                });

                //$scope.updateAuthpass();
                //$scope.updatePIN();
            }
        }

        $scope.updateAuthpass = function () {
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_Resetpassself";
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "cache-control": "no-cache",
                    "x-dreamfactory-api-key": "68181bd08ebbc58257151383f0a9142f85dfdcbd935a466a464ae4f7d0c7a57a",
                    "x-dreamfactory-session-token": $scope.Auth.session
                },
                data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.user.id + "\"\r\n    },\r\n    {\r\n      \"name\": \"oldPassword\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.user.password + "\"\r\n    },\r\n    {\r\n      \"name\": \"newpassword\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.pass + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            }).success(function (data) {
                $scope.updatePIN();
            }).
            error(function (error) {
                console.log(error);
                $scope.cload = false
                Materialize.toast('Error Processing Request. Please try again later.', 5000);
            });
        }

        $scope.updatePIN = function () {
            $scope.seccan = $filter('lowercase')($scope.secca);
            console.log($scope.seccq)
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_updtpin" //"{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"newPIN\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.pin + "\"\r\n    },\r\n{\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.user.id +"\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            var datem = "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"newPIN\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.pin + "\"\r\n    },\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.user.id + "\"\r\n    },\r\n    {\r\n      \"name\": \"secq\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.seccq + "\"\r\n    },\r\n    {\r\n      \"name\": \"seca\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.seccan + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"

            $http({
                method: 'POST',
                url: url,
                headers: {
                    "cache-control": "no-cache",
                    "x-dreamfactory-session-token": $scope.Auth.session,
                    "x-dreamfactory-api-key": "68181bd08ebbc58257151383f0a9142f85dfdcbd935a466a464ae4f7d0c7a57a"
                },
                data: datem
            }).success(function (data) {
                $scope.updatesecprofile();
            }).
            error(function (error) {
                //  console.log(error);
                $scope.cload = false
                Materialize.toast('Error Processing Request. Please try again later.', 5000);
            });
        }

        $scope.updatepass = function () {
            var url = S_URL + "/api/v2/user/password";
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-session-token": $scope.Auth.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"old_password\": \"" + $scope.user.password + "\",\r\n  \"new_password\": \"" + $scope.pass + "\",\r\n  \"email\": \"" + $scope.user.email + "\",\r\n  \"code\": \"\"\r\n}"
            }).success(function (data) {
                // console.log('3 dance')
                $scope.cload = false
                swal("Done", "Update Successful!", "success");
                $('#modalupdatecreds').closeModal();
                // $scope.updatesecprofile();
            }).
            error(function (error) {
                //console.log(error);
                $scope.cload = false
                Materialize.toast('Error Processing Request. Please try again later.', 5000);
            });
        }

        $scope.updatesecprofile = function () {
            console.log($scope.seccq)

            var url = S_URL + "/api/v2/user/profile";
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-session-token": $scope.Auth.session,
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"email\": \"" + $scope.user.email + "\",\r\n  \"security_question\": \"" + $scope.seccq + "\",\r\n  \"security_answer\": \"" + $scope.seccan + "\"\r\n}"
            }).success(function (data) {
                $scope.updatepass();
            }).
            error(function (error) {
                //console.log(error);
                $scope.cload = false
                Materialize.toast('Error Processing Request. Please try again later.', 5000);
            });
        }

        $scope.resetnxt = function () {
            $scope.rload = true
            var url = S_URL + "/api/v2/portalsrvc/_proc/Biz_ChallengeResp";
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "beb04e2cc524e366a582c7bd72f1f57ef8ece549da5b9b79e8a6fb4e41d76839",
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"params\": [\r\n    {\r\n      \"name\": \"cusid\",\r\n      \"param_type\": \"string\",\r\n      \"value\": \"" + $scope.txtusr + "\"\r\n    }\r\n  ],\r\n  \"schema\": {\r\n    \"_field_name_\": \"string\"\r\n  },\r\n  \"wrapper\": \"\",\r\n  \"returns\": \"\"\r\n}"
            }).success(function (data) {
                $scope.rload = false
                if (data.length == 0) {
                    swal("Oops!", "Invalid User ID!", "error");
                }
                else {
                    $scope.txtsq = data[0].securityq
                    $scope.em = data[0].email
                    $scope.code = data[0].code
                    $scope.part1 = false
                    console.log(data[0])
                }

            }).
            error(function (error) {
                $scope.rload = false
                swal("Oops!", "Problems connecting at the moment. Please try again later!", "error");
                console.log(error);
            });
        }

        $scope.resetp = function () {
            $scope.rload = true
            $scope.seca = $filter('lowercase')($scope.txtsa);
            //      console.log($scope.seca)
            var url = S_URL + "/api/v2/user/password?login=true";
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "cache-control": "no-cache"
                },
                data: "{\r\n  \"email\": \"" + $scope.em + "\",\r\n  \"security_answer\":  \"" + $scope.seca + "\",\r\n  \"new_password\": \"" + $scope.code + "\"\r\n}"
            }).success(function (data) {
                console.log(data)
                $scope.sesh = data.session_token
                $scope.rload = false
                $scope.resetp2()
            }).
            error(function (error) {
                $scope.rload = false
                swal("Oops!", "Invalid Security Answer!", "error");
                console.log(error);
            });
        }

        $scope.resetp2 = function () {
            $scope.rload = true
            var url = S_URL + "/api/v2/npmbbusiness/Resetpass";
            var datar = "{\r\n  \"userid\":\"" + $scope.txtusr + "\",\r\n  \"cmail\":\"" + $scope.em + "\",\r\n  \"code\":\"" + $scope.code + "\"\r\n}"
            console.log($scope.code)
            console.log(datar)
            $http({
                method: 'POST',
                url: url,
                headers: {
                    "x-dreamfactory-api-key": "f7b1c4f261d1858d0238165a5a3fd03474bcb23e23f253748f9350798b8b909e",
                    "x-dreamfactory-session-token": $scope.sesh,
                    "cache-control": "no-cache"
                },
                data: datar
            }).success(function (data) {
                console.log(data)
                $scope.rload = false
                swal("Done!", "A new password has been sent to your email address!", "success");
                $scope.clearit()
            }).
            error(function (error) {
                $scope.rload = false
                swal("Oops!", "Problems connecting at the moment. Please try again later!", "error");
                console.log(error);
            });
        }

        $scope.getip = function (ip) {
            var json = 'https://ipv4.myexternalip.com/json';
            $http.get(json).then(function (result) {
                // console.log(result.data)
                $scope.ip = result.data.ip;
                if ($scope.ip == '41.75.92.242') {
                    //   window.location.href = 'http://192.168.10.19:802/ibank'
                }
            }, function (e) {
                $scope.local = 'false'
                console.log(error);
            });
        }

        $scope.clearit = function () {
            $scope.rload = false
            $scope.part1 = true
            $scope.txtsq = ""
            $scope.em = ""
            $scope.code = ""
            $scope.txtsa = ""
            $('#modalresetp').closeModal()
        }
    })
})();
