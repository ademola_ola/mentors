/**
 * Created by stikks-workstation on 2/4/17.
 */

var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var user_mentorSchema = mongoose.Schema({
        creator: String,
        email: String,
        name: String,
        username: String,
        image_url: String,
        is_mentor: Boolean
    },
    {
        timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

user_mentorSchema.plugin(mongoosePaginate);

var UserMentors = mongoose.model('UserMentors', user_mentorSchema);

module.exports = UserMentors;