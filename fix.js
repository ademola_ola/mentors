/**
 * Created by stikks-workstation on 2/4/17.
 */

var lupus = require('lupus');
var express = require('express');
var cluster = require("cluster");
var app = express();
var http = require('http').Server(app);
var request = require('request-promise');
var es = require('elasticsearch');

var client = new es.Client({
    host: '178.62.199.243:9200',
    log: 'trace'
});

client.ping({
    requestTimeout: Infinity,
    hello: "elasticsearch!"
}, function (error) {
    if (error) {
        console.trace('elasticsearch cluster is down!');
    } else {
        console.log('All is well');
    }
});

request({
    url: 'http://178.62.199.243:8180/rest/system/user', //URL to hit
    method: 'GET',
    headers: {
        "Authorization": 'Basic c3R5Y2NzQGdtYWlsLmNvbTpvbGFkaXBvMQ=='
    }
}, function (error, response, body) {
    var x = JSON.parse(body).resource;
    lupus(0, x.length, function(n) {
        var z = x[n];
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?filter=email%3D%22" + z.email + "%22";
        request({
            url: url,
            method: 'GET',
            headers: {
                "Authorization": 'Basic c3R5Y2NzQGdtYWlsLmNvbTpvbGFkaXBvMQ=='
            }
        }, function (err, resp, bd) {
            var y = JSON.parse(bd);
            if (y.resource.length > 0) {
                client.exists({
                    index: 'talents',
                    type: 'people',
                    id: y.resource[0].email
                }, function (error, exists) {
                    if (!exists) {
                        client.index({
                            index: 'talents',
                            type: 'people',
                            id: z.email,
                            body: {
                                first_name: z.first_name,
                                last_name: z.last_name,
                                name: z.name,
                                birthdate: y.resource[0].birthdate,
                                signup_date: y.resource[0].datecreated,
                                marital: y.resource[0].marital,
                                email: y.resource[0].email,
                                lastlogin: y.resource[0].lastlogin,
                                phone: y.resource[0].phone,
                                occupation: y.resource[0].industry,
                                banner_url: y.resource[0].banner_url,
                                image_url: y.resource[0].image_url,
                                profile_url_param: y.resource[0].profile_url_param,
                                summary: y.resource[0].summary,
                                is_mentor: y.resource[0].is_mentor,
                                is_verified: y.resource[0].is_verified
                            }
                        })
                    }
                });
            }
        });
    }, function() {
        console.log('All done!');
    });
});


http.listen(4600);