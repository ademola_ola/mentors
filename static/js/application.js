﻿(function () {
  'use strict';

  function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
      return String.fromCharCode('0x' + p1);
    }));
  }

  function sortByKey(array, key) {
    return array.sort(function(a, b) {
      var x = a[key]; var y = b[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    }).reverse();
  }

  function re_order(str) {
    return str.split('').sort().join('');
  }

  function renew () {
    // var user = $cookieStore.get('user_profile');
    var user = $.cookie("user_profile");
    var url = "http://178.62.199.243:8180/api/v2/user/session?session_token=" + user.session;
    $.ajax({
      url: url,
      type: 'PUT',
      headers: {
        "x-dreamfactory-session-token": user.session,
        "cache-control": "no-cache"
      },
      success: function (data) {
        user.session = data.session_token;
        $.cookie("user_profile", user)
      },
      error: function (err) {
        // location.href='/logout';
      }
    });
    // $http({
    //   method: 'GET',
    //   url: url,
    //   headers: {
    //     "x-dreamfactory-session-token": user.session,
    //     "cache-control": "no-cache"
    //   }
    // }).success(function (data) {
    //   user.session = data.session_token;
    //   $.cookie("user_profile", user)
    //   // $cookieStore.put('user_profile', user);
    // }).
    // error(function (error) {
    //   location.reload();
    //   // location.href = '/logout';
    // });
  }

  function extend(obj, src) {
    for (var key in src) {
      if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
  }

  var module = angular.module('primeApp', ['facebook', 'ngCookies',
    'ngFileUpload', 'summernote', 'angularMoment', 'ngSanitize', 'angularUtils.directives.dirPagination', 'ngAutocomplete']);

  // module.factory('socket', function ($rootScope) {
  //   var socket = io.connect();
  //   return {
  //     on: function (eventName, callback) {
  //       socket.on(eventName, function () {
  //         var args = arguments;
  //         $rootScope.$apply(function () {
  //           callback.apply(socket, args);
  //         });
  //       });
  //     },
  //     emit: function (eventName, data, callback) {
  //       socket.emit(eventName, data, function () {
  //         var args = arguments;
  //         $rootScope.$apply(function () {
  //           if (callback) {
  //             callback.apply(socket, args);
  //           }
  //         });
  //       })
  //     }
  //   };
  // });

  module.config(function (FacebookProvider) {
    FacebookProvider.init({
      appId: '550680588469426',
      xfbml: true,
      version: 'v2.8',
      cookie: true
    });
    // IdleProvider.idle(300); // 10 minutes idle
    // // IdleProvider.timeout(30); // after 30 seconds idle, time the user out
    // KeepaliveProvider.interval(300); // 5 minute keep-alive ping
  });

  // module.run(['Idle', function (Idle) {
  //   Idle.watch();
  // }]);

  module.constant('S_URL', 'http://178.62.199.243:8180');

  module.service('S3UploadService', ['$q', function ($q) {
    // Us standard region
    AWS.config.region = 'us-east-1';
    AWS.config.update({
      accessKeyId: 'AKIAJ7YO3EAGMS5WBDWA',
      secretAccessKey: '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'
    });

    var bucket = new AWS.S3({params: {Bucket: 'talents', maxRetries: 10}, httpOptions: {timeout: 360000}});

    this.Progress = 0;
    this.Upload = function (file) {
      var deferred = $q.defer();
      var params = {Bucket: 'talents', Key: file.name, ContentType: file.type, Body: file};
      var options = {
        // Part Size of 10mb
        partSize: 10 * 1024 * 1024,
        queueSize: 1,
        // Give the owner of the bucket full control
        ACL: 'bucket-owner-full-control'
      };
      var uploader = bucket.upload(params, options, function (err, data) {
        if (err) {
          deferred.reject(err);
        }
        deferred.resolve();
      });
      uploader.on('httpUploadProgress', function (event) {
        deferred.notify(event);
      });

      return deferred.promise;
    };
  }]);

  module.controller('MainController', function($scope, $cookieStore, S_URL, $http, $timeout, $q, $interval) {

    var load_notifications = function () {
      var user = $cookieStore.get('user_profile');
      if (user) {
        $timeout(function () {
          var url = '/notifications/' + user.email + '/get';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            var notes = __data.response;
            if (notes && notes.length > 0) {
              $scope.notifications = notes;
            }
          }).error(function (error) {
            // location.href = '/logout';
          });
        }, 5);
      }
    };

    $scope.notifications = [];
    load_notifications();

    // socket.on('notification', function (data) {
    //   load_notifications();
    // });

    var renew = function () {
      var user = $cookieStore.get('user_profile');
      if (user) {
        var url = "http://178.62.199.243:8180/api/v2/user/session?session_token=" + user.session;
        $http({
          method: 'PUT',
          url: url,
          headers: {
            "x-dreamfactory-session-token": user.session,
            "cache-control": "no-cache"
          }
        }).success(function (data) {
          user.session = data.session_token;
          $cookieStore.put('user_profile', user);
        }).
        error(function (error) {
          // location.reload();
          location.href = '/logout';
        });
      }
    };

    $interval(renew, 1800000);

    // $scope.$on('IdleStart', function () {
    //   renew();
    // });

    // $scope.$on('IdleTimeout', function () {
    //   console.log('timedout');
    //   $('#modaltimeout').closeModal();
    //   $scope.logout();
    // });
    //
    // $scope.$on('IdleEnd', function () {
    //   console.log('stopped')
    //   $('#modaltimeout').closeModal();
    // });

  });

  module.controller('AppController', function ($scope, $timeout, $cookieStore) {
    $scope.data = {'email': ''};
    $scope.getStarted = function () {
      $cookieStore.put('email', {data: $scope.data.email});
      location.href = '/choices';
    };

  });

  module.controller('CheckController', function ($scope, $cookieStore) {

    $scope.loading = true;
    $scope.choice = {'data': ''};

    var email = $cookieStore.get('email');

    $scope.loading = false;

    $scope.error = null;

    $scope.pickChoice = function () {
      if ($scope.choice == undefined) {
        $scope.error = "Pick an option";
      }
      $cookieStore.put('choice', {data: $scope.choice.data});
      location.href = '/details';
    }
  });

  module.controller('DetailController', function ($scope, $cookieStore, $http, $timeout) {

    $scope.email = $cookieStore.get('email').data;
    $scope.choice = $cookieStore.get('choice').data;
    $scope.is_mentor = $scope.choice == 'mentor' ? 1 : 0;
    $scope.value = 0;

    $scope.error = {"message": null};

    var postMentor = function (name, email, username) {
      $timeout(function () {
        $.post('/mail/mentors', {
          name: name,
          image_url: '',
          email: email,
          text: '',
          username: username
        }, function (response, error) {
        })
      }, 1);
    };

    $scope.create_profile = function (session, email, phone, birthdate, marital, industry, location, image_url, banner_url, username, summary, is_mentor) {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/CreateProfile";
      var data = {
        "params": [
          {
            "name": "vemail",
            "param_type": "string",
            "value": email
          },
          {
            "name": "vphone",
            "param_type": "string",
            "value": phone
          },
          {
            "name": "vbirthdate",
            "param_type": "string",
            "value": birthdate
          },
          {
            "name": "vmarital",
            "param_type": "string",
            "value": marital
          },
          {
            "name": "industry",
            "param_type": "string",
            "value": industry
          },
          {
            "name": "vloc",
            "param_type": "string",
            "value": location
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          },
          {
            "name": "vbanner",
            "param_type": "string",
            "value": banner_url
          },
          {
            "name": "vpublic",
            "param_type": "string",
            "value": username
          },
          {
            "name": "vsummary",
            "param_type": "string",
            "value": summary
          },
          {
            "name": "vmentor",
            "param_type": "string",
            "value": is_mentor
          },
          {
            "name": "verified",
            "param_type": "string",
            "value": 0
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      var datar = JSON.stringify(data);
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (_data) {
        if ($scope.is_mentor) {
          postMentor(name, email, username);
        }
        return _data == 1;
      })
        .error(function (err) {
          return err;
        });
    };

    $scope.indexProfile = function (data, url) {
      $.post(url, {
        email: data.email || '',
        summary: data.summary || '',
        username: data.username || '',
        image_url: data.image_url || '',
        banner_url: data.banner_url || '',
        location: data.location || '',
        industry: data.industry || '',
        marital: data.marital || '',
        birthdate: data.birthdate || '',
        phone: data.phone || '',
        is_mentor: false,
        id: data.email,
        name: data.name
      }, function (data, status) {
      })
    };

    $scope.saveDetails = function () {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/user/register";
      $scope.Auth = [];
      var data = "{\n  \"email\": \"" + $scope.email + "\",\n  \"first_name\": \"" + $scope.first_name + "\",\n  \"last_name\": \"" + $scope.last_name + "\",\n  \"display_name\": \"" + $scope.first_name + "\",\n  \"new_password\": \"" + $scope.password + "\"\n}";
      $http({
        method: 'POST',
        url: url,
        headers: {
          "cache-control": "no-cache"
        },
        data: data
      }).success(function (_data) {
        var data = "{\r\n  \"email\": \"" + $scope.email + "\",\r\n  \"password\": \"" + $scope.password + "\",\r\n  \"duration\": 0\r\n}";
        $http({
          url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
          method: 'POST',
          headers: {
            "cache-control": "no-cache"
          },
          data: data
        }).success(function (__data) {
          var rnd = b64EncodeUnicode(__data.email);
          $scope.create_profile(__data.session_token, __data.email, '', '', '', '', '', '', '', rnd, '', $scope.value);
          var auth = {
            session: __data.session_token,
            first_name: __data.first_name,
            last_name: __data.last_name,
            email: __data.email,
            name: __data.name,
            username: rnd
          };
          var resg = {
            birthdate: '',
            signup_date: '',
            marital: '',
            email: __data.email,
            lastlogin: '',
            phone: '',
            occupation: '',
            banner_url: '',
            image_url: '',
            profile_url_param: rnd,
            summary: '',
            is_mentor: 0
          };
          // var dat = Object.assign(auth, resg);
          var dat = extend(auth, resg);
          $cookieStore.put('user_profile', dat);
          $scope.indexProfile(dat, '/elastic/people/index');
          location.href = '/login';
          $scope.loading = false;
        }).error(function (error) {
          $scope.error = error.error.message;
          $scope.loading = false;
        });
      }).error(function (err) {
        $scope.error.message = "Account already exists";
        $scope.loading = false
      });
    }
  });

  module.controller('EducationController', function ($scope, $state, $cookieStore) {

    $scope.loading = true;

    var email = $cookieStore.get('email');

    $scope.loading = false;

    $scope.error = null;

    $scope.save = function () {
      if ($scope.choice == undefined) {
        $scope.error = "Pick an option";
      }
      $cookieStore.put('choice', {choice: $scope.choice});
      location.href = '/details';
    }
  });

  module.controller('LoginController', function ($scope, $http, $timeout, $cookieStore) {

    $scope.init = function () {
      $scope.clicked = false;
      $scope.msg = {'success':null, 'error': null}
    };

    $timeout(function () {
      $scope.loading = false;
    }, 5);

    // $scope.$watch(
    //   function () {
    //     return Facebook.isReady();
    //   },
    //   function (newVal) {
    //     if (newVal)
    //       $scope.facebookReady = true;
    //   }
    // );

    $scope.googleLogin = function (email, password) {
      var data_ = "{\r\n  \"email\": \"" + email + "\",\r\n  \"password\": \"" + password + "\",\r\n  \"duration\": 0\r\n}";
      $http({
        url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
        method: 'POST',
        headers: {
          "cache-control": "no-cache"
        },
        data: data_
      }).success(function (resp) {
        saveAuth(resp);
        $timeout(function () {
          location.href = '/home';
        }, 400);
        $scope.loading = false;
      }).error(function (error) {
        $scope.error = error.error.message;
        $scope.loading = false;
      });
    };

    $scope.googleRegister = function (email, password, name, image_url) {
      $scope.loading = true;
      var data = "{\n  \"email\": \"" + email + "\",\n  \"first_name\": \"" + name + "\",\n  \"last_name\": \"" + name + "\",\n  \"display_name\": \"" + name + "\",\n  \"new_password\": \"" + password + "\"\n}";
      $http({
        url: 'http://178.62.199.243:8180/api/v2/user/register',
        method: 'POST',
        headers: {
          "cache-control": "no-cache"
        },
        data: data
      }).success(function (_data) {
        var data_ = "{\r\n  \"email\": \"" + email + "\",\r\n  \"password\": \"" + password + "\",\r\n  \"duration\": 0\r\n}";
        $http({
          url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
          method: 'POST',
          headers: {
            "cache-control": "no-cache"
          },
          data: data_
        }).success(function (resp) {
          saveAuth(resp);
          $timeout(function () {
            location.href = '/home';
          }, 400);
          $scope.loading = false;
        }).error(function (error) {
          $scope.error = error.error.message;
          $scope.loading = false;
        });
      }).error(function (err) {
        $scope.error = err.error.context.email;
        $scope.loading = false;
      });
    };

    // $scope.facebookLogin = function () {
    //   $scope.loading = true;
    //
    //   $timeout(function () {
    //     $scope.loading = false;
    //   }, 5);
    //
    //   Facebook.getLoginStatus(function (response) {
    //     if (response.status == 'connected') {
    //       Facebook.api('/me?fields=email,education,first_name,gender,last_name,religion', function (response) {
    //         var data = "{\r\n  \"email\": \"" + response.email + "\",\r\n  \"password\": \"" + response.id + "\",\r\n  \"duration\": 0\r\n}";
    //         $http({
    //           url: 'http://178.62.199.243:8180/api/v2/user/session',
    //           method: 'POST',
    //           headers: {
    //             "cache-control": "no-cache"
    //           },
    //           data: data
    //         }).success(function (data) {
    //           saveAuth(data);
    //           $timeout(function () {
    //             location.href = '/home';
    //           }, 400);
    //           $scope.loading = false;
    //         }).error(function (error) {
    //
    //           $scope.error = error.error.message;
    //           $scope.loading = false;
    //         });
    //       });
    //     }
    //     else {
    //       Facebook.login(function (response) {
    //         var data = "{\r\n  \"email\": \"" + response.email + "\",\r\n  \"password\": \"" + response.id + "\",\r\n  \"duration\": 0\r\n}";
    //         $http({
    //           url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
    //           method: 'POST',
    //           headers: {
    //             "cache-control": "no-cache"
    //           },
    //           data: data
    //         }).success(function (data) {
    //           saveAuth(data);
    //           $timeout(function () {
    //             location.href = '/home';
    //           }, 400);
    //           $scope.loading = false;
    //         }).error(function (error) {
    //           $scope.error = error.error.message;
    //           $scope.loading = false;
    //         });
    //       });
    //     }
    //   });
    // };
    //
    // $scope.facebookRegister = function () {
    //   $scope.loading = true;
    //
    //   $timeout(function () {
    //     $scope.loading = false;
    //   }, 5);
    //
    //   Facebook.getLoginStatus(function (response) {
    //     if (response.status == 'connected') {
    //       Facebook.api('/me?fields=email,education,first_name,gender,last_name,religion', function (response) {
    //         var data = "{\n  \"email\": \"" + response.email + "\",\n  \"first_name\": \"" + response.first_name + "\",\n  \"last_name\": \"" + response.last_name + "\",\n  \"display_name\": \"" + response.first_name + "\",\n  \"new_password\": \"" + response.id + "\"\n}";
    //         $http({
    //           url: 'http://178.62.199.243:8180/api/v2/user/register',
    //           method: 'POST',
    //           headers: {
    //             "cache-control": "no-cache"
    //           },
    //           data: data
    //         }).success(function (_data) {
    //           var data_ = "{\r\n  \"email\": \"" + response.email + "\",\r\n  \"password\": \"" + response.id + "\",\r\n  \"duration\": 0\r\n}";
    //           $http({
    //             url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
    //             method: 'POST',
    //             headers: {
    //               "cache-control": "no-cache"
    //             },
    //             data: data_
    //           }).success(function (resp) {
    //             saveAuth(resp);
    //             $timeout(function () {
    //               location.href = '/home';
    //             }, 400);
    //             $scope.loading = false;
    //           }).error(function (error) {
    //             $scope.error = error.error.message;
    //             $scope.loading = false;
    //           });
    //         }).error(function (err) {
    //           $scope.error = err.error.context.email;
    //           $scope.loading = false;
    //         });
    //       });
    //     }
    //     else {
    //       Facebook.login(function (response) {
    //         var data = "{\r\n  \"email\": \"" + response.email + "\",\r\n  \"password\": \"" + response.id + "\",\r\n  \"duration\": 0\r\n}";
    //         $http({
    //           url: 'http://178.62.224.142/api/v2/user/register', //URL to hit
    //           method: 'POST',
    //           headers: {
    //             "cache-control": "no-cache"
    //           },
    //           data: data
    //         }).success(function (data) {
    //           saveAuth(data);
    //           location.href = '/home';
    //           $scope.loading = false;
    //         }).error(function (error) {
    //           $scope.error = "No User registered with this facebook account";
    //           $scope.loading = false;
    //         });
    //       });
    //     }
    //   });
    // };

    $scope.comparePassword = function () {
      if ($scope.password != $scope.confirm_password) {
        $scope.password_error = "Passwords do not match";
      }
      else {
        $scope.password_error = null;
      }
    };

    $scope.create_profile = function (session, email, phone, birthdate, marital, industry, location, image_url, banner_url, username, summary, is_mentor) {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/CreateProfile";
      var data = {
        "params": [
          {
            "name": "vemail",
            "param_type": "string",
            "value": email
          },
          {
            "name": "vphone",
            "param_type": "string",
            "value": phone
          },
          {
            "name": "vbirthdate",
            "param_type": "string",
            "value": birthdate
          },
          {
            "name": "vmarital",
            "param_type": "string",
            "value": marital
          },
          {
            "name": "industry",
            "param_type": "string",
            "value": industry
          },
          {
            "name": "vloc",
            "param_type": "string",
            "value": location
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          },
          {
            "name": "vbanner",
            "param_type": "string",
            "value": banner_url
          },
          {
            "name": "vpublic",
            "param_type": "string",
            "value": username
          },
          {
            "name": "vsummary",
            "param_type": "string",
            "value": summary
          },
          {
            "name": "vmentor",
            "param_type": "string",
            "value": is_mentor
          },
          {
            "name": "verified",
            "param_type": "string",
            "value": 0
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      var datar = JSON.stringify(data);
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (_data) {
        return _data == 1;
      })
        .error(function (err) {
          return err;
        });
    };

    $scope.loginUser = function () {
      $scope.loading = true;
      var data = "{\r\n  \"email\": \"" + $scope.username + "\",\r\n  \"password\": \"" + $scope.password + "\",\r\n  \"remember_me\": true\r\n}";
      $http({
        url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
        method: 'POST',
        headers: {
          "cache-control": "no-cache"
        },
        data: data
      }).success(function (data) {

        if (data.session_token) {
          var auth = {
            session: data.session_token,
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            name: data.name
          };
          $cookieStore.put('session', auth);
          var _url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?filter=email%3D%22" + auth.email + "%22";
          $http({
            method: 'GET',
            url: _url,
            headers: {
              "cache-control": "no-cache",
              "x-dreamfactory-session-token": auth.session,
              "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
            }
          }).success(function (__data) {
            if (__data.resource.length > 0) {
              var resg = {
                birthdate: __data.resource[0].birthdate,
                signup_date: __data.resource[0].datecreated,
                marital: __data.resource[0].marital,
                email: __data.resource[0].email,
                lastlogin: __data.resource[0].lastlogin,
                phone: __data.resource[0].phone,
                occupation: __data.resource[0].industry,
                banner_url: __data.resource[0].banner_url,
                image_url: __data.resource[0].image_url,
                profile_url_param: __data.resource[0].profile_url_param,
                summary: __data.resource[0].summary,
                is_mentor: __data.resource[0].is_mentor,
                is_verified: __data.resource[0].is_verified
              };
              // var dat = Object.assign(auth, resg);
              var dat = extend(auth, resg);
              $cookieStore.put('user_profile', dat);
              $timeout(function () {
                location.href = '/home';
                $scope.loading = false;
              }, 20)
            }
            else {
              var x = $scope.create_profile(data.session_token, data.email, '', '', '', '', '', '', '', b64EncodeUnicode(data.email), '', 0);
              var rex = {
                birthdate: '',
                signup_date: '',
                marital: '',
                email: data.email,
                lastlogin: '',
                phone: '',
                occupation: '',
                banner_url: '',
                image_url: '',
                profile_url_param: '',
                summary: '',
                is_mentor: ''
              };
              // var dax = Object.assign(auth, rex);
              var dax = extend(auth, rex);
              $cookieStore.put('user_profile', dax);
              $timeout(function () {
                location.href = '/home';
                $scope.loading = false;
              }, 20)
            }

          }).error(function (error) {
          });
        } else {
          $scope.error = "Internet not available. Please check and connect again.";
          $scope.loading = false;
        }

      }).error(function (err) {
        if (err) {
          $scope.error = err.error.message;
        }
        else {
          $scope.error = "Internet not available. Please check and connect again.";
        }
        $scope.loading = false;
      });
    };

    $scope.resetPassword = function () {
      $scope.msg = {'success':null, 'error': null};
      $scope.clicked = true;
      var datar = "{\r\n  \"email\": \"" + $scope.username + "\",\r\n  \"password\": \"" + $scope.password + "\",\r\n  \"remember_me\": true\r\n}";
      $scope.loading = true;
      $http({
        url: 'http://178.62.199.243:8180/api/v2/user/password?reset=true', //URL to hit
        method: 'POST',
        headers: {
          "cache-control": "no-cache"
        },
        data:datar
      }).success(function (data) {
        $scope.clicked = true;
        $scope.msg.success = 'Check your email for instructions on resetting your password';
        $scope.loading = false;
      }).error(function (err) {
        $scope.clicked = false;
        $scope.msg.error = 'No account with this email address was found.';
        $scope.loading = false;
      });
    };

    $scope.indexProfile = function (data, url) {
      $.post(url, {
        email: data.email || '',
        summary: data.summary || '',
        username: data.username || '',
        image_url: data.image_url || '',
        banner_url: data.banner_url || '',
        location: data.location || '',
        industry: data.industry || '',
        marital: data.marital || '',
        birthdate: data.birthdate || '',
        phone: data.phone || '',
        is_mentor: data.is_mentor || false,
        id: data.email,
        name: data.name
      }, function (data, status) {
      })
    };

    $scope.registerUser = function () {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/user/register";
      $scope.Auth = [];
      var data = "{\n  \"email\": \"" + $scope.email + "\",\n  \"first_name\": \"" + $scope.first_name + "\",\n  \"last_name\": \"" + $scope.last_name + "\",\n  \"display_name\": \"" + $scope.first_name + "\",\n  \"new_password\": \"" + $scope.password + "\"\n}";
      $http({
        method: 'POST',
        url: url,
        headers: {
          "cache-control": "no-cache"
        },
        data: data
      }).success(function (_data) {

        // login user
        var data = "{\r\n  \"email\": \"" + $scope.email + "\",\r\n  \"password\": \"" + $scope.password + "\",\r\n  \"duration\": 0\r\n}";
        $http({
          url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
          method: 'POST',
          headers: {
            "cache-control": "no-cache"
          },
          data: data
        }).success(function (__data) {
          $scope.create_profile(__data.session_token, __data.email, '', '', '', '', '', '', '', b64EncodeUnicode(__data.email), '', 0);
          var auth = {
            session: __data.session_token,
            first_name: __data.first_name,
            last_name: __data.last_name,
            email: __data.email,
            name: __data.name,
            username: b64EncodeUnicode(__data.email),
            birthdate: '',
            signup_date: '',
            marital: '',
            lastlogin: '',
            phone: '',
            occupation: '',
            banner_url: '',
            image_url: '',
            profile_url_param: b64EncodeUnicode(__data.email),
            summary: '',
            is_mentor: 0
          };
          $scope.indexProfile(auth, '/elastic/people/index');
          $cookieStore.put('user_profile', auth);
          $timeout(function () {
            location.href = '/login';
          }, 10);
          $scope.loading = false;
        }).error(function (error) {
          $scope.error = error.error.message;
          $scope.loading = false;
        });
      }).error(function (err) {
        $scope.error = err.error.context.email;
        $scope.pass_error = err.error.context.password;
        $scope.loading = false
      });
    };

    $scope.googleSignIn = function(username, password) {
      $scope.loading = true;
      var data = "{\r\n  \"email\": \"" + username + "\",\r\n  \"password\": \"" + password + "\",\r\n  \"remember_me\": true\r\n}";
      $http({
        url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
        method: 'POST',
        headers: {
          "cache-control": "no-cache"
        },
        data: data
      }).success(function (data) {
        if (data.session_token) {
          var auth = {
            session: data.session_token,
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            name: data.name
          };
          $cookieStore.put('session', auth);
          var _url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?filter=email%3D%22" + auth.email + "%22";
          $http({
            method: 'GET',
            url: _url,
            headers: {
              "cache-control": "no-cache",
              "x-dreamfactory-session-token": auth.session,
              "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
            }
          }).success(function (__data) {
            if (__data.resource.length > 0) {
              var resg = {
                birthdate: __data.resource[0].birthdate,
                signup_date: __data.resource[0].datecreated,
                marital: __data.resource[0].marital,
                email: __data.resource[0].email,
                lastlogin: __data.resource[0].lastlogin,
                phone: __data.resource[0].phone,
                occupation: __data.resource[0].industry,
                banner_url: __data.resource[0].banner_url,
                image_url: __data.resource[0].image_url,
                profile_url_param: __data.resource[0].profile_url_param,
                summary: __data.resource[0].summary,
                is_mentor: __data.resource[0].is_mentor
              };
              // var dat = Object.assign(auth, resg);
              var dat = extend(auth, resg);
              $cookieStore.put('user_profile', dat);
              $timeout(function () {
                location.href = '/home';
                $scope.loading = false;
              }, 20)
            }
            else {
              var x = $scope.create_profile(data.session_token, data.email, '', '', '', '', '', '', '', b64EncodeUnicode(data.email), '', 0);
              var rex = {
                birthdate: '',
                signup_date: '',
                marital: '',
                email: data.email,
                lastlogin: '',
                phone: '',
                occupation: '',
                banner_url: '',
                image_url: '',
                profile_url_param: '',
                summary: '',
                is_mentor: ''
              };
              // var dax = Object.assign(auth, rex);
              var dax = extend(auth, rex);
              $scope.indexProfile(dax, '/elastic/people/index');
              $cookieStore.put('user_profile', dax);
              $timeout(function () {
                location.href = '/home';
                $scope.loading = false;
              }, 20)
            }

          }).error(function (error) {
          });
        } else {
          $scope.error = "Internet not available. Please check and connect again.";
          $scope.loading = false;
        }

      }).error(function (err) {
        if (err) {
          $scope.error = err.error.message;
        }
        else {
          $scope.error = "Internet not available. Please check and connect again.";
        }
        $scope.loading = false;
      });
    };

    $scope.googleSignUp = function (email, password, name) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/user/register";
      var _name = name.split(" ");
      $scope.Auth = [];
      var data = "{\n  \"email\": \"" + email + "\",\n  \"first_name\": \"" + _name[0] + "\",\n  \"last_name\": \"" + _name[1] + "\",\n  \"display_name\": \"" + name + "\",\n  \"new_password\": \"" + password + "\"\n}";
      $http({
        method: 'POST',
        url: url,
        headers: {
          "cache-control": "no-cache"
        },
        data: data
      }).success(function (_data) {

        // login user
        var data = "{\r\n  \"email\": \"" + email + "\",\r\n  \"password\": \"" + password + "\",\r\n  \"duration\": 0\r\n}";
        $http({
          url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
          method: 'POST',
          headers: {
            "cache-control": "no-cache"
          },
          data: data
        }).success(function (__data) {
          $scope.create_profile(__data.session_token, __data.email, '', '', '', '', '', '', '', b64EncodeUnicode(__data.email), '', 0);
          var auth = {
            session: __data.session_token,
            first_name: __data.first_name,
            last_name: __data.last_name,
            email: __data.email,
            name: __data.name,
            username: b64EncodeUnicode(__data.email),
            birthdate: '',
            signup_date: '',
            marital: '',
            lastlogin: '',
            phone: '',
            occupation: '',
            banner_url: '',
            image_url: '',
            profile_url_param: b64EncodeUnicode(__data.email),
            summary: '',
            is_mentor: 0
          };
          $scope.indexProfile(auth, '/elastic/people/index');
          $cookieStore.put('user_profile', auth);
          $timeout(function () {
            location.href = '/login';
          }, 10);
          $scope.loading = false;
        }).error(function (error) {
          $scope.error = error.error.message;
          $scope.loading = false;
        });
      }).error(function (err) {
        $scope.error = err.error.context.email;
        $scope.pass_error = err.error.context.password;
        $scope.loading = false
      });
    }

  });

  module.controller('HomeController', function ($scope, $timeout, $cookieStore, $http, S3UploadService, $q) {

    var load_redis = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/posts/' + $scope.user.email + '/get';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          if (__data.error) {
            // location.href = '/logout';
          }
          $scope.user.posts = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    var load_posts = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();

      $timeout(function () {
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost?filter=(status%3D1)and(tag%3Dadmin)or(tag%3Dpost)and(status%3D1)and(addedby%3D" + $scope.user.email + ")or(tag%3Dpublic)";
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.user.posts = __data.resource.reverse();
          var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost?filter=addedby%20in%20(';
          angular.forEach($scope.connected, function (key) {
            url = url + '"' + key + '"';
            if (($scope.connected.length -1) != $scope.connected.indexOf(key)) {
              url = url + ','
            }
          });
          url = url + ')';
          $http({
            method: 'GET',
            url: url,
            headers: {
              "cache-control": "no-cache",
              "x-dreamfactory-session-token": $scope.user.session,
              "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
            }
          }).success(function (resp) {
            resp.resource.map(function (elem) {
              $scope.user.posts.unshift(elem);
            });
            sortByKey($scope.user.posts, 'dateadded');
          }).error(function (err) {
            console.log(err);
          })
        }).error(function (error) {
          renew();
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    var load_recommendation = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/elastic/' + $scope.user.email + '/recommendations';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          var recommendations = __data.response;
          var recommend = [];
          if (recommendations && recommendations.length > 0) {
            $scope.user.recommendations = recommendations.filter(function (res) {
              return res.email != $scope.user.email;
            });
          }
        }).error(function (error) {
          renew();
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    var load_requested = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/account/' + $scope.user.email + '/requested';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.requested_connections = __data.response.map(function (elem) {
            return elem.email;
          });
        }).error(function (error) {
          renew();
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    var load_connected = function () {
      var deferred = $q.defer();

      $timeout(function () {
        $http({
          method: 'GET',
          url: '/connections/' + $scope.user.email + '/get'
        }).success(function (__data) {
          $scope.connected = __data.response.map(function (elem) {
            return elem.email;
          });
        }).error(function (error) {
          renew();
          // location.href = '/logout';
        });
        deferred.resolve('connected');
      }, 10);

      return deferred.promise;
    };

    var load_likes = function () {
      var deferred = $q.defer();

      $timeout(function () {
        $http({
          method: 'GET',
          url: '/users/' + $scope.user.email + '/likes'
        }).success(function (__data) {
          $scope.user.likes = __data.response.map(function (elem) {
            return elem.post_id;
          });
        }).error(function (error) {
          renew();
          // location.href = '/logout';
        });
        deferred.resolve('likes');
      }, 10);

      return deferred.promise;
    };

    var checkRequest = function () {
      var url = '/requested/' + $scope.user.email + '/check';
      $http({
        method: 'GET',
        url: url
      }).success(function (__data) {
        $scope.requested = __data.response;
      }).error(function (error) {
        renew()
      });
    };

    var checkVerification = function () {
      var url = '/requested/' + $scope.user.email + '/verification';
      $http({
        method: 'GET',
        url: url
      }).success(function (__data) {
        $scope.verification_requested = __data.response;
      }).error(function (error) {
        renew()
      });
    };

    $scope.shareGPlus = function (post_id) {
      window.open('https://plus.google.com/share?url=' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    $scope.shareFB = function (post_id) {
      window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.origin + '/posts/' + post_id) + '&t=' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    $scope.shareIn = function (post_id) {
      window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(location.origin + '/posts/' + post_id) + '&title=' + encodeURIComponent('Talents&Mentors'));
      return false;
    };

    $scope.shareTweet = function (post_id) {
      window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent('Talents&Mentors') + ':%20' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    $scope.shareMail = function (post_id) {
      window.open('mailto:?subject=' + encodeURIComponent('Talents&Mentors') + '&body=' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    $scope.startParallel = function () {

      $q.all([load_recommendation(), load_posts(), load_connected(), load_requested(), checkRequest(), load_likes(), checkVerification()]).then(
        function (successResult) {
        }, function (failureReason) {
          // renew()
        }
      );
    };

    $scope.init = function () {

      $scope.loading = true;
      $scope.message_sent = false;

      $scope.user = {"profile": {}, "recommendations": [], "notifications": [], "groups": [], "likes": []};
      $scope.message = {"success": null, "error": null};
      $scope.requested_connections = [];
      $scope.connected = [];
      $scope.text = null;
      $scope.request_text = false;
      $scope.post_text = {'message': null};
      $scope.verification_requested = false;

      $timeout(function () {
        $scope.user = $cookieStore.get('user_profile');
        $scope.startParallel();
        $scope.loading = false;
      }, 10);
    };

    $scope.options = {
      height: 300,
      focus: true,
      // airMode: true,
      toolbar: [
        ['edit', ['undo', 'redo']],
        ['headline', ['style']],
        ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
        ['fontface', ['fontname']],
        ['textsize', ['fontsize']],
        ['fontclr', ['color']],
        ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video', 'hr']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']]
      ],
      popover: {
        image: [
          ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
          ['float', ['floatLeft', 'floatRight', 'floatNone']],
          ['remove', ['removeMedia']]
        ],
        link: [
          ['link', ['linkDialogShow', 'unlink']]
        ],
        air: [
          ['color', ['color']],
          ['font', ['bold', 'underline', 'clear']],
          ['para', ['ul', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture']]
        ]
      }
    };

    $scope.requestMentor = function () {
      $scope.request_text = true;
    };

    $scope.postMentor = function () {
      var _data = {
        name: $scope.user.name,
        image_url: $scope.user.image_url,
        email: $scope.user.email,
        text: $scope.request_message,
        username: $scope.user.profile_url_param
      };
      $timeout(function () {
        $.post('/mail/mentors',_data , function (response, error) {
          $scope.message.success = "Your request is being processed.";
          location.reload();
        })
      }, 1);
    };

    var incrComment = function (post_id) {
      var deferred = $q.defer();
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/incrComment";
      $timeout(function () {
        $http({
          async: true,
          crossDomain: true,
          method: 'POST',
          url: url,
          headers: {
            "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
            "x-dreamfactory-session-token": $scope.user.session,
            "cache-control": "no-cache"
          },
          data: {
            "params": [
              {
                "name": "vpost",
                "param_type": "string",
                "value": post_id
              }
            ],
            "schema": {
              "_field_name_": "string"
            },
            "wrapper": "string",
            "returns": "string"
          }
        }).success(function (_data) {
        }).error(function (err) {
          // renew()
        });
        deferred.resolve('incr');
      }, 10);
      return deferred.promise;
    };

    var incrLike = function (post_id) {
      var deferred = $q.defer();
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/incrLike";
      $timeout(function () {
        $http({
          async: true,
          crossDomain: true,
          method: 'POST',
          url: url,
          headers: {
            "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
            "x-dreamfactory-session-token": $scope.user.session,
            "cache-control": "no-cache"
          },
          data: {
            "params": [
              {
                "name": "vpost",
                "param_type": "string",
                "value": post_id
              }
            ],
            "schema": {
              "_field_name_": "string"
            },
            "wrapper": "string",
            "returns": "string"
          }
        }).success(function (_data) {
        }).error(function (err) {
          console.log(err);
          // renew()
        });
        deferred.resolve('incr');
      }, 10);
      return deferred.promise;
    };

    var decrLike = function (post_id) {
      var deferred = $q.defer();
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/decrLike";
      $timeout(function () {
        $http({
          async: true,
          crossDomain: true,
          method: 'POST',
          url: url,
          headers: {
            "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
            "x-dreamfactory-session-token": $scope.user.session,
            "cache-control": "no-cache"
          },
          data: {
            "params": [
              {
                "name": "vpost",
                "param_type": "string",
                "value": post_id
              }
            ],
            "schema": {
              "_field_name_": "string"
            },
            "wrapper": "string",
            "returns": "string"
          }
        }).success(function (_data) {
        }).error(function (err) {
          console.log(err);
          // renew()
        });
        deferred.resolve('incr');
      }, 10);
      return deferred.promise;
    };

    $scope.comPost = function (post_id, body) {
      var post = $scope.user.posts.find(function (res) {
        return res.id == post_id;
      });
      var comment = {
        author: $scope.user.email,
        postID: post_id,
        body: body,
        username: $scope.user.profile_url_param,
        image_url: $scope.user.image_url,
        name: $scope.user.name
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: '/post/' + post.id + '/comment',
        data: comment
      }).success(function (data) {
        post.comments += 1;
        incrComment(post.id);
        post.text = null;
        post.new_comment = comment;
      }).error(function (err) {
      });
    };

    $scope.showComments = function (post_id) {
      var post = $scope.user.posts.find(function (res) {
        return res.id == post_id;
      });
      $http({
        async: true,
        crossDomain: true,
        method: 'GET',
        url: '/posts/' + post_id + '/comments'
      }).success(function (data) {
        post.comment = data;
        post.show = true;
        post.new_comment = null;
      }).error(function (err) {
        location.reload();
      });
    };

    $scope.hideComments = function (post_id) {
      var post = $scope.user.posts.find(function (res) {
        return res.id == post_id;
      });
      post.comment = [];
      post.show = false;
      post.new_comment = null;
    };

    $scope.likePost = function (post_id) {
      var post = $scope.user.posts.find(function (res) {
        return res.id == post_id;
      });
      post.click = true;
      post.unclick = true;
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: '/post/' + post.id + '/like',
        data: {
          email: $scope.user.email,
          postID: post_id,
          username: $scope.user.profile_url_param,
          image_url: $scope.user.image_url,
          name: $scope.user.name
        }
      }).success(function (data) {
        post.likes += 1;
        incrLike(post.id);
      }).error(function (err) {
      });
    };

    $scope.unLike = function (post_id) {
      var post = $scope.user.posts.find(function (res) {
        return res.id == post_id;
      });
      post.unclick = false;
      post.click = false;
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: '/post/' + post.id + '/unlike',
        data: {
          email: $scope.user.email
        }
      }).success(function (data) {
        post.likes -= 1;
        decrLike(post.id);
      }).error(function (err) {
      });
    };

    $scope.deletePost = function (post_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'DELETE',
          url: "http://178.62.199.243:8180/api/v2/prime/_table/tblpost/" + post_id,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          location.reload();
          $scope.loading = false;
        }).error(function (error) {
          // renew()
          location.reload();
        });
      }, 10);
    };

    $scope.uploadFiles = function (files) {
      $scope.Files = files;
      if (files && files.length > 0) {
        angular.forEach($scope.Files, function (file, key) {
          S3UploadService.Upload(file).then(function (result) {
            // Mark as success
            file.Success = true;
            var url = "https://s3.amazonaws.com/talents/" + file.name;
            // $scope.post_redis('image', $scope.text, url);
            var tag = $scope.user.is_verified ? 'public': 'post';
            $scope.post_update(tag, $scope.text, url);
          }, function (error) {
            // Mark the error
            $scope.Error = error;
          }, function (progress) {
            // Write the progress as a percentage
            file.Progress = (progress.loaded / progress.total) * 100
          });
        });
      }
    };

    $scope.postUpdate = function () {
      $scope.post_update('update', $scope.text);
    };

    $scope.post_redis = function (tag, body, image_url) {
      var url = '/posts/create';
      var datar = {
        "text": body,
        "tag": tag,
        "user": $scope.user.email,
        "poster_url": $scope.user.profile.image_url,
        "poster": $scope.user.name,
        "image_url": image_url,
        "id": new Date().getTime()
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        data: datar
      }).success(function (data) {
        $timeout(function () {
          $scope.init();
          location.href = '/home';
          $scope.loading = false;
        }, 10)
      })
        .error(function (err) {
          location.reload();
          // location.href = '/logout';
        });
    };

    $scope.post_update = function (tag, body, image_url) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/CreatePost";
      var datar = {
        "params": [
          {
            "name": "vbody",
            "param_type": "string",
            "value": body || ''
          },
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          },
          {
            "name": "vtag",
            "param_type": "string",
            "value": tag
          },
          {
            "name": "vimage_url",
            "param_type": "string",
            "value": $scope.user.image_url
          },
          {
            "name": "vposter",
            "param_type": "string",
            "value": $scope.user.name
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          },
          {
            "name": "vcomments",
            "param_type": "string",
            "value": 0
          },
          {
            "name": "vlikes",
            "param_type": "string",
            "value": 0
          },
          {
            "name": "vgroup",
            "param_type": "string",
            "value": ''
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        $timeout(function () {
          location.reload();
          $scope.loading = false;
        }, 10)
      })
        .error(function (err) {
          // renew()
        });
    };

    $scope.selectContact = function (contact) {
      $scope.contact = contact;
    };

    $scope.sendRequest = function () {
      var _data = {
        requester: $scope.user.email,
        sender: $scope.user.name,
        requestee: $scope.contact.email,
        recipient: $scope.contact.name,
        text: $scope.connect_message,
        image_url: $scope.user.image_url,
        username: $scope.user.profile_url_param,
        is_mentor: $scope.contact.is_mentor
      };
      $scope.loading = true;
      $.post('/send/mail', _data, function (response, error) {
        location.href = '/talents';
        $scope.loading = false;
      })
    };

    $scope.checkConnection = function (account) {
      if ($scope.requested_connections.length > 0 || $scope.connected.length > 0) {
        return $scope.requested_connections.indexOf(account) != -1 && $scope.connected.indexOf(account) == -1;
      }
      return false;
    };

    $scope.checkPost = function (post_id) {
      if ($scope.user.likes) {
        return $scope.user.likes.indexOf(String(post_id)) != -1;
      }
    };

    $scope.sharePost = function () {
      var tag = $scope.user.is_verified ? 'public': 'post';
      return $scope.post_update(tag, $scope.text, '');
    };

    $scope.postVerify = function () {
      var _data = {
        name: $scope.user.name,
        image_url: $scope.user.image_url,
        email: $scope.user.email,
        text: $scope.request_message,
        username: $scope.user.profile_url_param
      };
      $timeout(function () {
        $.post('/mentors/verify',_data , function (response, error) {
          $scope.message.success = "Your request is being processed.";
          location.reload();
        })
      }, 1);
    };

  });

  module.controller('PostController', function ($scope, $timeout, $cookieStore, $http, S3UploadService, $q) {

    var load_post = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost/" + $scope.post_id;
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-api-key": 'dffa29b8a71b7aaf8a064bf3772a5ffe437ed27f4d88787f5f542e3c61950b2b'
          }
        }).success(function (__data) {
          $scope.post = __data;
        }).error(function (error) {
          location.href = "/logout";
        });
        deferred.resolve('post');
      }, 10);

      return deferred.promise;
    };

    var load_likes = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          $http({
            method: 'GET',
            url: '/users/' + $scope.user.email + '/likes'
          }).success(function (__data) {
            $scope.user.likes = __data.response.map(function (elem) {
              return elem.post_id;
            });
          }).error(function (error) {
            renew()
          });
          deferred.resolve('likes');
        }, 10);

        return deferred.promise;
      }
    };

    $scope.shareGPlus = function (post_id) {
      window.open('https://plus.google.com/share?url=' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    $scope.shareFB = function (post_id) {
      window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.origin + '/posts/' + post_id) + '&t=' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    $scope.shareIn = function (post_id) {
      window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(location.origin + '/posts/' + post_id) + '&title=' + encodeURIComponent('Talents&Mentors'));
      return false;
    };

    $scope.shareTweet = function (post_id) {
      window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent('Talents&Mentors') + ':%20' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    $scope.shareMail = function (post_id) {
      window.open('mailto:?subject=' + encodeURIComponent('Talents&Mentors') + '&body=' + encodeURIComponent(location.origin + '/posts/' + post_id));
      return false;
    };

    var incrComment = function (post_id) {
      var deferred = $q.defer();
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/incrComment";
      $timeout(function () {
        $http({
          async: true,
          crossDomain: true,
          method: 'POST',
          url: url,
          headers: {
            "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
            "x-dreamfactory-session-token": $scope.user.session,
            "cache-control": "no-cache"
          },
          data: {
            "params": [
              {
                "name": "vpost",
                "param_type": "string",
                "value": post_id
              }
            ],
            "schema": {
              "_field_name_": "string"
            },
            "wrapper": "string",
            "returns": "string"
          }
        }).success(function (_data) {
        }).error(function (err) {
          console.log(err);
          // renew()
        });
        deferred.resolve('incr');
      }, 10);
      return deferred.promise;
    };

    var incrLike = function (post_id) {
      var deferred = $q.defer();
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/incrLike";
      $timeout(function () {
        $http({
          async: true,
          crossDomain: true,
          method: 'POST',
          url: url,
          headers: {
            "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
            "x-dreamfactory-session-token": $scope.user.session,
            "cache-control": "no-cache"
          },
          data: {
            "params": [
              {
                "name": "vpost",
                "param_type": "string",
                "value": post_id
              }
            ],
            "schema": {
              "_field_name_": "string"
            },
            "wrapper": "string",
            "returns": "string"
          }
        }).success(function (_data) {
        }).error(function (err) {
          console.log(err);
          // renew()
        });
        deferred.resolve('incr');
      }, 10);
      return deferred.promise;
    };

    var decrLike = function (post_id) {
      var deferred = $q.defer();
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/decrLike";
      $timeout(function () {
        $http({
          async: true,
          crossDomain: true,
          method: 'POST',
          url: url,
          headers: {
            "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
            "x-dreamfactory-session-token": $scope.user.session,
            "cache-control": "no-cache"
          },
          data: {
            "params": [
              {
                "name": "vpost",
                "param_type": "string",
                "value": post_id
              }
            ],
            "schema": {
              "_field_name_": "string"
            },
            "wrapper": "string",
            "returns": "string"
          }
        }).success(function (_data) {
        }).error(function (err) {
          console.log(err);
          // renew()
        });
        deferred.resolve('incr');
      }, 10);
      return deferred.promise;
    };

    $scope.comPost = function (post_id, body) {
      var post = $scope.post;
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: '/post/' + post.id + '/comment',
        data: {
          author: $scope.user.email,
          postID: post_id,
          body: body,
          username: $scope.user.profile_url_param,
          image_url: $scope.user.image_url,
          name: $scope.user.name
        }
      }).success(function (data) {
        post.comments += 1;
        incrComment(post.id);
      }).error(function (err) {
      });
    };

    $scope.showComments = function (post_id) {
      var post = $scope.post;
      $http({
        async: true,
        crossDomain: true,
        method: 'GET',
        url: '/posts/' + post_id + '/comments'
      }).success(function (data) {
        post.comment = data;
        post.show = true
      }).error(function (err) {
        location.reload();
      });
    };

    $scope.hideComments = function (post_id) {
      var post = $scope.post;
      post.comment = [];
      post.show = false
    };

    $scope.likePost = function (post_id) {
      var post = $scope.post;
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: '/post/' + post.id + '/like',
        data: {
          email: $scope.user.email
        }
      }).success(function (data) {
        post.likes += 1;
        incrLike(post.id);
      }).error(function (err) {
      });
    };

    $scope.unLike = function (post_id) {
      var post = $scope.post;
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: '/post/' + post.id + '/unlike',
        data: {
          email: $scope.user.email
        }
      }).success(function (data) {
        post.likes -= 1;
        decrLike(post.id);
      }).error(function (err) {
      });
    };

    $scope.startParallel = function () {
      var startTime = new Date().getTime();
      $scope.overallStatus = 'Running in parallel';

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_post(), load_likes()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          renew()
        }
      );
    };

    $scope.init = function () {

      $scope.loading = true;

      $scope.user = {"notifications": []};
      $scope.post_id = location.pathname.split('/').pop();
      $scope.IN_URL = 'http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(location.href + '/posts/' + $scope.post_id) + '&title=&summary=&source=' + encodeURIComponent(location.href);
      $scope.PLUS_URL = 'https://plus.google.com/share?url=' + encodeURIComponent(location.href + '/posts/' + $scope.post_id);
      $scope.TWEET_URL = 'https://twitter.com/intent/tweet?source=' + encodeURIComponent(location.href) + '&text=:%20' + encodeURIComponent(location.href + '/posts/' + $scope.post_id)
      $scope.FB_URL = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(location.href) + '&t=';
      $scope.MAIL_URL = 'mailto:?subject=&body=:%20' + encodeURIComponent(location.href);

      $timeout(function () {
        $scope.$apply(function () {
          $scope.user = $cookieStore.get('user_profile');
          $scope.startParallel();
        });
        $scope.loading = false;
      }, 10);
    };

  });

  module.controller('PostsController', function ($scope, $timeout, $cookieStore, $http, $q, S3UploadService) {

    var load_posts = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();

      $timeout(function () {
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost?filter=(tag%3Dpost)and(addedby%3D" + $scope.user.email + ")and(tag%3Dpublic)";
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.user.posts = __data.resource.reverse();
        }).error(function (error) {
          renew()
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    $scope.startParallel = function () {

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_posts()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          renew()
        }
      );
    };

    $scope.options = {
      height: 300,
      focus: true,
      toolbar: [
        ['edit', ['undo', 'redo']],
        ['headline', ['style']],
        ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
        ['fontface', ['fontname']],
        ['textsize', ['fontsize']],
        ['fontclr', ['color']],
        ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video', 'hr']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']]
      ],
      popover: {
        image: [
          ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
          ['float', ['floatLeft', 'floatRight', 'floatNone']],
          ['remove', ['removeMedia']]
        ],
        link: [
          ['link', ['linkDialogShow', 'unlink']]
        ],
        air: [
          ['color', ['color']],
          ['font', ['bold', 'underline', 'clear']],
          ['para', ['ul', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture']]
        ]
      }
    };

    $scope.init = function () {
      $scope.loading = true;
      $scope.user = $cookieStore.get('user_profile');
      $scope.user.posts = [];
      $scope.startParallel();
      $scope.loading = false;
      $scope.edited = {"body": null};
    };

    $scope.pageChanged = function (newPage) {
      if ($scope.jobs.prevPage > newPage) {
        $scope.jobs.from = $scope.jobs.from - $scope.jobs.PerPage;
      }
      else {
        $scope.jobs.from = $scope.jobs.from + $scope.jobs.PerPage;
      }
      $scope.jobs.prevPage = newPage;
      $scope.load_redis($scope.jobs.from);
    };

    $scope.deletePost = function (post_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'DELETE',
          url: "http://178.62.199.243:8180/api/v2/prime/_table/tblpost/" + post_id,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          location.reload();
          $scope.loading = false;
        }).error(function (error) {
          renew()
        });
      }, 10);
    };

    $scope.banPost = function (post_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/ban/' + post_id + '/post',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          renew()
        });
      }, 10);
    };

    $scope.activatePost = function (post_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/activate/' + post_id + '/post',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.uploadFiles = function (files) {
      $scope.Files = files;
      if (files && files.length > 0) {
        angular.forEach($scope.Files, function (file, key) {
          S3UploadService.Upload(file).then(function (result) {
            // Mark as success
            file.Success = true;
            var url = "https://s3.amazonaws.com/talents/" + file.name;
            // $scope.post_redis('image', $scope.text, url);
            $scope.update_post($scope.edited.id, $scope.edited.body, url);
          }, function (error) {
            // Mark the error
            $scope.Error = error;
          }, function (progress) {
            // Write the progress as a percentage
            file.Progress = (progress.loaded / progress.total) * 100
          });
        });
      }
    };

    $scope.updatePost = function () {
      $scope.update_post($scope.edited.id, $scope.edited.body, $scope.edited.image);
    };

    $scope.update_post = function (pid, body, image_url) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/EditPost";
      var datar = {
        "params": [
          {
            "name": "pid",
            "param_type": "string",
            "value": pid
          },
          {
            "name": "vbody",
            "param_type": "string",
            "value": body
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        $timeout(function () {
          location.reload();
          $scope.loading = false;
        }, 10)
      })
        .error(function (err) {
          console.log(err);
          // location.href = '/logout';
        });
    };

    $scope.selectPost = function (post) {
      $scope.edited = post;
    };

    $scope.searchPost = function () {
      $scope.loading = true;
      var _q = $scope.search_q == undefined ? '' : $scope.search_q;
      $timeout(function () {
        var url = '/elastic/job/query';
        $http({
          method: 'POST',
          url: url,
          data: {
            "from": 0,
            "size": $scope.jobs.PerPage,
            "search_q": _q
          }
        }).success(function (__data) {
          if (__data.error == null) {
            $scope.jobs.data = __data.response;
            $scope.jobs.total = __data.total;
            $scope.jobs.currentPage = 1;
            $scope.jobs.from = 0;
            $scope.jobs.prevPage = 1;
            $scope.loading = false;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    }
  });

  module.controller('TalentController', function ($scope, $timeout, $http, $cookieStore, $q) {

    $scope.checkConnection = function (account) {
      if ($scope.requested_connections.length > 0 || $scope.connected.length > 0) {
        return $scope.requested_connections.indexOf(account) != -1 || $scope.connected.indexOf(account) != -1;
      }
      return false;
    };

    var load_profiles = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?limit=20&offset" + $scope.talents.from;
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (resp) {
          $scope.talents.data = resp.resource.filter(function (resq) {
            if ($scope.user) {
              return resq.email != $scope.user.email;
            }
            else {
              return resq;
            }
          });
          $scope.talents.total = resp.resource.length;
        }).error(function (error) {
          // location.href = '/logout';
        });

        deferred.resolve('profile');
      }, 10);
      return deferred.promise;
    };

    var load_talents = function () {
      var deferred = $q.defer();

      var datar = {
        "from": $scope.talents.from,
        "size": $scope.talents.PerPage
      };

      if ($scope.user) {
        datar["email"] = $scope.user.email;
      }
      var url = $scope.user ? '/elastic/talents' : '/elastic/talents/query';
      $timeout(function () {
        $http({
          method: 'POST',
          url: url,
          data: datar
        }).success(function (__data) {
          if (__data.error == null) {
            $scope.talents.data = __data.response.filter(function (resq) {
              if ($scope.user) {
                return resq.email != $scope.user.email;
              }
              else {
                return resq;
              }
            });
            $scope.talents.total = __data.total;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    var load_requested = function () {
      if ($scope.user) {
        $scope.thread4Status = 'Started';
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/account/' + $scope.user.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.requested_connections = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          $scope.thread4Status = 'Complete';
          deferred.resolve('thread4');
        }, 10);

        return deferred.promise;
      }
    };

    var load_connected = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          $http({
            method: 'GET',
            url: '/connections/' + $scope.user.email + '/get'
          }).success(function (__data) {
            $scope.connected = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('thread4');
        }, 10);

        return deferred.promise;
      }
    };

    $scope.startParallel = function () {

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_connected(), load_requested(), load_talents()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {
      $scope.total = 0;
      $scope.loading = true;
      $scope.talents = {'data': [], "total": 0, "currentPage": 1, "PerPage": 9, "from": 0, "prevPage": 1};
      $scope.user = $cookieStore.get('user_profile');
      $scope.requested_connections = [];
      $scope.connected = [];
      $scope.startParallel();
      $scope.loading = false;
    };

    $scope.init_likes = function () {
      $scope.likes = {"data": []};
      var post_id = location.pathname.split('/')[2];
      $http({
        method: 'GET',
        url: "/likes/" + post_id
      }).success(function (__data) {
        console.log(__data);
        $scope.likes.data = __data.response;
      }).error(function (error) {
        // location.href = "/logout";
      });
    };

    $scope.pageChanged = function (newPage) {
      if ($scope.talents.prevPage > newPage) {
        $scope.talents.from = $scope.talents.from - $scope.talents.PerPage;
      }
      else {
        $scope.talents.from = $scope.talents.from + $scope.talents.PerPage;
      }
      $scope.talents.prevPage = newPage;
      // var url = $scope.user ? '/elastic/talents' : '/elastic/talents/query';
      load_talents();
      // load_profiles();
    };

    $scope.selectContact = function (contact) {
      $scope.contact = contact;
    };

    $scope.sendRequest = function () {
      var _data = {
        requester: $scope.user.email,
        sender: $scope.user.name,
        requestee: $scope.contact.email,
        recipient: $scope.contact.name,
        text: $scope.message,
        image_url: $scope.user.image_url,
        username: $scope.user.profile_url_param,
        is_mentor: $scope.contact.is_mentor
      };
      $scope.loading = true;
      $.post('/send/mail', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

    $scope.selectRecipient = function (acct) {
      $scope.account = acct;
    };

    $scope.sendMessage = function () {
      var _id = re_order($scope.user.email + $scope.account.email);
      var _data = {
        uniqueID: _id,
        sender_url: $scope.user.image_url,
        sender_username: $scope.user.profile_url_param,
        recipient_url: $scope.account.image_url,
        recipient_username: $scope.account.profile_url_param,
        text: $scope.text_message
      };
      $scope.loading = true;
      $.post('/user/' + _id + '/messages', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

  });

  module.controller('ConnectionController', function ($scope, $timeout, $http, $cookieStore, $q) {

    var load_connections = function (page) {
      var deferred = $q.defer();

      var url = '/connections/' + $scope.user.email + '/get';

      $timeout(function () {
        $http({
          method: 'POST',
          url: url,
          data: {
            "page": page
          }
        }).success(function (__data) {
          var cons = __data.response.docs;
          if (cons && cons.length > 0) {
            $scope.connections.data = cons;
            $scope.connections.total = __data.response.total;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    // var load_connections = function () {
    //   $timeout(function () {
    //     var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblconnections?filter=creator%3D%22' + $scope.user.email + '%22';
    //     $http({
    //       method: 'GET',
    //       url: url,
    //       headers: {
    //         "cache-control": "no-cache",
    //         "x-dreamfactory-session-token": $scope.user.session,
    //         "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
    //       }
    //     }).success(function (__data) {
    //       $scope.connections.data = __data.resource;
    //       $scope.connections.total = __data.resource.length;
    //     }).error(function (err) {
    //       console.log(err);
    //       // location.href = '/logout';
    //     });
    //   }, 10);
    // };

    var load_requested = function () {
      if ($scope.user) {
        $scope.thread4Status = 'Started';
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/account/' + $scope.user.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.requested_connections = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          $scope.thread4Status = 'Complete';
          deferred.resolve('thread4');
        }, 10);

        return deferred.promise;
      }
    };

    $scope.startParallel = function () {

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_connections()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {
      $scope.total = 0;
      $scope.loading = true;
      $scope.connections = {'data': [], "total": 0, "currentPage": 1, "PerPage": 20, "from": 0, "prevPage": 1};
      $scope.user = $cookieStore.get('user_profile');
      $scope.user.notifications = [];
      $scope.loading = false;
      $scope.startParallel();
    };

    $scope.selectRecipient = function (acct) {
      $scope.account = acct;
    };

    $scope.sendMessage = function () {
      var _id = re_order($scope.user.email + $scope.account.email);
      var _data = {
        uniqueID: _id,
        sender_url: $scope.user.image_url,
        sender_username: $scope.user.profile_url_param,
        recipient_url: $scope.account.image_url,
        recipient_username: $scope.account.profile_url_param,
        text: $scope.text_message
      };
      $scope.loading = true;
      $.post('/user/' + _id + '/messages', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

    $scope.pageChanged = function (newPage) {
      if ($scope.connections.prevPage > newPage) {
        $scope.connections.from = $scope.connections.from - $scope.connections.PerPage;
      }
      else {
        $scope.connections.from = $scope.connections.from + $scope.connections.PerPage;
      }
      $scope.connections.prevPage = newPage;
      load_connections(newPage);
    };

  });

  module.controller('MentorController', function ($scope, $timeout, $http, $cookieStore, $q) {

    $scope.checkConnection = function (account) {
      if ($scope.requested_connections.length > 0) {
        return $scope.requested_connections.indexOf(account) != -1;
      }
      return false;
    };

    var load_mentors = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();

      var datar = {
        "from": $scope.mentors.from,
        "size": $scope.mentors.PerPage
      };

      if ($scope.user) {
        datar["email"] = $scope.user.email;
      }
      var url = $scope.user ? '/elastic/mentors' : '/elastic/mentors/query';
      $timeout(function () {
        $http({
          method: 'POST',
          url: url,
          data: datar
        }).success(function (__data) {
          if (__data.error == null) {
            $scope.mentors.data = __data.response.filter(function (resq) {
              if ($scope.user) {
                return resq.email != $scope.user.email;
              }
              else {
                return resq;
              }
            });
            $scope.mentors.total = __data.total;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);

      return deferred.promise;
    };

    var load_requested = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/mentor/' + $scope.user.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.requested_connections = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('requested');
        }, 10);

        return deferred.promise;
      }
    };

    var load_connected = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          $http({
            method: 'GET',
            url: '/connections/' + $scope.user.email + '/get'
          }).success(function (__data) {
            $scope.connected = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('thread4');
        }, 10);

        return deferred.promise;
      }
    };

    var my_mentors = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/mentors/' + $scope.user.email + '/get';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.private_mentors.data = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('my_mentors');
      }, 10);

      return deferred.promise;
    };

    $scope.init_private = function () {
      $scope.loading = true;
      $scope.user = $cookieStore.get('user_profile');
      $scope.private_mentors = {"data": []};

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([my_mentors()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
          // var endTime = new Date().getTime();
          // var str = 'Process took ' + (endTime - startTime) + ' miliseconds: \n';
          // for (var i = 0; i < successResult.length; i++) {
          //   // concat the various result strings for the alert() output
          //   str = str.concat(successResult[i] + ' ');
          // }
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // $scope.overallStatus = 'Failed: ' + failureReason;
          // location.href = '/logout';
        }
      );
      $scope.loading = false;
    };

    $scope.startParallel = function () {

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_connected(), load_mentors(), load_requested()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {
      $scope.total = 0;
      $scope.loading = true;
      $scope.mentors = {'data': [], "total": 0, "currentPage": 1, "PerPage": 9, "from": 0, "prevPage": 1};
      $scope.user = $cookieStore.get('user_profile');
      $scope.requested_connections = [];
      $scope.startParallel();
      $scope.loading = false;
    };

    $scope.pageChanged = function (newPage) {
      if ($scope.mentors.prevPage > newPage) {
        $scope.mentors.from = $scope.mentors.from - $scope.mentors.PerPage;
      }
      else {
        $scope.mentors.from = $scope.mentors.from + $scope.mentors.PerPage;
      }
      $scope.mentors.prevPage = newPage;
      load_mentors();
    };

    $scope.selectContact = function (contact) {
      $scope.contact = contact;
    };

    $scope.sendRequest = function () {
      var _data = {
        requester: $scope.user.email,
        sender: $scope.user.name,
        requestee: $scope.contact.email,
        recipient: $scope.contact.name,
        text: $scope.message,
        image_url: $scope.user.image_url,
        username: $scope.user.profile_url_param,
        is_mentor: $scope.contact.is_mentor
      };
      $scope.loading = true;
      $.post('/mentor/request', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

  });

  module.controller('ProfileController', function ($scope, $timeout, $cookieStore, $http, Upload, S3UploadService, $location, $q) {

    var load_interests = function () {
      // use $q service to get deferred object
      var deferred = $q.defer();

      // use $timeout service to safely cause a delay
      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblinterests?filter=user%3D%22' + $scope.user.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.user.interests = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('thread1');
      }, 10);

      return deferred.promise;
    };

    var load_profile = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?filter=email%3D%22" + $scope.user.email + "%22";
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (resg) {
          $scope.user.profile = {};
          if (resg.resource[0].birthdate) {
            $scope.user.profile.birthdate = resg.resource[0].birthdate;
          }
          else {
            $scope.user.profile.birthdate = ""
          }

          if (resg.resource[0].signup_date) {
            $scope.user.profile.signup_date = resg.resource[0].datecreated
          }
          else {
            $scope.user.profile.signup_date = ""
          }

          if (resg.resource[0].marital) {
            $scope.user.profile.marital = resg.resource[0].marital
          }
          else {
            $scope.user.profile.marital = ""
          }

          if (resg.resource[0].email) {
            $scope.user.profile.email = resg.resource[0].email
          }
          else {
            $scope.user.profile.email = ""
          }

          if (resg.resource[0].lastlogin) {
            $scope.user.profile.lastlogin = resg.resource[0].lastlogin
          }
          else {
            $scope.user.profile.lastlogin = ""
          }

          if (resg.resource[0].phone) {
            $scope.user.profile.phone = resg.resource[0].phone
          }
          else {
            $scope.user.profile.phone = ""
          }

          if (resg.resource[0].industry) {
            $scope.user.profile.occupation = resg.resource[0].industry
          }
          else {
            $scope.user.profile.occupation = ""
          }

          if (resg.resource[0].banner_url) {
            $scope.user.profile.banner_url = resg.resource[0].banner_url
          }
          else {
            $scope.user.profile.banner_url = ""
          }

          if (resg.resource[0].image_url) {
            $scope.user.profile.image_url = resg.resource[0].image_url
          }
          else {
            $scope.user.profile.image_url = ""
          }

          if (resg.resource[0].profile_url_param) {
            $scope.user.profile.profile_url_param = resg.resource[0].profile_url_param;
            var _path = $location.$$absUrl.replace('profile', 'public');
            $scope.profile_url = _path + '/' + $scope.user.profile.profile_url_param;
          }
          else {
            $scope.user.profile.profile_url_param = ""
          }

          if (resg.resource[0].summary) {
            $scope.user.profile.summary = resg.resource[0].summary
          }
          else {
            $scope.user.profile.summary = ""
          }

          if (resg.resource[0].location) {
            $scope.user.profile.location = resg.resource[0].location;
          }
          else {
            $scope.user.profile.location = ""
          }

          if (resg.resource[0].is_mentor) {
            $scope.user.profile.is_mentor = resg.resource[0].is_mentor
          }
          else {
            $scope.user.profile.is_mentor = 0
          }
          if (resg.resource[0].is_verified) {
            $scope.user.profile.is_verified = resg.resource[0].is_verified
          }
          else {
            $scope.user.profile.is_verified = 0
          }
        }).error(function (error) {
          // location.href = '/logout';
        });

        deferred.resolve('profile');
      }, 10);
      return deferred.promise;
    };

    var load_skills = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblskills?filter=user%3D%22' + $scope.user.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.user.skills = __data.resource;
        }).error(function (error) {
        });
        deferred.resolve('skills');
      }, 10);

      return deferred.promise;
    };

    var load_recommendation = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/elastic/' + $scope.user.email + '/recommendations';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          var recommendations = __data.response;
          var recommend = [];
          if (recommendations && recommendations.length > 0) {
            $scope.user.recommendations = recommendations.filter(function (res) {
              return res.email != $scope.user.email;
            });
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('recommend');
      }, 10);

      return deferred.promise;
    };

    var load_experiences = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblexperience?filter=user%3D%22' + $scope.user.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.user.experiences = __data.resource;
        }).error(function (error) {

        });
        deferred.resolve('experiences');
      }, 500);

      return deferred.promise;
    };

    var load_education = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tbleducation?filter=user%3D%22' + $scope.user.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.user.education = [];
          $scope.user.education = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('education');
      }, 10);

      return deferred.promise;
    };

    var load_connected = function () {
      var deferred = $q.defer();

      $timeout(function () {
        $http({
          method: 'GET',
          url: '/connections/' + $scope.user.email + '/get'
        }).success(function (__data) {
          $scope.connected = __data.response.map(function (elem) {
            return elem.email;
          });
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('con');
      }, 10);

      return deferred.promise;
    };

    $scope.startParallel = function () {

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_interests(), load_profile(), load_skills(), load_education(), load_experiences(), load_recommendation(), load_connected()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
        }
      );
    };

    $scope.init = function () {

      $scope.user = {"data": {}, "profile": {}, "interests": [], "education": [], "skills": [], "experiences": []};
      $scope.user = $cookieStore.get('user_profile');

      $scope.result1 = '';
      $scope.options1 = null;
      $scope.details1 = '';

      $scope.startParallel();
    };

    $scope.indexProfile = function (data, url) {
      var datar = {
        email: data.email || '',
        summary: data.summary || '',
        username: data.profile_url_param || '',
        image_url: data.image_url || '',
        banner_url: data.banner_url || '',
        location: data.location || '',
        industry: data.occupation || '',
        marital: data.marital || '',
        birthdate: data.birthdate || '',
        phone: data.phone || '',
        is_mentor: data.is_mentor || false,
        id: data.email,
        name: $scope.user.name,
        is_verified: data.is_verified || false
      };
      $.post(url, datar, function (data, status) {
      })
    };

    $scope.update_profile = function (session, email, phone, birthdate, marital, industry, loc, image_url, banner_url, username, summary, is_mentor, is_verified) {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/UpdateProfile";
      var _ismentor = is_mentor ? 1 : 0;
      var data = {
        "params": [
          {
            "name": "vemail",
            "param_type": "string",
            "value": email
          },
          {
            "name": "vphone",
            "param_type": "string",
            "value": phone
          },
          {
            "name": "vbirthdate",
            "param_type": "string",
            "value": birthdate
          },
          {
            "name": "vmarital",
            "param_type": "string",
            "value": marital
          },
          {
            "name": "industry",
            "param_type": "string",
            "value": industry
          },
          {
            "name": "vloc",
            "param_type": "string",
            "value": loc
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          },
          {
            "name": "vbanner",
            "param_type": "string",
            "value": banner_url
          },
          {
            "name": "vpublic",
            "param_type": "string",
            "value": username
          },
          {
            "name": "vsummary",
            "param_type": "string",
            "value": summary
          },
          {
            "name": "vmentor",
            "param_type": "string",
            "value": _ismentor
          },
          {
            "name": "verified",
            "param_type": "string",
            "value": is_verified
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      var datar = JSON.stringify(data);
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (resp) {
        $scope.indexProfile($scope.user.profile, '/elastic/people/update');
        location.reload();
        $scope.loading = false;
      })
        .error(function (err) {
          location.href = '/logout';
        });
    };

    $scope.uploadFiles = function (files) {
      $scope.Files = files;
      if (files && files.length > 0) {
        angular.forEach($scope.Files, function (file, key) {
          S3UploadService.Upload(file).then(function (result) {
            // Mark as success
            file.Success = true;
            $scope.user.profile.image_url = "https://s3.amazonaws.com/talents/" + file.name;
            $scope.updateProfile();
          }, function (error) {
            // Mark the error
            $scope.Error = error;
          }, function (progress) {
            // Write the progress as a percentage
            file.Progress = (progress.loaded / progress.total) * 100
          });
        });
      }
    };

    $scope.updateProfile = function () {
      $scope.loading = true;
      $scope.update_profile($scope.user.session, $scope.user.email, $scope.user.profile.phone, $scope.user.profile.birthdate, $scope.user.profile.marital,
        $scope.user.profile.occupation, $scope.user.profile.location, $scope.user.profile.image_url, $scope.user.profile.banner_url, $scope.user.profile.profile_url_param, $scope.user.profile.summary, $scope.user.profile.is_mentor);
    };

    $scope.AddInterest = function () {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/AddInterest";
      var datar = {
        "params": [
          {
            "name": "vinterest",
            "param_type": "string",
            "value": $scope.interest
          },
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };

      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (_data) {
        location.reload();
        $scope.loading = false;
      }).error(function (err) {
        // location.href = '/logout';
      });
    };

    $scope.DeleteInterest = function (interest_id) {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/DeleteInterest";
      var datar = {
        "params": [
          {
            "name": "iid",
            "param_type": "string",
            "value": interest_id
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        location.href = "/profile";
      })
        .error(function (err) {
          // location.href = '/logout';
        });
    };

    $scope.AddSkill = function () {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/AddSkill";
      var datar = {
        "params": [
          {
            "name": "vskill",
            "param_type": "string",
            "value": $scope.skill
          },
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        location.reload();
      })
        .error(function () {
          // location.href = '/logout';
        });
    };

    $scope.DeleteSkill = function (skill_id) {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/DeleteSkill";
      var datar = {
        "params": [
          {
            "name": "sid",
            "param_type": "string",
            "value": skill_id
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        location.reload();
      })
        .error(function (err) {
          // location.href = '/logout';
        });
    };

    $scope.AddEducation = function () {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/AddEducation";
      var datar = {
        "params": [
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          },
          {
            "name": "vschool",
            "param_type": "string",
            "value": $scope.school
          },
          {
            "name": "vdegree",
            "param_type": "string",
            "value": $scope.degree
          },
          {
            "name": "vstudyfield",
            "param_type": "string",
            "value": $scope.field
          },
          {
            "name": "vgrade",
            "param_type": "string",
            "value": $scope.grade
          },
          {
            "name": "vloc",
            "param_type": "string",
            "value": $scope.location
          },
          {
            "name": "vdesc",
            "param_type": "string",
            "value": $scope.description || ''
          },
          {
            "name": "ventrylevel",
            "param_type": "string",
            "value": ''
          },
          {
            "name": "vgrad",
            "param_type": "string",
            "value": $scope.year
          },
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (_data) {
        locaction.reload()
      }).error(function (err) {
        // location.href = '/logout';
      });
    };

    $scope.editEdu = function (education) {
      $scope.education = education;
    };

    $scope.UpdateEducation = function () {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/UpdateEducation";
      var datar = {
        "params": [
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          },
          {
            "name": "vschool",
            "param_type": "string",
            "value": $scope.education.school
          },
          {
            "name": "vdegree",
            "param_type": "string",
            "value": $scope.education.degree
          },
          {
            "name": "vloc",
            "param_type": "string",
            "value": $scope.education.location
          },
          {
            "name": "vdesc",
            "param_type": "string",
            "value": $scope.education.description || ''
          },
          {
            "name": "vstudyfield",
            "param_type": "string",
            "value": $scope.education.studyfield
          },
          {
            "name": "ventrylevel",
            "param_type": "string",
            "value": ''
          },
          {
            "name": "vgrade",
            "param_type": "string",
            "value": $scope.education.grade
          },
          {
            "name": "eid",
            "param_type": "string",
            "value": $scope.education.id
          },
          {
            "name": "vgrad",
            "param_type": "string",
            "value": $scope.education.graduation_year
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (_data) {
        location.reload();
      }).error(function (err) {
        location.href = '/logout';
      });
    };

    $scope.DeleteEducation = function (education_id) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/RemoveEducation";
      var datar = {
        "params": [
          {
            "name": "iid",
            "param_type": "string",
            "value": education_id
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        location.reload()
      })
        .error(function (err) {
          // location.href = '/logout';
        });
    };

    $scope.AddExperience = function () {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/AddExperience";
      var datar = {
        "params": [
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          },
          {
            "name": "vcompany",
            "param_type": "string",
            "value": $scope.company
          },
          {
            "name": "vtitle",
            "param_type": "string",
            "value": $scope.title
          },
          {
            "name": "vlocation",
            "param_type": "string",
            "value": $scope.loc
          },
          {
            "name": "vstartdate",
            "param_type": "string",
            "value": $scope.startdate
          },
          {
            "name": "venddate",
            "param_type": "string",
            "value": $scope.enddate
          },
          {
            "name": "vdesc",
            "param_type": "string",
            "value": $scope.desc || ''
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (_data) {
        location.reload();
      }).error(function (err) {
        // location.href = '/logout';
      });
    };

    $scope.editExperience = function (experience) {
      $scope.experience = experience;
    };

    $scope.UpdateExperience = function () {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/UpdateExperience";
      var datar = {
        "params": [
          {
            "name": "eid",
            "param_type": "string",
            "value": $scope.experience.id
          },
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          },
          {
            "name": "vcompany",
            "param_type": "string",
            "value": $scope.experience.company
          },
          {
            "name": "vtitle",
            "param_type": "string",
            "value": $scope.experience.title
          },
          {
            "name": "vlocation",
            "param_type": "string",
            "value": $scope.experience.location
          },
          {
            "name": "vstartdate",
            "param_type": "string",
            "value": $scope.experience.startdate
          },
          {
            "name": "venddate",
            "param_type": "string",
            "value": $scope.experience.enddate
          },
          {
            "name": "vdesc",
            "param_type": "string",
            "value": $scope.experience.description
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (_data) {
        location.reload()
      }).error(function (err) {
        // location.href = '/logout';
      });
    };

    $scope.DeleteExperience = function (experience_id) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/DeleteExperience";
      var datar = {
        "params": [
          {
            "name": "iid",
            "param_type": "string",
            "value": experience_id
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        location.reload()
      })
        .error(function (err) {
          console.log(err);
          // location.href = '/logout';
        });
    };

  });

  module.controller('JobController', function ($scope, $timeout, $cookieStore, $http, $q) {

    var load_jobs = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();
      var url = '/elastic/job';
      $timeout(function () {
        $http({
          method: 'POST',
          url: url,
          data: {
            "from": $scope.jobs.from,
            "size": $scope.jobs.PerPage
          }
        }).success(function (__data) {
          if (__data.error == null) {
            $scope.jobs.data = __data.response;
            $scope.jobs.total = __data.total;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        $scope.thread4Status = 'Complete';
        deferred.resolve('thread4');
      }, 10);
      return deferred.promise;
    };

    // var load_notifications = function () {
    //   if ($scope.user) {
    //     $scope.thread3Status = 'Started';
    //     var deferred = $q.defer();
    //
    //     $timeout(function () {
    //       var url = '/notifications/' + $scope.user.email + '/get';
    //       $http({
    //         method: 'GET',
    //         url: url
    //       }).success(function (__data) {
    //         var notes = __data.response;
    //         if (notes && notes.length > 0) {
    //           $scope.user.notifications = notes;
    //         }
    //       }).error(function (error) {
    //         location.href = '/logout';
    //       });
    //       $scope.thread3Status = 'Complete';
    //       deferred.resolve('thread3');
    //     }, 500);
    //     return deferred.promise;
    //   }
    // };

    $scope.startParallel = function () {

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_jobs()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {
      $scope.loading = true;
      $scope.jobs = {'data': [], "total": 0, "currentPage": 1, "PerPage": 10, "from": 0, "prevPage": 1};
      $scope.user = $cookieStore.get('user_profile');
      $scope.startParallel();
      $scope.loading = false;
    };

    $scope.pageChanged = function (newPage) {
      if ($scope.jobs.prevPage > newPage) {
        $scope.jobs.from = $scope.jobs.from - $scope.jobs.PerPage;
      }
      else {
        $scope.jobs.from = $scope.jobs.from + $scope.jobs.PerPage;
      }
      $scope.jobs.prevPage = newPage;
      load_jobs();
    };

    $scope.AddJob = function () {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/AddJob";
      var datar = {
        "params": [
          {
            "name": "vtitle",
            "param_type": "string",
            "value": $scope.title
          },
          {
            "name": "vlink",
            "param_type": "string",
            "value": $scope.link
          },
          {
            "name": "vdesc",
            "param_type": "string",
            "value": $scope.description
          },
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.email
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        $timeout(function () {
          $.post('/elastic/job/index', {
            title: $scope.title,
            description: $scope.description,
            posted_by: $scope.user.email,
            link: $scope.link || '',
            dateadded: new Date().getTime(),
            id: $scope.title + '_' + $scope.user.email
          }, function (data, status) {
          });
          location.reload();
          $scope.loading = false;
        }, 100)
      })
        .error(function (err) {
          console.log(err);
          // location.href = '/logout';
        });
    };

    $scope.searchJob = function () {
      $scope.loading = true;
      var _q = $scope.search_q == undefined ? '' : $scope.search_q;
      $timeout(function () {
        var url = '/elastic/job/query';
        $http({
          method: 'POST',
          url: url,
          data: {
            "from": 0,
            "size": $scope.jobs.PerPage,
            "search_q": _q
          }
        }).success(function (__data) {
          if (__data.error == null) {
            $scope.jobs.data = __data.response;
            $scope.jobs.total = __data.total;
            $scope.jobs.currentPage = 1;
            $scope.jobs.from = 0;
            $scope.jobs.prevPage = 1;
            $scope.loading = false;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    }
  });

  module.controller('CommunityController', function ($scope, $timeout, $cookieStore, $http, $q) {

    $scope.checkConnection = function (account) {
      if ($scope.requested_connections.length > 0) {
        return $scope.requested_connections.indexOf(account) != -1;
      }
      return false;
    };

    var load_jobs = function (from) {
      var deferred = $q.defer();
      $timeout(function () {
        var url = '/elastic/job/all';
        $http({
          method: 'POST',
          url: url,
          data: {
            "from": from,
            "size": 8
          }
        }).success(function (__data) {
          if (__data.error == null) {
            $scope.jobs.data = __data.response;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('thread1');
      }, 10);
      return deferred.promise;
    };

    var load_recommendations = function () {
      var deferred = $q.defer();
      var url = '/elastic/recommendations';
      $timeout(function () {
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          var recommendations = __data.response;
          var recommend = [];
          if (recommendations && recommendations.length > 0) {
            $scope.data.recommendations = recommendations.filter(function (res) {
              if ($scope.user) {
                return res.email != $scope.user.email;
              }
              return res;
            });
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('thread2');
      }, 10);

      return deferred.promise;
    };

    var load_groups = function () {
      var url = '/elastic/groups';
      $http({
        method: 'GET',
        url: url
      }).success(function (__data) {
        var groups = __data.response;
        if (groups && groups.length > 0) {
          $scope.data.groups = groups.filter(function (res) {
            if ($scope.user) {
              return res.email != $scope.user.email;
            }
            return res;
          });
        }
      }).error(function (error) {
        // location.href = '/logout';
      });
    };

    // $scope.load_people = function () {
    //   var url = '/elastic/people';
    //   $http({
    //     method: 'GET',
    //     url: url
    //   }).success(function (__data) {
    //     var people = __data.response;
    //     if (people && people.length > 0) {
    //       $scope.data.people = people.filter(function (res) {
    //         if ($scope.user) {
    //           return res.email != $scope.user.email;
    //         }
    //         return res;
    //       });
    //     }
    //   }).error(function (error) {
    //     location.href = '/logout';
    //   });
    // };

    var load_requested = function () {
      if ($scope.user) {
        $scope.thread4Status = 'Started';
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/account/' + $scope.user.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.data.requested_connections = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          $scope.thread4Status = 'Complete';
          deferred.resolve('thread4');
        }, 10);

        return deferred.promise;
      }
    };

    var load_requested_groups = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/groups/' + $scope.user.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.data.requested_groups = __data.response.map(function (elem) {
              return elem.user;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('requested_groups');
        }, 10);

        return deferred.promise;
      }
    };

    // var load_notifications = function () {
    //   if ($scope.user) {
    //     $scope.thread3Status = 'Started';
    //     var deferred = $q.defer();
    //
    //     $timeout(function () {
    //       var url = '/notifications/' + $scope.user.email + '/get';
    //       $http({
    //         method: 'GET',
    //         url: url
    //       }).success(function (__data) {
    //         var notes = __data.response;
    //         if (notes && notes.length > 0) {
    //           $scope.user.notifications = notes;
    //         }
    //       }).error(function (error) {
    //         location.href = '/logout';
    //       });
    //       $scope.thread3Status = 'Complete';
    //       deferred.resolve('thread3');
    //     }, 500);
    //     return deferred.promise;
    //   }
    // };

    var load_connected = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          $http({
            method: 'GET',
            url: '/connections/' + $scope.user.email + '/get'
          }).success(function (__data) {
            $scope.data.connected = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('connected');
        }, 10);

        return deferred.promise;
      }
    };

    $scope.startParallel = function () {
      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_connected(), load_jobs(), load_requested(), load_recommendations(), load_groups(), load_requested_groups()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {
      $scope.loading = true;
      $scope.jobs = {'data': [], "total": 0, "currentPage": 1, "PerPage": 8, "from": 0, "prevPage": 1};
      $scope.data = {"recommendations": [], "groups": [], "requested_connections": [], "requested_groups": [], "connected": []};
      $scope.user = $cookieStore.get('user_profile');
      $scope.startParallel();
      $scope.loading = false;
    };

    $scope.selectContact = function (contact) {
      $scope.contact = contact;
    };

    $scope.sendRequest = function () {
      var _data = {
        requester: $scope.user.email,
        sender: $scope.user.name,
        requestee: $scope.contact.email,
        recipient: $scope.contact.name,
        text: $scope.message,
        image_url: $scope.user.image_url,
        username: $scope.user.profile_url_param,
        is_mentor: $scope.contact.is_mentor
      };
      $scope.loading = true;
      $.post('/send/mail', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

    $scope.checkConnection = function (account) {
      if ($scope.data.requested_connections.length > 0 || $scope.data.connected.length > 0) {
        return $scope.data.requested_connections.indexOf(account) != -1 || $scope.data.connected.indexOf(account) != -1;
      }
      return false;
    };

    $scope.checkGroups = function (grp_id) {
      if ($scope.user) {
        return $scope.data.requested_groups.indexOf(grp_id) != -1;
      }
      return false
    };

    $scope.Join = function (grp) {
      $scope.loading = true;
      var _data = {
        requester: $scope.user.data.email,
        sender: $scope.user.data.name,
        requestee: grp.email,
        recipient: grp.name,
        text: "I want to join your group",
        image_url: $scope.user.data.image_url,
        username: $scope.user.data.profile_url_param,
        is_mentor: $scope.user.data.is_mentor,
        group_id: grp.id
      };
      $.post('/group/request', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

    $scope.checkUser = function (email) {
      if ($scope.user) {
        return email != $scope.user.email;
      }
      return false
    };

    $scope.selectRecipient = function (acct) {
      $scope.account = acct;
    };

    $scope.sendMessage = function () {
      var _id = re_order($scope.user.email + $scope.account.email);
      var _data = {
        uniqueID: _id,
        sender_url: $scope.user.image_url,
        sender_username: $scope.user.profile_url_param,
        recipient_url: $scope.account.image_url,
        recipient_username: $scope.account.profile_url_param,
        text: $scope.text_message
      };
      $scope.loading = true;
      $.post('/user/' + _id + '/messages', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

  });

  module.controller('DashboardController', function ($scope, $timeout, $cookieStore, $http, S3UploadService, $q) {

    var load_requests = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/mentor/' + $scope.user.email + '/requests';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.user.requests = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('requested');
      }, 10);

      return deferred.promise;
    };

    var load_talents = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/mentor/' + $scope.user.email + '/talents';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.user.talents = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('talents');
      }, 10);

      return deferred.promise;
    };

    var load_convos = function (_email) {
      var x = re_order($scope.user.email + _email);
      var account = $scope.user.talents.find(function (res) {
        return res.email == _email;
      });
      $timeout(function () {
        var url = '/user/' + x + '/conversations';
        $http({
          method: 'GET',
          url: url
        }).success(function (data) {
          console.log(data);
          account.conversations = data;
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10)
    };

    $scope.user = $cookieStore.get('user_profile');

    // socket.on($scope.user.email, function (data) {
    //   load_convos(data.email);
    // });

    $scope.selectRecipient = function (acct) {
      $scope.account = acct;
      load_convos($scope.account.email);
    };

    $scope.sendMessage = function () {
      var _id = re_order($scope.user.email + $scope.account.email);
      var _data = {
        uniqueID: _id,
        sender_url: $scope.user.image_url,
        sender_username: $scope.user.profile_url_param,
        sender_name: $scope.user.name,
        sender_email: $scope.user.email,
        recipient_mail: $scope.account.email,
        recipient_url: $scope.account.image_url,
        recipient_username: $scope.account.profile_url_param,
        text: $scope.text.msg
      };
      $.post('/user/' + _id + '/messages', _data, function (response, error) {
        $scope.text.msg = null;
      })
    };

    var load_connections = function () {
      var deferred = $q.defer();

      $timeout(function () {
        $http({
          method: 'GET',
          url: '/connections/' + $scope.user.email + '/get'
        }).success(function (__data) {
          $scope.user.connections = __data.response;
        }).error(function (error) {
          console.log(error);
          // location.href = '/logout';
        });
        deferred.resolve('connections');
      }, 10);

      return deferred.promise;
    };

    var load_groups = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/groups/' + $scope.user.email + '/get';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.user.groups = __data.response;
        }).error(function (error) {
          console.log(error);
          // location.href = '/logout';
        });
        deferred.resolve('groups');
      }, 10);

      return deferred.promise;
    };

    var load_posts = function () {
      $scope.thread4Status = 'Started';
      var deferred = $q.defer();

      $timeout(function () {
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost?filter=(tag%3Dpost)and(status%3D1)and(addedby%3D" + $scope.user.email + ")";
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.user.posts = __data.resource.reverse();
        }).error(function (error) {
          location.href = '/logout';
        });
        deferred.resolve('posts');
      }, 10);

      return deferred.promise;
    };

    $scope.startParallel = function () {
      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_connections(), load_posts(), load_requests(), load_groups(), load_talents()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          location.href = '/logout';
        }
      );
    };

    $scope.init = function () {

      $scope.loading = true;
      $scope.message_sent = false;

      $scope.user = {
        "profile": {},
        "connections": [],
        "notifications": [],
        "groups": [],
        "requests": [],
        "talents": []
      };
      $scope.message = {"success": null, "error": null};
      $scope.text = null;
      $scope.request_text = false;

      $timeout(function () {
        var _data = $cookieStore.get('user_profile');
        $scope.$apply(function () {
          $scope.user = _data;
          $scope.startParallel();
        });
        $scope.loading = false
      }, 10);
    };

    $scope.acceptReq = function (note_id, name, username, image_url, email, is_mentor) {
      $scope.loading = true;
      $http({
        method: 'POST',
        url: '/mentors/accept',
        data: {
          "email": $scope.user.email,
          "recipient_mail": email,
          "recipient": name,
          "name": $scope.user.name,
          "note_id": note_id,
          "mentor_username": $scope.user.profile_url_param,
          "username": username,
          "image_url": image_url,
          "mentor_image_url": $scope.user.image_url,
          "is_mentor": is_mentor,
          "mentor_check": $scope.user.is_mentor
        }
      }).success(function (data) {
        location.reload();
        $scope.loading = false;
      }).error(function (error) {
        // location.href = '/logout';
      });
    };

    $scope.rejectReq = function (note_id, email) {
      $scope.loading = true;
      $http({
        method: 'POST',
        url: '/mentors/reject',
        data: {
          "email": $scope.user.email,
          "note_id": note_id,
          "recipient_mail": email
        }
      }).success(function (data) {
        location.reload();
        $scope.loading = false;
      }).error(function (error) {
        // location.href = '/logout';
      });
    }

  });

  module.controller('NotificationController', function ($scope, $timeout, $cookieStore, $http, $q) {

    var load_notifications = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/notifications/' + $scope.user.email + '/get';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          var notes = __data.response;
          if (notes && notes.length > 0) {
            $scope.user.notifications = notes;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('notes');
      }, 500);

      return deferred.promise;
    };

    $scope.startParallel = function () {
      var startTime = new Date().getTime();
      $scope.overallStatus = 'Running in parallel';

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_notifications()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {

      $scope.loading = true;

      $timeout(function () {
        $scope.user = $cookieStore.get('user_profile');
        $scope.startParallel();
        $scope.loading = false;
      }, 10);
    };

    $scope.getID = function () {
      $scope.user = $cookieStore.get('user_profile');
      $scope.data = {"note": {}};
      var _id = location.pathname.split('/').pop();
      $timeout(function () {
        $http({
          method: 'GET',
          url: '/notification/' + _id + '/get'
        }).success(function (data) {
          $scope.data.note = data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 5)
    };

    $scope.acceptConnection = function (note) {
      var datar = {
        "params": [
          {
            "name": "pri_name",
            "param_type": "string",
            "value": note.actor
          },
          {
            "name": "pri_username",
            "param_type": "string",
            "value": note.username
          },
          {
            "name": "pri_email",
            "param_type": "string",
            "value": note.recipient_mail
          },
          {
            "name": "pri_image",
            "param_type": "string",
            "value": note.image_url
          },
          {
            "name": "pri_mentor",
            "param_type": "string",
            "value": note.is_mentor == true ? 1: 0
          },
          {
            "name": "pri_admin",
            "param_type": "string",
            "value": $scope.user.email
          },
          {
            "name": "sec_name",
            "param_type": "string",
            "value": $scope.user.name
          },
          {
            "name": "sec_username",
            "param_type": "string",
            "value": $scope.user.profile_url_param
          },
          {
            "name": "sec_email",
            "param_type": "string",
            "value": $scope.user.email
          },
          {
            "name": "sec_image",
            "param_type": "string",
            "value": $scope.user.image_url
          },
          {
            "name": "sec_mentor",
            "param_type": "string",
            "value": $scope.user.is_mentor == true ? 1: 0
          },
          {
            "name": "sec_admin",
            "param_type": "string",
            "value": note.recipient_mail
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: 'http://178.62.199.243:8180/api/v2/prime/_proc/AddConnection',
        headers: {
          "cache-control": "no-cache",
          "x-dreamfactory-session-token": $scope.user.session,
          "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
        },
        data: datar
      }).success(function (data) {
        console.log(data);
      })
      .error(function (err) {
        console.log(err);
      });
    };

    $scope.accept = function (note) {
      var _datar = {
        "email": $scope.user.email,
        "recipient_mail": note.recipient_mail,
        "recipient": note.actor,
        "name": $scope.user.name,
        "note_id": note.id,
        "username": note.username,
        "mentor_username": $scope.user.profile_url_param,
        "image_url": note.image_url,
        "mentor_image_url": $scope.user.image_url,
        "is_mentor": note.is_mentor,
        "mentor_check": $scope.user.is_mentor,
        "session": $scope.user.session
      };
      var url = '/connections/accept';
      if ($scope.data.note.verb == 'group_request') {
        url = '/groups/accept';
      }
      else if ($scope.data.note.verb == 'mentor_request') {
        url = '/mentors/accept';
      }
      $http({
        method: 'POST',
        url: url,
        data: _datar
      }).success(function (data) {
        location.href = '/talents';
      }).error(function (error) {
        // location.href = '/logout';
      });
    };

    $scope.reject = function () {
      var url = '/connections/reject';
      if ($scope.data.note.verb == 'group_request') {
        url = '/groups/reject'
      }
      else if ($scope.data.note.verb == 'mentor_request') {
        url = '/mentors/reject';
      }
      var datar = {
        "email": $scope.user.email,
        "recipient": $scope.data.note.recipient_mail,
        "note_id": $scope.data.note.id,
        "name": $scope.data.note.name
      };
      $http({
        method: 'POST',
        url: url,
        data: datar
      }).success(function (data) {
        location.href = '/talents';
      }).error(function (error) {
        // location.href = '/logout';
      });
    }

  });

  module.controller('ConversationController', function ($scope, $timeout, $cookieStore, $http) {

    $scope.load_convos = function () {
      $timeout(function () {
        var url = '/conversations/' + $scope.user.email + '/get';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          if (__data.error == null) {
            $scope.conversations.data = __data.response;
            $scope.conversations.total = __data.total;
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10)
    };

    $scope.init = function () {

      $scope.loading = true;
      $scope.user = {};
      $scope.conversations = {'data': [], "total": 0, "currentPage": 1, "PerPage": 10, "from": 0, "prevPage": 1};

      $timeout(function () {
        var _data = $cookieStore.get('user_profile');
        $scope.$apply(function () {
          $scope.user = _data;
          $scope.load_convos();
        });
        $scope.loading = false;
      }, 10);
    };

    $scope.pageChanged = function (newPage) {
      if ($scope.conversations.prevPage > newPage) {
        $scope.conversations.from = $scope.conversations.from - $scope.conversations.PerPage;
      }
      else {
        $scope.conversations.from = $scope.conversations.from + $scope.conversations.PerPage;
      }
      $scope.conversations.prevPage = newPage;
      $scope.load_convos();
    };

  });

  module.controller('MessageController', function ($scope, $timeout, $cookieStore, $http, $q) {

    var load_convos = function (_email) {
      var x = re_order($scope.user.email + _email);
      var account = $scope.connections.find(function (res) {
        return res.email == _email;
      });
      $timeout(function () {
        var url = '/user/' + x + '/conversations';
        $http({
          method: 'GET',
          url: url
        }).success(function (data) {
          account.selected = true;
          account.conversations = data;
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10)
    };

    $scope.user = $cookieStore.get('user_profile');

    // socket.on($scope.user.email, function (data) {
    //   load_convos(data.email);
    // });

    var load_connections = function () {
      var deferred = $q.defer();

      $timeout(function () {
        $http({
          method: 'GET',
          url: '/connections/' + $scope.user.email + '/get'
        }).success(function (__data) {
          $scope.connections = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('connections');
      }, 10);

      return deferred.promise;
    };

    $scope.startParallel = function () {

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_connections()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {

      $scope.loading = true;

      $timeout(function () {
        $scope.user = $cookieStore.get('user_profile');
        $scope.connections = [];
        $scope.startParallel();
        $scope.loading = false;
        $scope.text = {msg: null};
        $scope.account = null;
      }, 10);
    };

    $scope.selectRecipient = function (acct) {
      $scope.account = acct;
      load_convos(acct.email);
    };

    $scope.sendMessage = function () {
      var _id = re_order($scope.user.email + $scope.account.email);
      var _data = {
        uniqueID: _id,
        sender_url: $scope.user.image_url,
        sender_username: $scope.user.profile_url_param,
        sender_name: $scope.user.name,
        sender_email: $scope.user.email,
        recipient_mail: $scope.account.email,
        recipient_url: $scope.account.image_url,
        recipient_username: $scope.account.profile_url_param,
        text: $scope.text.msg
      };
      $.post('/user/' + _id + '/messages', _data, function (response, error) {
        $scope.text.msg = null;
      })
    };

  });

  module.controller('SearchController', function ($scope, $timeout, $cookieStore, $http, $q) {

    var load_requested = function () {
      if ($scope.user.data) {
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/account/' + $scope.user.data.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.requested_connections = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('requested');
        }, 10);

        return deferred.promise;
      }
    };

    var load_connections = function () {
      if ($scope.user.data) {
        var deferred = $q.defer();

        var url = '/connections/' + $scope.user.data.email + '/get';

        $timeout(function () {
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.connections = __data.response;
            $scope.connected = $scope.connections.map(function (elem) {
              return elem.email;
            })
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('con');
        }, 10);

        return deferred.promise;
      }
    };

    var load_search = function () {
      var deferred = $q.defer();
      $timeout(function () {
        $.post('/search', {search_q: $scope.search_q}, function (resp, error) {
          $scope.$apply(function () {
            $scope.search_result.data = resp.response;
            var links = resp.response.map(function (elem) {
              return {"name": elem._type, 'active': false};
            });
            $scope.search_result.links = links.filter(function (elem, index, self) {
              return index == self.indexOf(elem);
            });
            $scope.search_result.links.push({'name': 'All', 'active': true});
          });
        });
        deferred.resolve('connected');
      }, 2);
      return deferred.promise;
    };

    $scope.startParallel = function () {
      $q.all([load_connections(), load_requested(), load_search()]).then(
        function (successResult) {
        }, function (failureReason) {
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {

      if (location.search.includes("?q=")) {
        var _q = location.search.split('=')[1];
        $scope.search_q = _q.replace('+', ' ');
      }

      $scope.loading = true;
      $scope.user = {"data": null, "notifications": [], "connections": []};
      $scope.requested_connections = [];
      $scope.connections = [];
      $scope.connected = []
      $scope.search_result = {"data": [], "links": []};

      if ($cookieStore.get('user_profile')) {
        $scope.user.data = $cookieStore.get('user_profile');
      }
      $scope.startParallel();
      $scope.loading = false;
    };

    $scope.selectContact = function (contact) {
      $scope.contact = contact;
    };

    $scope.sendRequest = function () {
      var _data = {
        requester: $scope.user.data.email,
        sender: $scope.user.data.name,
        requestee: $scope.contact.email,
        recipient: $scope.contact.name,
        text: $scope.message,
        image_url: $scope.user.data.image_url,
        username: $scope.user.data.profile_url_param,
        is_mentor: $scope.contact.is_mentor
      };
      $scope.loading = true;
      $.post('/send/mail', _data, function (response, error) {
        location.href = '/talents';
        $scope.loading = false;
      })
    };

    $scope.checkConnection = function (account) {
      if (!$scope.user.data) {
        return false;
      }
      else {
        return $scope.requested_connections.indexOf(account) != -1 || $scope.user.data.email == account || $scope.connected.indexOf(account) != -1;
      }
    };

    $scope.checkConnected = function (account) {
      return $scope.connected.indexOf(account) == -1;
    };

    $scope.selectRecipient = function (acct) {
      $scope.account = acct;
    };

    $scope.sendMessage = function () {
      var _id = re_order($scope.user.email + $scope.account.email);
      var _data = {
        uniqueID: _id,
        sender_url: $scope.user.image_url,
        sender_username: $scope.user.profile_url_param,
        sender_name: $scope.user.name,
        sender_email: $scope.user.email,
        recipient_url: $scope.account.image_url,
        recipient_username: $scope.account.profile_url_param,
        text: $scope.text_message
      };
      $scope.loading = true;
      $.post('/user/' + _id + '/messages', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

  });

  module.controller('UserProfileController', function ($scope, $cookieStore, $http, $timeout, $q) {

    var load_profile = function () {
      $timeout(function () {
        // var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?filter=profile_url_param%3D%22" + $scope._id + "%22";
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + $scope._id + '?id_field=profile_url_param&id_type=string';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.result.data.UserID = __data.UserID;
        }).error(function (error) {
          // location.href = "/logout";
        });
      }, 10)
    };

    var load_requested = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/account/' + $scope.user.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.requested_connections = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('requested');
        }, 10);

        return deferred.promise;
      }
    };

    var load_interests = function () {
      // use $q service to get deferred object
      var deferred = $q.defer();

      // use $timeout service to safely cause a delay
      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblinterests?filter=user%3D%22' + $scope.result.data.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.result.interests = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('thread1');
      }, 10);

      return deferred.promise;
    };

    var load_skills = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblskills?filter=user%3D%22' + $scope.result.data.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.result.skills = __data.resource;
        }).error(function (error) {
        });
        deferred.resolve('skills');
      }, 10);

      return deferred.promise;
    };

    var load_experiences = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblexperience?filter=user%3D%22' + $scope.result.data.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.result.experiences = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('experiences');
      }, 500);

      return deferred.promise;
    };

    var load_education = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tbleducation?filter=user%3D%22' + $scope.result.data.email + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.result.education = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('education');
      }, 10);

      return deferred.promise;
    };

    var load_connections = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        var url = '/connections/' + $scope.user.email + '/get';

        $timeout(function () {
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.connections = __data.response.docs;
            $scope.connected = $scope.connections.map(function (elem) {
              return elem.email;
            })
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('thread4');
        }, 10);

        return deferred.promise;
      }
    };

    var load_recommendation = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/elastic/profiles';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.recommendations = __data.response;
          if ($scope.user) {
            $scope.recommendations = $scope.recommendations.filter(function (res) {
              return res.email != $scope.user.email;
            });
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('recommend');
      }, 10);

      return deferred.promise;
    };

    $scope.startParallel = function () {
      $q.all([load_interests(), load_experiences(), load_education(), load_skills(), load_connections(),
        load_requested(), load_recommendation()]).then(
        function (successResult) {
        }, function (failureReason) {
          // location.href = '/logout';
        }
      );
    };

    var load_search = function (param) {
      $timeout(function () {
        $.post('/elastic/people/search', {
          field: "username",
          value: param
        }, function (data, status) {
          $scope.result.data = data.response[0];
          $scope.user = $cookieStore.get('user_profile');
          $scope.startParallel();
        });
      }, 2)
    };

    $scope.init = function () {
      $scope.result = {"data": null, "interests": [], "skills": [], "experiences": [], "education": []};
      $scope._id = location.pathname.split('/')[2];
      $scope.requested_connections = [];
      $scope.connections = [];
      $scope.connected = [];
      $scope.recommendations = [];
      load_search($scope._id);
    };

    $scope.selectContact = function (contact) {
      $scope.contact = contact;
    };

    $scope.sendRequest = function () {
      var _data = {
        requester: $scope.user.email,
        sender: $scope.user.name,
        requestee: $scope.contact.email,
        recipient: $scope.contact.name,
        text: $scope.message,
        image_url: $scope.user.image_url,
        username: $scope.user.profile_url_param,
        is_mentor: $scope.contact.is_mentor
      };
      $scope.loading = true;
      $.post('/send/mail', _data, function (response, error) {
        location.href = '/talents';
        $scope.loading = false;
      })
    };

    $scope.checkConnection = function (account) {
      if (!$scope.user) {
        return false;
      }
      return $scope.requested_connections.indexOf(account) != -1 || $scope.connected.indexOf(account) != -1 || $scope.user.email == account;
    };

    $scope.selectRecipient = function (acct) {
      $scope.account = acct;
    };

    $scope.sendMessage = function () {
      var _id = re_order($scope.user.email + $scope.account.email);
      var _data = {
        uniqueID: _id,
        sender_url: $scope.user.image_url,
        sender_username: $scope.user.profile_url_param,
        recipient_url: $scope.account.image_url,
        recipient_username: $scope.account.profile_url_param,
        text: $scope.text_message
      };
      $scope.loading = true;
      $.post('/user/' + _id + '/messages', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

  });

  module.controller('AdminController', function ($scope, $cookieStore, $http, $timeout, $q, S3UploadService, S_URL) {

    $scope.error_message = null;

    $scope.loginUser = function () {
      $scope.loading = true;
      $http({
        url: '/login',
        method: 'POST',
        headers: {
          "cache-control": "no-cache"
        },
        data: {
          username: $scope.username,
          password: $scope.password
        }
      }).success(function (data) {
        $cookieStore.put('admin', data.response);
        $timeout(function () {
          location.href = '/';
        });
        $scope.loading = false;
      }).error(function (error) {
        $scope.error = error.error.message;
        $scope.loading = false;
      });
    };

    var load_posts = function () {
      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.data.posts = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    var load_groups = function () {
      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblgroups';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.data.groups = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    var load_jobs = function () {
      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tbljobs';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.jobs.total = __data.resource.length;
          $scope.jobs.data = __data.resource.reverse();
        }).error(function (error) {
          location.href = '/logout';
        });
      }, 10);
    };

    var load_talents = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (__data) {
          $scope.data.talents = __data.resource;
          $scope.data.mentors = $scope.data.talents.filter(function (elem) {
            return elem.is_mentor == 1;
          })
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('talents');
      }, 5);
      return deferred.promise;
    };

    var load_requested = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/requested/';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.data.requested = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('requested');
      }, 10);

      return deferred.promise;
    };

    var startParallel = function () {
      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([load_jobs(), load_groups(), load_talents(), load_requested(), load_posts()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // $scope.overallStatus = 'Failed: ' + failureReason;
          // location.href = '/logout';
        }
      );
    };

    var checkUser = function () {
      var user = $cookieStore.get('admin');
      var url = S_URL + "/api/v2/user/session";
      console.log(url);
      $http({
        method: 'GET',
        url: url,
        headers: {
          "x-dreamfactory-session-token": user.session,
          "cache-control": "no-cache"
        }
      }).success(function (data) {
        user.session = data.session_token;
        $cookieStore.put('admin', user);
      }).
      error(function (error) {
        console.log(error);
        // location.href = '/logout';
      });
    };

    $scope.options = {
      height: 300,
      focus: true,
      toolbar: [
        ['edit', ['undo', 'redo']],
        ['headline', ['style']],
        ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],
        ['fontface', ['fontname']],
        ['textsize', ['fontsize']],
        ['fontclr', ['color']],
        ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video', 'hr']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']]
      ],
      popover: {
        image: [
          ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
          ['float', ['floatLeft', 'floatRight', 'floatNone']],
          ['remove', ['removeMedia']]
        ],
        link: [
          ['link', ['linkDialogShow', 'unlink']]
        ],
        air: [
          ['color', ['color']],
          ['font', ['bold', 'underline', 'clear']],
          ['para', ['ul', 'paragraph']],
          ['table', ['table']],
          ['insert', ['link', 'picture']]
        ]
      }
    };

    $scope.init = function () {

      $scope.loading = true;
      $scope.jobs = {'data': [], "total": 0, "currentPage": 1, "PerPage": 10, "from": 0, "prevPage": 1};
      $scope.data = {"groups": [], "talents": [], "mentors": [], "new_users": [], "posts": [], "requested": []};
      $scope.user = $cookieStore.get('admin');
      startParallel();
      $scope.loading = false;
      $scope.post = {"message": null, "image_url": null};
      $scope.edited = {"body": null};
      $cookieStore.put('post', '');
    };

    $scope.init_post = function () {
      var _id = location.pathname.split('/')[2];
      $timeout(function () {
        $http({
          method: 'GET',
          url: "http://178.62.199.243:8180/api/v2/prime/_table/tblpost/" + _id,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          $scope.edited = data;
        }).error(function (err) {
          location.href = '/posts';
        });
      });
    };

    $scope.acceptReq = function (account) {
      $scope.loading = true;
      var url = '/mentors/accept';
      if (account.type == null || account.type == 'verification') {
        url = '/mentors/verify'
      }
      $http({
        method: 'POST',
        url: url,
        data: {
          "email": account.email,
          "name": account.username
        }
      }).success(function (data) {
        $scope.loading = false;
        location.reload();
      }).error(function (error) {
        // location.href = '/logout';
      });
    };

    $scope.rejectReq = function (account) {
      var url = '/mentors/reject';
      $http({
        method: 'POST',
        url: url,
        data: {
          "email": account.email,
          "name": account.username
        }
      }).success(function (data) {
        location.reload();
      }).error(function (error) {
        // location.href = '/logout';
      });
    };

    $scope.deleteJob = function (job) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/job/delete',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          },
          data: {
            title: job.title,
            posted_by: job.posted_by
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.deleteGroup = function (grp_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/group/delete',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          },
          data: {
            query: grp_id
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.deleteUser = function (email) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'GET',
          url: 'http://talentsandmentors.com:8180/rest/system/user?filter=email%3D' + email + '%20',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          if(data.resource.length > 0) {
            $http({
              method: 'POST',
              url: '/user/' + data.resource[0].id + '/delete',
              headers: {
                "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
              },
              data: {
                query: email
              }
            }).success(function (data) {
              $scope.loading = false;
              location.reload();
            }).error(function (error) {
              // location.href = '/logout';
            });
          }
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.markMentor = function (account) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/mentor/mark',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          },
          data: {
            email: account
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.unMark = function (account) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/mentor/unmark',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          },
          data: {
            email: account
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.Verify = function (account) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/people/verify',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          },
          data: {
            email: account
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.unVerify = function (account) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/people/unverify',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          },
          data: {
            email: account
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.banPost = function (post_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/ban/' + post_id + '/post',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.deletePost = function (post_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'DELETE',
          url: "http://178.62.199.243:8180/api/v2/prime/_table/tblpost/" + post_id,
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          location.reload();
          $scope.loading = false;
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.activatePost = function (post_id) {
      $scope.loading = true;
      $timeout(function () {
        $http({
          method: 'POST',
          url: '/activate/' + post_id + '/post',
          headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    $scope.updatePost = function () {
      $scope.update_post($scope.edited.id, $scope.edited.body, $scope.edited.image);
    };

    $scope.update_post = function (pid, body, image_url) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/EditPost";
      var datar = {
        "params": [
          {
            "name": "pid",
            "param_type": "string",
            "value": pid
          },
          {
            "name": "vbody",
            "param_type": "string",
            "value": body
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        data: datar
      }).success(function (data) {
        $timeout(function () {
          // location.reload();
          location.href = '/posts';
          $scope.loading = false;
        }, 10)
      })
        .error(function (err) {
          console.log(err);
          // location.href = '/logout';
        });
    };

    // $scope.selectPost = function (post) {
    //   $scope.edited = post;
    //   $cookieStore.put('post', post);
    //   location.href = '/edit'
    // };

    $scope.update_profile = function () {
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/UpdateProfile";
      var data = {
        "params": [
          {
            "name": "vemail",
            "param_type": "string",
            "value": $scope.user.email || ''
          },
          {
            "name": "vphone",
            "param_type": "string",
            "value": $scope.user.phone || ''
          },
          {
            "name": "vbirthdate",
            "param_type": "string",
            "value": $scope.user.birthdate || ''
          },
          {
            "name": "vmarital",
            "param_type": "string",
            "value": $scope.user.marital || ''
          },
          {
            "name": "industry",
            "param_type": "string",
            "value": $scope.user.industry || ''
          },
          {
            "name": "vloc",
            "param_type": "string",
            "value": $scope.user.location || ''
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": $scope.user.image_url || ''
          },
          {
            "name": "vbanner",
            "param_type": "string",
            "value": $scope.user.banner_url || ''
          },
          {
            "name": "vpublic",
            "param_type": "string",
            "value": $scope.user.username || ''
          },
          {
            "name": "vsummary",
            "param_type": "string",
            "value": $scope.user.summary || ''
          },
          {
            "name": "vmentor",
            "param_type": "string",
            "value": $scope.user.is_mentor || ''
          },
          {
            "name": "verified",
            "param_type": "string",
            "value": $scope.user.is_verified || ''
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      var datar = JSON.stringify(data);
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        data: datar
      }).success(function (resp) {
        location.reload();
        $scope.loading = false;
      }).error(function (err) {
        // location.href = '/logout';
      });
    };

    $scope.uploadFiles = function (files) {
      $scope.Files = files;
      if (files && files.length > 0) {
        angular.forEach($scope.Files, function (file, key) {
          S3UploadService.Upload(file).then(function (result) {
            // Mark as success
            file.Success = true;
            var url = "https://s3.amazonaws.com/talents/" + file.name;
            $scope.update_post($scope.post.id, $scope.post.message, url);
          }, function (error) {
            $scope.Error = error;
          }, function (progress) {
            file.Progress = (progress.loaded / progress.total) * 100
          });
        });
      }
    };

    $scope.updateFiles = function (files) {
      $scope.Files = files;
      if (files && files.length > 0) {
        angular.forEach($scope.Files, function (file, key) {
          S3UploadService.Upload(file).then(function (result) {
            // Mark as success
            file.Success = true;
            $scope.user.image_url = "https://s3.amazonaws.com/talents/" + file.name;
            $scope.update_profile();
          }, function (error) {
            $scope.Error = error;
          }, function (progress) {
            file.Progress = (progress.loaded / progress.total) * 100
          });
        });
      }
    };

    $scope.postMessage = function (body, image_url) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/CreatePost";
      var datar = {
        "params": [
          {
            "name": "vbody",
            "param_type": "string",
            "value": body || ''
          },
          {
            "name": "vuser",
            "param_type": "string",
            "value": 'admin@talents.com'
          },
          {
            "name": "vtag",
            "param_type": "string",
            "value": 'admin'
          },
          {
            "name": "vimage_url",
            "param_type": "string",
            "value": 'https://s3.amazonaws.com/talents/talents.jpg'
          },
          {
            "name": "vposter",
            "param_type": "string",
            "value": 'Akeem Mustapha'
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          },
          {
            "name": "vcomments",
            "param_type": "string",
            "value": 0
          },
          {
            "name": "vlikes",
            "param_type": "string",
            "value": 0
          },
          {
            "name": "vgroup",
            "param_type": "string",
            "value": ''
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        data: datar
      }).success(function (data) {
        $timeout(function () {
          location.reload();
          $scope.loading = false;
        }, 10)
      })
        .error(function (err) {
          // renew()
        });
    };

    $scope.createFiles = function (files) {
      $scope.Files = files;
      if (files && files.length > 0) {
        angular.forEach($scope.Files, function (file, key) {
          S3UploadService.Upload(file).then(function (result) {
            // Mark as success
            file.Success = true;
            $scope.postMessage($scope.post.message, "https://s3.amazonaws.com/talents/" + file.name);
          }, function (error) {
            $scope.Error = error;
          }, function (progress) {
            file.Progress = (progress.loaded / progress.total) * 100
          });
        });
      }
    };

  });

  module.controller('GroupController', function ($scope, $cookieStore, $http, $timeout, $q, S3UploadService, S_URL) {

    var load_groups = function () {
      var url = '/elastic/groups';
      $http({
        method: 'GET',
        url: url
      }).success(function (__data) {
        $scope.groups.data = __data.response;
      }).error(function (error) {
        // location.href = '/logout';
      });
    };

    var load_requested = function () {
      if ($scope.user.data) {
        var deferred = $q.defer();

        $timeout(function () {
          var url = '/groups/' + $scope.user.data.email + '/requested';
          $http({
            method: 'GET',
            url: url
          }).success(function (__data) {
            $scope.requested_groups = __data.response.map(function (elem) {
              return elem.user;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('requested');
        }, 10);

        return deferred.promise;
      }
    };

    var load_connected = function () {
      if ($scope.user) {
        var deferred = $q.defer();

        $timeout(function () {
          $http({
            method: 'GET',
            url: '/groups/' + $scope.user.email + '/get'
          }).success(function (__data) {
            $scope.connected = __data.response.map(function (elem) {
              return elem.email;
            });
          }).error(function (error) {
            // location.href = '/logout';
          });
          deferred.resolve('connected');
        }, 10);

        return deferred.promise;
      }
    };

    $scope.startParallel = function () {
      $q.all([load_connected(), load_requested(), load_groups()]).then(
        function (successResult) {
        }, function (failureReason) {
          // location.href = '/logout';
        }
      );
    };

    $scope.init = function () {

      $scope.loading = true;
      $scope.image_url = $scope.location = $scope.website = '';
      $scope.groups = {"data": []};
      $scope.user = {"data": null, "notifications": [], "groups": []};
      $scope.requested_groups = [];

      $scope.result1 = '';
      $scope.options1 = null;
      $scope.details1 = '';

      if ($cookieStore.get('user_profile')) {
        $scope.user.data = $cookieStore.get('user_profile');
        $scope.email = $scope.user.data.email
      }
      $scope.startParallel();
      $scope.loading = false;
    };

    var my_groups = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/groups/' + $scope.user.data.email + '/get';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.user.groups = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('groups');
      }, 10);

      return deferred.promise;
    };

    var load_members = function () {
      var deferred = $q.defer();

      $timeout(function () {
        var url = '/group/' + $scope.search_q + '/get';
        $http({
          method: 'GET',
          url: url
        }).success(function (__data) {
          $scope.members = __data.response;
        }).error(function (error) {
          // location.href = '/logout';
        });
        deferred.resolve('members');
      }, 10);

      return deferred.promise;
    };

    var load_posts = function () {
      $timeout(function () {
        var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost?filter=group_id%3D%22' + $scope.group.data.id + '%22';
        $http({
          method: 'GET',
          url: url,
          headers: {
            "cache-control": "no-cache",
            "x-dreamfactory-session-token": $scope.user.data.session,
            "x-dreamfactory-api-key": '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88'
          }
        }).success(function (__data) {
          $scope.group.posts = __data.resource;
        }).error(function (error) {
          // location.href = '/logout';
        });
      }, 10);
    };

    var load_group = function () {
      $.post('/elastic/groups/search', {
        field: "id",
        value: $scope.search_q
      }, function (resp, error) {
        $timeout(function () {
          $scope.$apply(function () {
            try {
              if (resp.response.length > 0) {
                var grp = resp.response.filter(function (elem) {
                  return elem.id == $scope.search_q
                });
                $scope.group.data = grp[0];
                $.post('/elastic/people/search', {
                  field: "email",
                  value: $scope.group.data.email
                }, function (resq, err) {
                  $scope.group.admin = resq.response[0];
                  load_posts();
                });
              }
            } catch (err) {
              location.href = '/home'
            }
          });
        }, 2);
      });
    };

    $scope.initialize = function () {
      $scope.loading = true;
      $scope.image_url = $scope.location = $scope.website = '';
      $scope.user = {"data": null, "notifications": [], "groups": []};

      if ($cookieStore.get('user_profile')) {
        $scope.user.data = $cookieStore.get('user_profile');
        $scope.email = $scope.user.data.email
      }

      // start workers and pass array of their promises to $q.all(), then wait on all promises to be fulfilled, then tally the elapsed time and update status message
      $q.all([my_groups()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // location.href = '/logout';
        }
      );
      $scope.loading = false;
    };

    $scope.init_individual = function () {
      $scope.loading = true;
      $scope.user = {"data": null, "notifications": []};
      $scope.members = [];
      $scope.group = {"data": null, "admin": null, "posts": []};

      if ($cookieStore.get('user_profile')) {
        $scope.user.data = $cookieStore.get('user_profile');
        $scope.email = $scope.user.data.email
      }

      var _q = location.pathname.split('/')[2];
      $scope.search_q = _q.replace('%20', ' ');

      $q.all([load_members(), load_group()]).then(
        function (successResult) { // execute this if ALL promises are resolved (successful)
        }, function (failureReason) { // execute this if any promise is rejected (fails) - we don't have any reject calls in this demo
          // $scope.overallStatus = 'Failed: ' + failureReason;
          // location.href = '/logout';
        }
      );
      $scope.loading = false;
    };

    $scope.indexGroup = function (data, url) {
      $.post(url, data, function (data, status) {
      })
    };

    $scope.addGroup = function () {
      $scope.loading = true;
      $http({
        method: 'GET',
        url: S_URL + "/api/v2/user/session",
        headers: {
          "x-dreamfactory-session-token": $scope.user.data.session,
          "cache-control": "no-cache"
        }
      }).success(function (_data) {
        $scope.user.data.session = _data.session_token;
        $cookieStore.put('user_profile', $scope.user.data);
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_proc/AddGroup";
        var data = {
          "params": [
            {
              "name": "vname",
              "param_type": "string",
              "value": $scope.name
            },
            {
              "name": "vsummary",
              "param_type": "string",
              "value": $scope.summary
            },
            {
              "name": "vdesc",
              "param_type": "string",
              "value": $scope.description
            },
            {
              "name": "vemail",
              "param_type": "string",
              "value": $scope.email
            },
            {
              "name": "vweb",
              "param_type": "string",
              "value": $scope.website
            },
            {
              "name": "vloc",
              "param_type": "string",
              "value": $scope.location
            },
            {
              "name": "vimage",
              "param_type": "string",
              "value": $scope.image_url
            }
          ],
          "schema": {
            "_field_name_": "string"
          },
          "wrapper": "string",
          "returns": "string"
        };
        var datar = JSON.stringify(data);
        $http({
          async: true,
          crossDomain: true,
          method: 'POST',
          url: url,
          headers: {
            "x-dreamfactory-api-key": "36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88",
            "x-dreamfactory-session-token": $scope.user.data.session,
            "cache-control": "no-cache"
          },
          data: datar
        }).success(function (_data) {
          $scope.indexGroup({
            name: $scope.name,
            email: $scope.email || '',
            summary: $scope.summary || '',
            description: $scope.description || '',
            image_url: $scope.image_url || '',
            location: $scope.location || '',
            website: $scope.website || '',
            id: $scope.name
          }, '/elastic/groups/index');
          $scope.loading = false;
          location.reload();
        })
          .error(function (err) {
            if (err.error.code == 401) {
              // location.href = '/logout';
            }
            else {
              $scope.error_message = "Group Name already taken";
            }
          });
      }).
      error(function (error) {
        // location.href = '/logout';
      });
    };

    $scope.checkUser = function (email) {
      if ($scope.user.data) {
        return email != $scope.user.data.email;
      }
      return false
    };

    $scope.checkRequested = function (grp_id) {
      if ($scope.user.data) {
        return $scope.requested_groups.indexOf(grp_id) != -1;
      }
      return false
    };

    $scope.Join = function (grp) {
      $scope.loading = true;
      var _data = {
        requester: $scope.user.data.email,
        sender: $scope.user.data.name,
        requestee: grp.email,
        recipient: grp.name,
        text: "I want to join your group",
        image_url: $scope.user.data.image_url,
        username: $scope.user.data.profile_url_param,
        is_mentor: $scope.user.data.is_mentor,
        group_id: grp.id
      };
      $.post('/group/request', _data, function (response, error) {
        location.reload();
        $scope.loading = false;
      })
    };

    $scope.uploadFiles = function (files) {
      $scope.Files = files;
      if (files && files.length > 0) {
        angular.forEach($scope.Files, function (file, key) {
          S3UploadService.Upload(file).then(function (result) {
            // Mark as success
            file.Success = true;
            var url = "https://s3.amazonaws.com/talents/" + file.name;
            $scope.post_update($scope.text, url);
          }, function (error) {
            $scope.Error = error;
          }, function (progress) {
            file.Progress = (progress.loaded / progress.total) * 100
          });
        });
      }
    };

    $scope.post_update = function (body, image_url) {
      $scope.loading = true;
      var url = "http://178.62.199.243:8180/api/v2/prime/_proc/CreatePost";
      var datar = {
        "params": [
          {
            "name": "vbody",
            "param_type": "string",
            "value": body
          },
          {
            "name": "vuser",
            "param_type": "string",
            "value": $scope.user.data.email
          },
          {
            "name": "vtag",
            "param_type": "string",
            "value": 'group'
          },
          {
            "name": "vimage_url",
            "param_type": "string",
            "value": $scope.user.data.image_url
          },
          {
            "name": "vposter",
            "param_type": "string",
            "value": $scope.user.data.name
          },
          {
            "name": "vimage",
            "param_type": "string",
            "value": image_url
          },
          {
            "name": "vcomments",
            "param_type": "string",
            "value": 0
          },
          {
            "name": "vlikes",
            "param_type": "string",
            "value": 0
          },
          {
            "name": "vgroup",
            "param_type": "string",
            "value": $scope.group.data.id
          }
        ],
        "schema": {
          "_field_name_": "string"
        },
        "wrapper": "string",
        "returns": "string"
      };
      $http({
        async: true,
        crossDomain: true,
        method: 'POST',
        url: url,
        headers: {
          "x-dreamfactory-api-key": "3fc473cf54b2f4316b8d46bd3207248ef3b24adc93430d97f033e7de80e5d433",
          "x-dreamfactory-session-token": $scope.user.data.session,
          "cache-control": "no-cache"
        },
        data: datar
      }).success(function (data) {
        $timeout(function () {
          location.reload();
          $scope.loading = false;
        }, 10)
      })
        .error(function (err) {
          // location.href = '/logout';
        });
    };

  });

  module.controller('InviteController', function ($scope, $cookieStore, $http, $q, $timeout) {

    $scope.choices = [{email: ''}];
    $scope.error = {"message": null};
    $scope.user = $cookieStore.get('user_profile');

    $scope.addNewChoice = function () {
      var newItemNo = $scope.choices.length + 1;
      $scope.choices.push({'email': '' + newItemNo});
    };

    $scope.removeChoice = function () {
      var lastItem = $scope.choices.length - 1;
      $scope.choices.splice(lastItem);
    };

    $scope.sendChoice = function () {
      var emails = $scope.choices.map(function (elem) {
        return elem.email
      }).join();
      if (emails != ',') {
        $scope.loading = true;
        $http({
          method: 'POST',
          url: '/invite',
          data: {
            sender: $scope.user.email,
            emails: emails
          }
        }).success(function (data) {
          $scope.loading = false;
          location.reload()
        }).error(function (error) {
          $scope.loading = false;
          location.reload()
        });
      }
      else {
        $scope.error.message = 'Add an email address';
      }
    };

  })

})();



