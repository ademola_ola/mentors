var mongoose = require('mongoose');

var commentSchema = mongoose.Schema({
        postID: String,
        body: String,
        author: String,
        username: String,
        image_url: String,
        name: String
    },
    {
        timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

var Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;