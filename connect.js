/**
 * Created by stikks-workstation on 2/4/17.
 */

var lupus = require('lupus');
var express = require('express');
var cluster = require("cluster");
var app = express();
var http = require('http').Server(app);
var request = require('request-promise');
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/commentSystem');


var Connection = require('./models/Connection');

request({
    url: 'http://178.62.199.243:8180/rest/system/user',
    method: 'GET',
    headers: {
        "Authorization": 'Basic c3R5Y2NzQGdtYWlsLmNvbTpvbGFkaXBvMQ=='
    }
}, function (error, response, body) {
    var x = JSON.parse(body).resource;
    lupus(0, x.length, function(n) {
        var z = x[n];
        var url = "http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile?filter=email%3D%22" + z.email + "%22";
        request({
            url: url,
            method: 'GET',
            headers: {
                "Authorization": 'Basic c3R5Y2NzQGdtYWlsLmNvbTpvbGFkaXBvMQ=='
            }
        }, function (err, resp, bd) {
            var y = JSON.parse(bd);
            if (y.resource.length > 0 && z.email != 'phakeem1@gmail.com') {
                var conn = new Connection();
                conn.creator = 'phakeem1@gmail.com';
                conn.name = z.name;
                conn.is_mentor = y.resource[0].is_mentor;
                conn.email = z.email;
                conn.username = y.resource[0].profile_url_param;
                conn.image_url = y.resource[0].image_url;
                conn.save(function (resp) {
                    console.log(resp);
                });

                var _conn = new Connection();
                _conn.creator = z.email;
                _conn.name = 'Akeem Mustapha';
                _conn.is_mentor = true;
                _conn.email = 'phakeem1@gmail.com';
                _conn.username = 'AkeemM';
                _conn.image_url = 'https://s3.amazonaws.com/talents/me.jpg';
                _conn.save(function (elem) {
                    console.log(elem);
                });
            }
        });
    }, function() {
        console.log('All done!');
    });
});

http.listen(4600);