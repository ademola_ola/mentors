/**
 * Created by stikks-workstation on 1/22/17.
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const LikeSchema = mongoose.Schema({
        postID: String,
        body: String,
        author: String,
        username: String,
        image_url: String,
        name: String,
        uniqueID: String
    },
    {
        timestamps: true
    });

module.exports = mongoose.model('Like', LikeSchema);
