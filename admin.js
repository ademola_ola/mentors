/**
 * Created by stikks-workstation on 12/27/16.
 */

var express = require('express');
var cor = require('cors');
var app = express();
var request = require('request-promise');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var sg = require('sendgrid')('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');
var es = require('elasticsearch');
var path = require('path');
var compression = require('compression');
var cluster = require("cluster");
var numCPUs = require("os").cpus().length;

// socket.io
var http = require('http').Server(app);
var io = require("socket.io")(http);

io.on('connection', function (socket) {
    console.log("A user is connected");
    socket.on('status added', function (status) {
        add_status(status, function (res) {
            if (res) {
                io.emit('refresh feed', status);
            } else {
                io.emit('error');
            }
        });
    });
});

// var redis = require("redis")
//     , subscriber = redis.createClient('6379', '178.62.199.243')
//     , publisher = redis.createClient('6379', '178.62.199.243');

// subscriber.on("message", function(channel, message) {
//     client2.hgetall(message, function(err, res) {
//       res.key = msg;
//       io.sockets.emit(res);
//   });
// });

// subscriber.subscribe("comments");
//
// publisher.publish("comments", "haaaaai");
// publisher.publish("comments", "kthxbai");

// s3 integration
var s3Url = 's3://AKIAJ7YO3EAGMS5WBDWA:p9lMyKj9eZeES+wY7QGQv7e+K5Q+5ybwvt9dPH1Q@talents.s3.amazonaws.com';
var s3 = require('node-s3')(s3Url);

var client = new es.Client({
    host: '178.62.199.243:9200',
    log: 'trace'
});

client.ping({
    requestTimeout: Infinity,
    hello: "elasticsearch!"
}, function (error) {
    if (error) {
        console.trace('elasticsearch cluster is down!');
    } else {
        console.log('All is well');
    }
});

// var redis = require('redis');
// var redis_client = redis.createClient('6379', '178.62.199.243');

var Redis = require('ioredis'),
    redis_client = new Redis('6379', '178.62.199.243');

redis_client.on('connect', function () {
    console.log('Redis connected');
});

function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}


function MHGETALL(keys, cb) {

    redis_client.multi({pipeline: false});

    keys.forEach(function (key, index) {
        redis_client.hgetall(key);
    });

    redis_client.exec(function (err, result) {
        cb(err, result);
    });
}

function toObject(arr) {
    var one = arr.filter(function (res) {
        return arr.indexOf(res) % 2 == 0
    });
    var two = arr.filter(function (res) {
        return one.indexOf(res) == -1
    });

    var rv = {};
    for (var i = 0; i < two.length; ++i)
        rv[one[i]] = two[i];
    return rv;
}

app.use(cookieParser());
app.use(compression());

app.engine('html', require('ejs').renderFile);
app.set('views', path.join(__dirname, 'views/panel'));
app.set('view engine', 'html');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});


function isAuthenticated(req, res, next) {

    // CHECK IF A USER IS LOGGED IN
    var session = req.cookies['admin'];

    if (session) {
        return next();
    }
    else {
        res.redirect('/login');
    }
}

function notAuthenticated(req, res, next) {

    var session = req.cookies['admin'];

    if (session) {
        res.redirect('/');
    }
    else {
        return next();
    }
}

function sendMail(recipient, from, subject, body, url, content_type, from_name, recipient_name) {

    url = url || '';
    content_type = content_type || 'text/html';
    from_name = from_name || null;
    recipient_name = recipient_name || null;

    var request = sg.emptyRequest();
    request.body = {
        "content": [
            {
                "type": content_type,
                "value": body + '<br><br><br><table class="module" role="module" data-type="button" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;" data-attributes="%7B%22dropped%22%3Atrue%2C%22borderradius%22%3A6%2C%22buttonpadding%22%3A%2212%2C18%2C12%2C18%22%2C%22text%22%3A%22Accept%22%2C%22alignment%22%3A%22center%22%2C%22fontsize%22%3A16%2C%22url%22%3A%22%2525url%2525%22%2C%22height%22%3A%22%22%2C%22width%22%3A%22%22%2C%22containerbackground%22%3A%22%23ffffff%22%2C%22padding%22%3A%220%2C0%2C0%2C0%22%2C%22buttoncolor%22%3A%22%231188e6%22%2C%22textcolor%22%3A%22%23ffffff%22%2C%22bordercolor%22%3A%22%231288e5%22%7D"><tr><td style="padding: 0px 0px 0px 0px;" align="center" bgcolor="#ffffff"><table border="0" cellpadding="0" cellspacing="0" class="wrapper-mobile"><tr><td align="center" style="-webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; font-size: 16px;" bgcolor="#1188e6"><a href="' + url + '" ' + 'class="bulletproof-button" target="_blank" style="height: px; width: px; font-size: 16px; line-height: px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; padding: 12px 18px 12px 18px; text-decoration: none; color: #ffffff; text-decoration: none; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; border: 1px solid #1288e5; display: inline-block;">Accept</a></td></tr></table></td></tr></table>'
            }
        ],
        "from": {
            "email": from,
            "name": from_name
        },
        // "mail_settings": {
        //   "footer": {
        //     "enable": true,
        //     "html": "<p>Thanks</br>Talents & Mentors</p>",
        //     "text": "Thanks,/n Talents & Mentors"
        //   }
        // },
        "personalizations": [
            {
                "headers": {
                    "X-Accept-Language": "en",
                    // "X-Mailer": "MyApp"
                },
                "subject": subject,
                // "substitutions": {
                //   "id": "substitutions",
                //   "type": "object"
                // },
                "to": [
                    {
                        "email": recipient,
                        "name": recipient_name
                    }
                ]
            }
        ],
        "reply_to": {
            "email": "hello@talentsandmentors.com",
            "name": "Hello"
        },
        "subject": subject,
        "template_id": "f68ff518-999f-4ae4-89b8-359470a81eca",
        "url": "http://talentsandmentors.com:3600"
    };
    request.method = 'POST';
    request.path = '/v3/mail/send';
    sg.API(request, function (error, response) {
    })
}

app.use(cor());
app.use('/static', express.static('static'));
app.use('/images', express.static('static/images'));

const ADMIN_EMAIL = 'admin@talentsandmentors.com';
const LOGIN_EMAIL = 'admin@talents.com';
const ADMIN_PASSWORD = 'mustapha1';

app.get('/', isAuthenticated, function (req, res) {
    res.render('index');
});

app.post('/mail/talents', function (req, res) {
    if (req.body.text) {
        var text = req.body.text;
    }
    else {
        var text = req.body.email + " is requesting for your mentoring";
    }

    sendMail(req.body.recipient, req.body.email, req.body.email + " is requesting for mentoring", text);
});

app.post('/mail/mentors', function (req, res) {
    if (req.body.text) {
        var text = req.body.text;
    }
    else {
        var text = req.body.email + " is requesting to be a mentor";
    }

    sendMail("styccs@gmail.com", "hello@talentsandmentors.com", "Request to be a mentor - " + req.body.email, text);

    try {
        // save requested action
        redis_client.set("requested_" + req.body.email, 'requested', function (data, status) {
        });
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.post('/send/mail', function (req, res) {
    if (req.body.text) {
        var text = req.body.text;
    }
    else {
        var text = req.body.sender + " is requesting to connect with you.";
    }

    var _id = new Date().getTime();
    var uniq_id = new Buffer(req.body.requester + '_notifications_' + _id).toString('base64');

    var resp = sendMail(req.body.requestee, req.body.requester, "Connection Request from " + req.body.sender, text, 'http://talentsandmentors.com:3600/notifications/' + uniq_id);

    try {
        redis_client.hmset(uniq_id, {
            "id": uniq_id,
            'actor': req.body.sender,
            'recipient_mail': req.body.requester,
            'action': '',
            'verb': 'request',
            'subject': "Connection request",
            'message': req.body.sender + " is requesting to connect with you.",
            'image_url': req.body.image_url,
            "username": req.body.username,
            "name": req.body.recipient,
            "is_mentor": req.body.is_mentor
        });
        redis_client.sadd("notifications_" + req.body.requestee, uniq_id);

        var identifier = new Buffer(req.body.requester + '_request_' + req.body.requestee).toString('base64');
        redis_client.hmset(identifier, {
            'user': req.body.recipient,
            "email": req.body.requestee
        });
        redis_client.sadd(req.body.requester + "_request", identifier, function (d, s) {
        });
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.get('/logout', isAuthenticated, function (req, res) {
    var session = req.cookies['admin'];
    res.clearCookie('admin');
    res.redirect('/login');
});

app.get('/login', notAuthenticated, function (req, res) {
    res.clearCookie('admin');
    res.render('login');
});

app.post('/login', function (req, res) {
    if (req.body.username.toLowerCase() == ADMIN_EMAIL && req.body.password.toLowerCase() == ADMIN_PASSWORD) {
        request({
            url: 'http://178.62.199.243:8180/api/v2/user/session', //URL to hit
            method: 'POST',
            json: {
                email: LOGIN_EMAIL,
                password: req.body.password,
                duration: 0
            }
        }, function (error, response, body) {
            res.json({"response": body, 'error': error});
        });
    }
    else {
        res.sendStatus(401);
    }
});

app.get('/mentors', isAuthenticated, function (req, res) {
    res.render('mentors');
});

app.get('/talents', isAuthenticated, function (req, res) {
    res.render('talents');
});

app.get('/groups', isAuthenticated, function (req, res) {
    res.render('groups');
});

app.get('/posts', isAuthenticated, function (req, res) {
    res.render('posts');
});

app.get('/requests', isAuthenticated, function (req, res) {
    res.render('requests');
});

app.get('/jobs', isAuthenticated, function (req, res) {
    res.render('jobs');
});

app.get('/notifications', isAuthenticated, function (req, res) {
    res.render('notification');
});

app.get('/search', isAuthenticated, function (req, res) {
    res.render('search');
});

app.get('/new', isAuthenticated, function (req, res) {
    res.render('new');
});

app.get('/edit/:id', isAuthenticated, function (req, res) {
    res.render('edit');
});

app.post('/search', isAuthenticated, function (req, res) {
    client.search({
        index: 'talents',
        fields: ["title", "description", "name", "phone", "occupation", "summary", "email", "location", "marital", "link"],
        q: req.body.search_q
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._id;
        });
        client.mget({
            index: 'talents',
            body: {
                ids: result
            }
        }, function (error, response) {
            if (response.error) {
                res.json({"response": [], "error": null, "total": 0});
            }
            else {
                // var output = response.docs.map(function (item) {
                //     return item._source;
                // });
                res.json({"response": response.docs, "error": null, "total": response.docs.length});
            }
        });
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/conversations', isAuthenticated, function (req, res) {
    res.render('admin/conversations');
});

app.get('/library', isAuthenticated, function (req, res) {
    res.render('admin/library');
});

app.get('/jobs', isAuthenticated, function (req, res) {
    res.render('templates/jobs');
});

app.post('/elastic/:_type/index', function (req, res) {
    client.index({
        index: "talents",
        type: req.params._type,
        id: req.body.id,
        body: req.body
    }, function (err, resp, status) {
        if (status == 201) {
            res.sendStatus(status);
        }
        else {
            res.json({"error": err});
        }
    });
});

app.post('/elastic/:_type/update', function (req, res) {
    client.update({
        index: 'talents',
        type: req.params._type,
        id: req.body.id,
        body: {
            doc: {
                name: req.body.name,
                image_url: req.body.image_url,
                banner_url: req.body.banner_url,
                summary: req.body.summary,
                username: req.body.username,
                location: req.body.location,
                industry: req.body.industry,
                marital: req.body.marital,
                birthdate: req.body.birthdate,
                phone: req.body.phone,
                is_mentor: req.body.is_mentor,
                id: req.body.email
            }
        }
    }, function (error, response) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({response: response, error: error}));
    });
});

app.get('/elastic/:email/recommendations', function (req, res) {

    client.search({
        index: 'talents',
        type: "people",
        body: {
            "query": {
                "function_score": {
                    "functions": [
                        {
                            "random_score": {
                                "seed": new Date().getTime()
                            }
                        }
                    ],
                    "score_mode": "sum",
                    "filter": {
                        "bool": {
                            "must_not": [
                                {"term": {"id": req.params.email}},
                                {"term": {"is_mentor": true}}
                            ]
                        }
                    }
                }
            },
            "from": 0,
            "size": 3
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/elastic/recommendations', function (req, res) {

    client.search({
        index: 'talents',
        type: "people",
        body: {
            "query": {
                "function_score": {
                    "functions": [
                        {
                            "random_score": {
                                "seed": new Date().getTime()
                            }
                        }
                    ],
                    "score_mode": "sum"
                }
            },
            "from": 0,
            "size": 8
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/:_type/all', function (req, res) {

    client.search({
        index: 'talents',
        type: req.params._type,
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "match_all": {}
                    }
                }
            },
            "sort": [
                {
                    "id": {
                        "order": "desc"
                    }
                }
            ],
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/talents', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": false}}
                            ],
                            "must_not": {
                                "term": {"email": req.body.email}
                            }
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/mentors/query', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": true}}
                            ]
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/talents/query', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": false}}
                            ]
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/mentors', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": true}}
                            ],
                            "must_not": {
                                "term": {"email": req.body.email}
                            }
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/elastic/people', function (req, res) {

    client.search({
        index: 'talents',
        type: 'people',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "bool": {
                            "must": [
                                {"term": {"is_mentor": true}}
                            ]
                        }
                    }
                }
            },
            "from": req.body.from,
            "size": req.body.size
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.get('/elastic/groups', function (req, res) {

    client.search({
        index: 'talents',
        type: 'groups',
        body: {
            "query": {
                "constant_score": {
                    "filter": {
                        "match_all": {}
                    }
                }
            },
            "sort": [
                {
                    "id": {
                        "order": "desc"
                    }
                }
            ],
            "from": 0,
            "size": 10000
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null, "total": resp.hits.total});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/:_type/search', function (req, res) {

    var _q = {};
    _q[req.body.field] = req.body.value;

    client.search({
        index: 'talents',
        type: req.params._type,
        body: {
            "query": {
                "match": _q
            }
        }
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._source;
        });
        res.json({"response": result, "error": null});
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/elastic/job/query', function (req, res) {

    client.search({
        index: 'talents',
        type: 'job',
        fields: ["title", "description"],
        q: req.body.search_q
    }).then(function (resp) {
        var result = resp.hits.hits.map(function (elem) {
            return elem._id;
        });
        client.mget({
            index: 'talents',
            type: 'job',
            body: {
                ids: result
            }
        }, function (error, response) {
            if (response.error) {
                res.json({"response": [], "error": null, "total": 0});
            }
            else {
                var output = response.docs.map(function (item) {
                    return item._source;
                });
                res.json({"response": output, "error": null, "total": response.docs.length});
            }
        });
    }, function (err) {
        res.json({"error": err});
    });
});

app.post('/notifications/record', isAuthenticated, function (req, res) {
    try {
        var _id = new Date().getTime();
        redis_client.hmset(req.body.user + '_' + _id, {
            "id": req.body.user + '_' + _id,
            'actor': req.body.actor,
            'action': req.body.action,
            'verb': req.body.verb,
            'subject': req.body.subject,
            'message': req.body.message,
            'image_url': req.body.image_url
        });
        redis_client.sadd("notifications_" + req.body.user, req.body.user + '_' + _id);
        res.json({"response": true});
    } catch (err) {
        res.json({"response": false});
    }
});

app.get('/notifications/:email/get', function (req, res) {
    redis_client.smembers("notifications_" + req.params.email, function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.get('/notification/:id/get', function (req, res) {
    redis_client.hgetall(req.params.id, function (status, data) {
        res.json({"response": data, 'error': status});
    });
});

app.post('/job/delete', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tbljobs/' + req.body.title + '?id_field=title&id_type=string&filter=' + req.body.posted_by;
    request({
        url: url,
        method: 'DELETE',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        }
    }, function (error, response, body) {
        client.delete({
            index: 'talents',
            type: 'job',
            id: req.body.title + '_' + req.body.posted_by
        }, function (error, response) {
            res.json({"response": response, 'error': error});
        });
        // client.deleteByQuery({
        //     index: 'talents',
        //     type: 'job',
        //     body: {
        //         query: {
        //             term: { title: req.body.title },
        //             term: { description: req.body.description }
        //         }
        //     }
        // }, function (error, response) {
        //     res.json({"response": response, 'error': error});
        // });
    });
});

app.post('/user/:id/delete', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/rest/system/user?ids=' + req.params.id;
    request({
        url: url,
        method: 'DELETE',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        }
    }, function (error, response, body) {
        var _ul = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + req.body.query + '?id_field=email&id_type=string';
        request({
            url: _ul,
            method: 'DELETE',
            headers: {
                "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
            }
        }, function (err, resp, bd) {
            var _url = 'http://178.62.199.243:9200/talents/people/' + req.body.query;
            request({
                url: _url,
                method: 'DELETE'
            }, function (error, response, body) {
                res.json({"response": response, 'error': error});
            });
        });
    });
});

app.post('/group/delete', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblgroups/' + req.body.query + '?id_field=name&id_type=string';
    request({
        url: url,
        method: 'DELETE',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        }
    }, function (error, response, body) {
        client.delete({
            index: 'talents',
            type: 'groups',
            id: req.body.query
        }, function (error, response) {
            res.json({"response": response, 'error': error});
        });
        // client.deleteByQuery({
        //     index: 'talents',
        //     type: 'group',
        //     body: {
        //         query: {
        //             term: { name: req.body.query }
        //         }
        //     }
        // }, function (error, response) {
        //     res.json({"response": response, 'error': error});
        // });
    });
});

app.post('/mentor/mark', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + req.body.email +'?id_field=email&id_type=string';
    request({
        url: url,
        method: 'PUT',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "is_mentor": true
        }
    }, function (error, response, body) {
        client.update({
            index: 'talents',
            type: 'people',
            id: req.body.email,
            body: {
                doc: {
                    is_mentor: true
                }
            }
        }, function (error, response) {
            res.json({"response": response, 'error': error});
        })
    });
});

app.post('/mentor/unmark', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + req.body.email +'?id_field=email&id_type=string';
    request({
        url: url,
        method: 'PUT',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "is_mentor": false
        }
    }, function (error, response, body) {
        client.update({
            index: 'talents',
            type: 'people',
            id: req.body.email,
            body: {
                doc: {
                    is_mentor: false
                }
            }
        }, function (error, response) {
            res.json({"response": response, 'error': error});
        })
    });
});

app.post('/people/verify', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + req.body.email +'?id_field=email&id_type=string';
    request({
        url: url,
        method: 'PUT',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "is_verified": true
        }
    }, function (error, response, body) {
        client.update({
            index: 'talents',
            type: 'people',
            id: req.body.email,
            body: {
                doc: {
                    is_verified: true
                }
            }
        }, function (error, response) {
            res.json({"response": response, 'error': error});
        })
    });
});

app.post('/people/unverify', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + req.body.email +'?id_field=email&id_type=string';
    request({
        url: url,
        method: 'PUT',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "is_verified": false
        }
    }, function (error, response, body) {
        client.update({
            index: 'talents',
            type: 'people',
            id: req.body.email,
            body: {
                doc: {
                    is_verified: false
                }
            }
        }, function (error, response) {
            res.json({"response": response, 'error': error});
        })
    });
});

app.get('/requested', function (req, res) {
    redis_client.smembers("admin_mentor_requests", function (err, keys) {
        MHGETALL(keys, function (err, arr) {
            var acts = arr.map(function (res) {
                if (res[0] == null) {
                    return toObject(res[1]);
                }
            });
            res.json({"response": acts, 'error': err});
        });
    })
});

app.post('/mentors/accept', isAuthenticated, function (req, res) {

    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + req.body.email +'?id_field=email&id_type=string';
    request({
        url: url,
        method: 'PUT',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "is_mentor": true
        }
    }, function (error, response, body) {
        client.update({
            index: 'talents',
            type: 'people',
            id: req.body.email,
            body: {
                doc: {
                    is_mentor: true
                }
            }
        }, function (error, response) {
            var identifier = new Buffer('mentor_request_' + req.body.email).toString('base64');
            redis_client.srem("admin_mentor_requests", identifier, function (data, status) {
                if (status == 1) {
                    var ident = new Buffer('requested_' + req.body.email).toString('base64');
                    redis_client.del(ident, function (da, sta) {
                        res.json({"response": true, 'error': null});
                    });
                }
            });
        })
    });
});

app.post('/mentors/reject', isAuthenticated, function (req, res) {
    var identifier = new Buffer('mentor_request_' + req.body.email).toString('base64');
    redis_client.srem("admin_mentor_requests", identifier, function (data, status) {
        if (status == 1) {
            var ident = new Buffer('requested' + req.body.email).toString('base64');
            redis_client.del(ident, function (da, sta) {
                res.json({"response": true, 'error': null});
            });
        }
    });
});

app.post('/mentors/verify', isAuthenticated, function (req, res) {

    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblprofile/' + req.body.email +'?id_field=email&id_type=string';
    request({
        url: url,
        method: 'PUT',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "is_verified": true
        }
    }, function (error, response, body) {
        client.update({
            index: 'talents',
            type: 'people',
            id: req.body.email,
            body: {
                doc: {
                    is_verified: true
                }
            }
        }, function (error, response) {
            var identifier = new Buffer('verification_request_' + req.body.email).toString('base64');
            redis_client.srem("admin_mentor_requests", identifier, function (data, status) {
                if (status == 1) {
                    var ident = new Buffer('verification_requested_' + req.body.email).toString('base64');
                    redis_client.del(ident, function (da, sta) {
                        res.json({"response": true, 'error': null});
                    });
                }
            });
        })
    });
});

app.post('/ban/:id/post', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost/' + req.params.id;
    request({
        url: url,
        method: 'PATCH',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "status": 0
        }
    }, function (error, response, body) {
        res.json({"response": response, 'error': error});
    });
});

app.post('/activate/:id/post', function (req, res) {
    var url = 'http://talentsandmentors.com:8180/api/v2/prime/_table/tblpost/' + req.params.id;
    request({
        url: url,
        method: 'PATCH',
        headers: {
            "Authorization": "Basic YWRtaW5AdGFsZW50c2FuZG1lbnRvcnMuY29tOm11c3RhcGhhMQ=="
        },
        json: {
            "status": 1
        }
    }, function (error, response, body) {
        res.json({"response": response, 'error': error});
    });
});


if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on("exit", function (worker, code, signal) {
        cluster.fork();
    });
} else {
    app.listen(4000, function () {
        console.log('Application listening on port 8081!')
    });
}